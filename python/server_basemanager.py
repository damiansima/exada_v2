from multiprocessing.managers import SyncManager, BaseManager
from threading import Thread
import time


class MyManager(BaseManager):
    pass


class muestra(Thread):
    def __init__(self):
        Thread.__init__(self)

    def run(self):
        while True:
            time.sleep(.01)
            k = 'abc_%05d' % 1
            if k in syncdict:
                print(syncdict[k])


syncdict = {}


def get_dict():
    return syncdict


def put_dict(k, v):
    print(k, v)
    return (k, v)


if __name__ == "__main__":
    mu = muestra()
    mu.start()

    MyManager.register("syncdict", get_dict)
    MyManager.register("put", put_dict)
    # manager = MyManager(("localhost", 5004), authkey=b'a')
    manager = MyManager(("192.168.0.9", 5004), authkey=b'a')
    # manager.start()
    manager.get_server().serve_forever()
    #input("Press any key to kill server")
    manager.shutdown()

