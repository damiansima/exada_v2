#!/usr/bin/env python

import ctypes
import mmap
import os
import struct



# fd = os.open('/tmp/mmaptest', os.O_CREAT | os.O_TRUNC | os.O_RDWR)
fd = os.open('/tmp/mmaptest',  os.O_RDWR)
# os.ftruncate(fd, mmap.PAGESIZE )

buf = mmap.mmap(fd, mmap.PAGESIZE, mmap.MAP_SHARED, mmap.PROT_WRITE)

# print(buf[0:20])
#
# v = ctypes.c_float.from_buffer(buf)
# v.value = 1
# print("c_int <%s> :: " % v.value , buf[0:8] )
#
#
# buf[10:14]=b"AEda"

print(buf[0:40])


from ctypes import *
class Tag(Structure):
    _fields_=[
        ("entero",(c_int)),
        ("corto",(c_short)),
        ("largo",(c_longlong)),
        ("real",(c_float)),
        ("doble",(c_double))]


class TagValues(Union):
    _fields_=[
        ("entero",c_int),
        ("real",c_float),
        ("doble",c_double) ]

class LTag(Structure):
    _fields_=[
        ("id",c_int),
        ("length",c_short),
        ("type",c_char),
        ("value",c_double) ]


v = Tag.from_buffer(buf)
pHeader = pointer(v)
print(pHeader[2].entero)
print(v.entero)
v.entero=34

import time
import random
if True:
    pHeader[2].entero=random.randint(-1000,10000)
    time.sleep(.01)
print(buf[0:40])
print(sizeof(Tag)) # es correcto redondeado a multiplo de 4
