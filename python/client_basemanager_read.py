from multiprocessing.managers import SyncManager, BaseManager
import sys
import time
from time import sleep
import random


def gen_dict():
    d = dict()
    for a in range(10000):
        if a % 4 == 1:
            d['abc_%05d' % a] = random.randint(1,666)
        elif a % 4 == 2:
            d['abc_%05d' % a] = "string [%s] " % a
        elif a % 4 == 3:
            d['abc_%05d' % a] = False
        else:
            d['abc_%05d' % a] = True

    return d


if __name__ == "__main__":
    # manager = BaseManager(("localhost", 5004), authkey=b'a')
    manager = BaseManager(("192.168.0.9", 5004), authkey=b'a')
    manager.connect()
    manager.register("syncdict")
    manager.register("put")
    syncdict = manager.syncdict()

    print ("dict = ", dir(syncdict) )
    # print("dict:", syncdict)
    # print("dict str:", syncdict.__str__())

    # val = gen_dict()
    # print (val)
    # syncdict.update(val)

    try:
        i = 0
        key = 'hola'
        key = 'abc_%05d' % 1

        if not key in syncdict.keys(): syncdict.update([(key, 0)])

        while True:
            print(key, syncdict.get(key))
            print(key, syncdict.get(key).__dir__)
            print('hola', syncdict.get('hola').__dir__)
            print('hola', syncdict.get('hola'))
            time.sleep(.1)
    except KeyboardInterrupt:
        print ("Killed client")
