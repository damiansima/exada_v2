#!/usr/bin/env python

import ctypes
import mmap
import os
import struct
import time

fd = os.open('/tmp/mmaptest',  os.O_RDWR)
buf = mmap.mmap(fd, mmap.PAGESIZE, mmap.MAP_SHARED, mmap.PROT_WRITE)

from ctypes import *
class Tag(Structure):
    _fields_=[
        ("entero",(c_int)),
        ("corto",(c_short)),
        ("largo",(c_longlong)),
        ("real",(c_float)),
        ("doble",(c_double))]

v = Tag.from_buffer(buf)
pHeader = pointer(v)

while True:
    print("pHeader[2].entero",pHeader[2].entero)
    time.sleep(.1)


print(v.entero)
v.entero=34
pHeader[2].entero=-200
print(buf[0:40])
print(sizeof(Tag)) # es correcto redondeado a multiplo de 4
