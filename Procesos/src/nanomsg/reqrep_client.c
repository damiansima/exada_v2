#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <nanomsg/nn.h>
#include <nanomsg/reqrep.h>

int main (void) {
    printf("Connecting to hello world server…\n");
    int s = nn_socket(AF_SP, NN_REQ);
    nn_connect(s, "tcp://localhost:5555");

    int request_nbr;
    for (request_nbr = 0; request_nbr != 10; request_nbr++) {
        char buffer [10];
        printf("Sending Hello %d...\n", request_nbr);
        nn_send(s, "Hello", 5, 0);
        nn_recv(s, buffer, 10, 0);
        printf("Received World %d\n", request_nbr);
    }
    nn_close(s);
    return 0;
}
