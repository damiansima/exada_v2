/* 
 * File:   reqrep.c
 * Author: nb
 *
 * Created on 7 de septiembre de 2015, 22:45
 */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <time.h>
#include <stdio.h>
#include <nanomsg/nn.h>
#include <nanomsg/reqrep.h>

#define NODE0 "node0"
#define NODE1 "node1"

typedef struct {
    int datao;
    char msg[1024];
} pepe_s;

int node0(const char *url) {
    int sock = nn_socket(AF_SP, NN_REP);
    assert(sock >= 0);
    assert(nn_bind(sock, url) >= 0); 
    while (1) {
        //      char *buf = NULL;
        //      int bytes = nn_recv (sock, &buf, NN_MSG, 0);
        //      assert (bytes >= 0);
        //      printf ("NODE0: RECEIVED \"%s\"\n", buf);
        //      nn_freemsg (buf);

        pepe_s *buf;
        int bytes = nn_recv(sock, &buf, sizeof(pepe_s), 0);
        assert(bytes >= 0);
        printf("NODE0: RECEIVED \"%d- %s\"\n", buf->datao, buf->msg);
        nn_freemsg(buf);
    }
}

int node1(const char *url, const char *msg) {
    int sz_msg; // '\0' too
    int sock = nn_socket(AF_SP, NN_REQ);
    pepe_s MSG;
    int r;
    char buf[7];
    
    
    MSG.datao = strlen(msg) + 1;;
    strcpy(MSG.msg,msg);
    sz_msg=sizeof(MSG);
    
    
    assert(sock >= 0);
    assert( (r=nn_connect(sock, url)) >= 0);
    printf("NODE1: SENDING [%d]\"[%d]- %s\"\n", r, MSG.datao, MSG.msg);
    int bytes = nn_send(sock, &MSG, sizeof(pepe_s), 0);
    assert(bytes == sizeof(pepe_s));
    bytes = nn_recv (sock, buf, sizeof (buf), 0); 
     
    return nn_shutdown(sock, 0); 
}

int main(const int argc, const char **argv) {
    if (strncmp(NODE0, argv[1], strlen(NODE0)) == 0 && argc > 1)
        return node0(argv[2]);
    else if (strncmp(NODE1, argv[1], strlen(NODE1)) == 0 && argc > 2)
        return node1(argv[2], argv[3]);
    else {
        fprintf(stderr, "Usage: pipeline %s|%s <URL> <ARG> ...'\n",
                NODE0, NODE1);
        return 1;
    }
}
