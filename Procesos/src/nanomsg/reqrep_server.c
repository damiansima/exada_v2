#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <nanomsg/nn.h>
#include <nanomsg/reqrep.h>

int main(void) {
    int s = nn_socket(AF_SP, NN_REP);
    int rc = nn_bind(s, "tcp://*:5555");
    assert (rc >= 0);

    while (1) {
        char buffer[10];
        nn_recv(s, buffer, 10, 0);
        printf("Received Hello\n");
        nn_send(s, "World", 5, 0);
        sleep(1);
    }

    return 0;
}
