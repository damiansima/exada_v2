#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <semaphore.h>
#include <string.h>
#include <sys/types.h>

#include <sys/mman.h>
#include <sys/msg.h>
#include <fcntl.h>

#include <sys/ipc.h>
#include <sys/shm.h>

#include <sys/time.h>

#include <stdarg.h>   



struct dipc_list {
    int key;
    struct dipc_list *next, *prev;
    void *data;
};


#define LEN_SHM 1024

typedef struct
{
    char name[96];
    int  addres;
    int  start;
    int  end;
    int  len_block;
    int  offset_block;
}Block;

typedef struct
{
    int num_block;
    enum  {busy, ready, writing }state;
}E_SHM_SCAN_H;

typedef Block E_SHM_SCAN_B;


//E_SHM_SCAN_H shmctr[LEN_SHM];
//E_SHM_SCAN_B *shmblock = (E_SHM_SCAN_B *)(shmctr+1);
//E_SHM_SCAN_B *nexshmblock, *prevshmblock;

typedef struct{
    int key;
    int nbytes;
    int next;
    int prev;
    char data[];
}LIST_BLOCK; 

typedef struct{
    int count_list;
    int block_start;
    int block_end;
    int length;
}LIST_CTR; 

typedef struct{
    LIST_CTR ctr;
    LIST_BLOCK block;
}LIST_t; 

#define assert_msg(A) ((A)? 0 : fprintf(stderr,"%s:%u: assert_msg(%s) fail !\n",__FILE__, __LINE__, #A))

static LIST_BLOCK *list_get_block(LIST_t *list, int key) 
{
    LIST_BLOCK *block=NULL;
    int i;
    
    if (list->ctr.count_list <= 0)
        return (LIST_BLOCK *)NULL; 
    
    block = (LIST_BLOCK *)((char *)&list->block);  
    if(block->key==key)
        return block;
    
    for(i=1; i < list->ctr.count_list && block->next>=0; i++)
    {
        block = (LIST_BLOCK *)((char *)&list->block + block->next);  
        if(block->key==key)
            return block;
    }
    return (LIST_BLOCK *)NULL;  
}
    
static LIST_BLOCK *list_add(LIST_t *list, int nbytes, int key) 
{
    LIST_BLOCK *block, *block_prev;
    if assert_msg((list->ctr.length - list->ctr.block_end - nbytes) >0 )
        return (LIST_BLOCK *)NULL;
    
    if assert_msg(list_get_block(list,key)==NULL)
        return (LIST_BLOCK *)NULL;
    
    block = (LIST_BLOCK *)((char *)&list->block + list->ctr.block_end);
    block->key = key;
    block->nbytes = nbytes;
    block->next =-1;
    block->prev = -1;
    memset(block->data,0,nbytes);
    
    if(list->ctr.count_list > 0)
    {
        block->prev = list->ctr.block_start;
        block_prev =(LIST_BLOCK *)((char *)&list->block + block->prev);  
        block_prev->next =  list->ctr.block_end;
    }
    list->ctr.block_start = list->ctr.block_end;
    list->ctr.block_end = list->ctr.block_start + nbytes + sizeof(LIST_BLOCK);
    list->ctr.count_list++;
    
    return block;
}


static LIST_BLOCK *list_first_block(LIST_t *list) 
{
    return (LIST_BLOCK *)((char *)&list->block);  
}

static LIST_BLOCK *list_next_block(LIST_t *list, LIST_BLOCK *block) 
{
    if(block->next == -1 )
    {
        return (LIST_BLOCK *)NULL;  
    }
    else 
    {
       return (LIST_BLOCK *)((char *)&list->block + block->next);  
    }
}

void list_init(LIST_t *list,int length)
{
    list->ctr.block_end=0;
    list->ctr.block_start=-1;
    list->ctr.count_list=0;
    list->ctr.length=length;
}

#define TESTS(a) printf("%-30s: %s\n",#a,a)
#define TEST(a) printf("%-30s: %i\n",#a,(int)a)
#define TESTN(a) {printf("--- %-30s\n",#a);a;}

int main ()
{
    LIST_BLOCK *block;
//    char *shm = (char *)malloc(LEN_SHM);
    LIST_t *list = (LIST_t *)malloc(LEN_SHM);
    
    list_init(list,LEN_SHM);
    
/*    TEST(list);
    TEST(&list->block);
    TEST(list->block.data);
    TEST(list->ctr.block_end);
    TEST(list->ctr.block_start);
    TEST(list->ctr.count_list);
    
    TESTN(block=list_add(list, 88, 2));
    TEST(block);
    TEST(block->data);
    TEST(block->nbytes);
    TEST(block->next);
    TEST(block->prev);
    TESTS(block->data);
    
    TESTN(block=list_add(list, 18, 3));
    TEST(block);
    TEST(block->data);
    TEST(block->nbytes);
    TEST(block->next);
    TEST(block->prev);
    TESTS(block->data);
    
    
    TESTN(block=list_add(list, 38, 4));
    TEST(block);
    TEST(block->data);
    TEST(block->nbytes);
    TEST(block->next);
    TEST(block->prev);
    TESTS(block->data);
    TEST(list->ctr.block_end);
    TEST(list->ctr.block_start);
    TEST(list->ctr.count_list);
*/
    
    TESTN(block = list_add(list, 100, 8));
    TESTN(block = list_add(list, 100, 2));
    TESTN(block = list_add(list, 100, 3));
    TESTN(block = list_add(list, 100, 4));
    TESTN(block = list_add(list, 100, 5));
    TESTN(block = list_add(list, 100, 6));
    TESTN(block = list_add(list, 100, 7));
    strcpy(block->data,"nada7");
//    TESTN(block=list_add(list, 98, 9));
    TEST(list->ctr.count_list);
    
//    TESTN(block = list_first_block(list));
//    while(block)
//    {
//        strcpy(block->data,"nada");
//        block = list_next_block(list,block);
//    }
    block=list_get_block(list,8);
    memcpy(block->data,"--- hola LOCO     ",13);
    
    TESTN(block = list_first_block(list));
    while(block)
    {
        TEST(block);
        TEST(block->key);
        TEST(block->data);
        TEST(block->nbytes);
        TEST(block->next);
        TEST(block->prev);
        TESTS(block->data);
        TESTN(block = list_next_block(list,block));
    }
    free(list);
            
    return 0;
}


//
//
//static void list_add(struct dipc_list **last, struct dipc_list *add) {
//    add->next = NULL;
//    add->prev = *last;
//    (*last)->next = add;
//    *last = add;
//}
//
//static struct dipc_list *list_find_key(struct dipc_list *first, int key) {
//    struct dipc_list *dl2 = first->next;
//
//    while (dl2) {
//        if (dl2->info.key == key)
//            break;
//        dl2 = dl2->next;
//    }
//    return dl2;
//}
//
//static void list_remove(struct dipc_list **last, struct dipc_list *dl) {
//    if (dl == *last)
//        (*last) = (*last)->prev;
//    (dl->prev)->next = dl->next;
//    if (dl->next)
//        (dl->next)->prev = dl->prev;
//}
//
//static struct dipc_list *get_next_key(struct dipc_list *dl) {
//    struct dipc_list *dl2 = dl->next;
//
//    while (dl2) {
//        if (dl2->info.key == dl->info.key)
//            break;
//        dl2 = dl2->next;
//    }
//    return dl2;
//}
//
//static BOOLEAN respond_to(struct dipc_list *query) {
//    struct message response;
//    struct dipc_list *dl;
//    int type;
//
//    response.info = query->info;
//    response.address = REFEREE_ADDRESS;
//    response.pid = query->pid;
//    response.request = RES_REFEREE;
//
//    type = query->info.type;
//    dl = find_in_list(&gotten_first[type], query->key);
//    if (dl) {
//        if (verbose || debug_protocol)
//            log(LOC, "Found Existing Key %d of type %s From %s",
//            dl->key, ipc_type[type], inet_ntoa(dl->address.sin_addr));
//        response.info.arg3 = dl->size;
//
//        /* 'raw' net address as arg4 needs extra conversion! */
//        response.info.arg4 = ntohl(dl->address.sin_addr.s_addr);
//        if (dl->distrib) {
//            if (verbose)
//                log(LOC, "found DIPC");
//            response.info.arg2 = FOUND_DIPC;
//            if (dl->transfer_mode == SEGMENT)
//                response.info.arg2 |= IS_SEGMENT;
//        } else {
//            if (verbose)
//                log(LOC, "found IPC");
//            response.info.arg2 = FOUND_IPC;
//        }
//        /* log(LOC,"owner address is %s",inet_ntoa(dl->address.sin_addr)); */
//    } else { /* no gotten found */
//        if (verbose)
//            log(LOC, "key not found");
//        response.info.arg3 = 0;
//
//        /* 'raw' net address as arg4 needs extra conversion! */
//        response.info.arg4 = ntohl(query->address.sin_addr.s_addr); /* its own address */
//        response.info.arg2 = NOT_FOUND;
//    }
//
//    if (debug_protocol) {
//        log(LOC, "Referee: respond %s func %d key %d pid %d %d",
//            inet_ntoa(query->address.sin_addr), response.info.func,
//            response.info.key, response.info.pid, response.pid);
//        log(LOC, "Referee: args %d %d %d %d owner %d",
//            response.info.arg1, response.info.arg2, response.info.arg3,
//            response.info.arg4, response.info.owner);
//    }
//
//    if (send_in_message(&response, query->address) == FAIL) {
//        log(LOC, "ERROR: Can't Return Result Of Search");
//        remove_from_list(&wanted_last[type], query);
//        free(query);
//        return FAIL;
//    }
//    query->status = REF_ANSWERED;
//    query->answer_time = time(NULL);
//    return SUCCESS;
//}
//
///* Places the query in the 'wanted' linked list and then conducts a search 
//   in the referee linked lists. If the query can be answered, it will
//   call respond_to, otherwise it has to wait for the results of earlier 
//   queries */
//static BOOLEAN referee_dipc_search(struct message *message) {
//    struct dipc_list *dl, *dl2;
//    int type = message->info.type;
//
//    if (verbose)
//        log(LOC, "Handling Search For Key %d of type %s",
//        message->info.key, ipc_type[type]);
//    /* we should put the request in a linked list, waiting for a commit answer. */
//    dl2 = (struct dipc_list *) malloc(sizeof (struct dipc_list));
//    if (dl2 == NULL) {
//        log(LOC, "ERROR: No Mem");
//        return FAIL;
//    }
//    dl2->key = message->info.key;
//    dl2->pid = message->pid;
//    dl2->info = message->info;
//    dl2->size = 0;
//    dl2->transfer_mode = message->info.arg3;
//    dl2->distrib = message->info.arg2;
//    dl2->status = REF_WAITING;
//    if (verbose) {
//        if (dl2->distrib)
//            log(LOC, "new request is DIPC");
//        else
//            log(LOC, "new request is IPC");
//    }
//    dl2->address = message->address;
//
//    /* see if another one is already waiting. do this before putting the new 
//       query in the list. */
//    dl = find_in_list(&wanted_first[type], message->info.key);
//    /* now put the new query in the list */
//    add_to_list(&wanted_last[type], dl2);
//    if (dl) { /* found another query, the new one should wait */
//        if (verbose)
//            log(LOC, "Key %d Must Wait", message->info.key);
//    } else {
//        /* this is the only request. answer it now */
//        if (FAIL == respond_to(dl2))
//            return FAIL;
//    }
//    return SUCCESS;
//}
//
///* handles the commit messages. If the structure is created in the local
//   machine, the request will be put in the 'gotten' linked list. otherwise
//   it is thrown away. In any case it is checked to see if other quesries
//   have been waiting for this result, and they are informed accordingly. */
//static BOOLEAN referee_dipc_commit(struct message *message, int restart) {
//    struct dipc_list *dl, *dl2, *dl3;
//    struct message response;
//    int type;
//
//    if (verbose)
//        log(LOC, "Handling Commit For Key %d of Type %s",
//        message->info.key, ipc_type[message->info.type]);
//    if (debug_protocol) {
//        log(LOC, "Referee: attemp com. %s func %d key %d pid %d %d",
//            inet_ntoa(message->address.sin_addr), message->info.func,
//            message->info.key, message->info.pid, message->pid);
//        log(LOC, "Referee: args %d %d %d %d owner %d",
//            message->info.arg1, message->info.arg2, message->info.arg3,
//            message->info.arg4, message->info.owner);
//    }
//
//    type = message->info.type;
//    response = *message;
//    response.address = REFEREE_ADDRESS;
//    /* this may be used to inform other tasks of the search result */
//    response.request = RES_REFEREE;
//    /* find the corresponding in the 'wanted' linked list */
//    dl = find_in_list(&wanted_first[type], message->info.key);
//    while (dl) {
//        if (SAME_MACHINE(&(dl->address), message->address))
//            break;
//        dl = get_next_key(dl);
//    }
//    if (dl == NULL) {
//        log(LOC, "ERROR: Commit For Non-Existing Key");
//        return FAIL;
//    } else {
//        /* in any case remove the request from wanted list, because either it
//           will be thrown away (failer) or put in another linked list (success) */
//        remove_from_list(&wanted_last[type], dl);
//        if (message->info.arg2 > 0) { /* successful COMMIT. put it in existing dipc list */
//            if (verbose)
//                log(LOC, "Commit: Success");
//            dl->status = REF_NORMAL;
//            dl->mode = message->info.arg3;
//            dl->size = message->info.arg2;
//
//            add_to_list(&gotten_last[type], dl);
//            /* we should also return FOUND to all the other waiting tasks.
//               search from the start and call respond_to */
//            dl2 = find_in_list(&wanted_first[type], dl->key);
//            while (dl2) {
//                dl3 = get_next_key(dl2);
//                if (respond_to(dl2) == FAIL)
//                    log(LOC, "ERROR: Can't Respond To Request");
//                dl2 = dl3;
//            }
//        } else { /* failed commit. remove it and give a chance to another machine */
//            if (verbose)
//                log(LOC, "Commit: No success");
//            dl2 = get_next_key(dl);
//            while (dl2) {
//                if (SUCCESS == respond_to(dl2))
//                    break;
//                dl2 = get_next_key(dl2);
//            }
//            /* destroy the failed request */
//            free(dl);
//        }
//    }
//
//    if (restart) {
//        /* the process sending the commit is waiting inside its local kernel.
//          We have to answer it, so it can continue. */
//        response = *message;
//        response.address = REFEREE_ADDRESS;
//        response.request = RES_REFEREE;
//        if (debug_protocol) {
//            log(LOC, "Referee: commit %s func %d key %d pid %d %d",
//                inet_ntoa(message->address.sin_addr), response.info.func,
//                response.info.key, response.info.pid, response.pid);
//            log(LOC, "Referee: args %d %d %d %d owner %d",
//                response.info.arg1, response.info.arg2, response.info.arg3,
//                response.info.arg4, response.info.owner);
//        }
//        if (send_in_message(&response, message->address) == FAIL) {
//            /* don't worry. The worker that sent the commit message will timeout */
//            log(LOC, "ERORR: Can't Restart The Commit: %s",
//                inet_ntoa(dl2->address.sin_addr));
//            return FAIL;
//        }
//    }
//    if (verbose)
//        log(LOC, "key %d Commit Done", message->info.key);
//    return SUCCESS;
//}
//
///* a request to delete a key in referee linked lists. It also checks to see
//   if it should inform a shared memory manager process to detach */
//static BOOLEAN referee_dipc_delkey(struct message *message) {
//    struct message response;
//    struct dipc_list *dl, *dl2;
//    int type;
//
//    if (verbose)
//        log(LOC, "Handling Delete Of Key %d of Type %s",
//        message->info.key, ipc_type[message->info.type]);
//    type = message->info.type;
//    dl = find_in_list(&gotten_first[type], message->info.key);
//    while (dl) {
//        if (SAME_MACHINE(&(dl->address), message->address))
//            break;
//        dl = get_next_key(dl);
//    }
//    if (dl == NULL) {
//        log(LOC, "ERROR: There Is No Key To Delete");
//        /* return FAIL; don't return fail, so that the stoped task can resatart */
//    }
//    else {
//        if (verbose) {
//            if (dl->mode & IPC_OWN)
//                log(LOC, "This Is The Owner");
//            else
//                log(LOC, "This Isn't The Owner");
//        }
//        remove_from_list(&gotten_last[type], dl);
//        /* now see if there is only one remaining machine with the key.
//           if this is the case and the key type is SHM, send a message 
//           to that machine, so that the key_manager can detach itself */
//        dl2 = find_in_list(&gotten_first[type], message->info.key);
//        if (type == SHM && dl2 && get_next_key(dl2) == NULL) {
//            if (verbose)
//                log(LOC, "Only Machine %s Has This Shared Memory Key",
//                inet_ntoa(dl->address.sin_addr));
//            response = *message;
//            response.address = REFEREE_ADDRESS;
//            response.request = REQ_SHMMAN;
//            response.info.func = DETACH;
//
//            if (debug_protocol) {
//                log(LOC, "Referee: delmgr %s func %d key %d pid %d %d",
//                    inet_ntoa(dl2->address.sin_addr), response.info.key,
//                    response.info.key, response.info.pid, response.pid);
//                log(LOC, "Referee: args %d %d %d %d owner %d",
//                    response.info.arg1, response.info.arg2, response.info.arg3,
//                    response.info.arg4, response.info.owner);
//            }
//            if (send_in_message(&response, dl2->address) == FAIL) {
//                log(LOC, "ERROR: Can't Inform Shared Memory Owner To Delete");
//                /* return FAIL; don't return fail, so that the stoped task can resatart */
//            }
//        }
//        free(dl);
//    }
//    /* in any case, come here and send a reply to original task */
//    response = *message;
//    response.address = REFEREE_ADDRESS;
//    response.request = RES_REFEREE;
//
//    if (debug_protocol) {
//        log(LOC, "Referee: delkey %s func %d key %d pid %d %d",
//            inet_ntoa(message->address.sin_addr), response.info.func,
//            response.info.key, response.info.pid, response.pid);
//        log(LOC, "Referee: args %d %d %d %d owner %d",
//            response.info.arg1, response.info.arg2, response.info.arg3,
//            response.info.arg4, response.info.owner);
//    }
//    if (send_in_message(&response, message->address) == FAIL) {
//        log(LOC, "ERORR: Can't Restart The Delete at %s",
//            inet_ntoa(message->address.sin_addr));
//        return FAIL;
//    }
//    if (verbose)
//        log(LOC, "key %d Delete Acknowledge Done", message->info.key);
//    return SUCCESS;
//}
//
///* a request to remove an IPC structure. This one is comming from the owner
//   of the structure. Inform other computers with a structure of the same 
//   key */
//static BOOLEAN referee_dipc_rmid(struct message *message) {
//    struct message response;
//    struct dipc_list *dl, *dl2;
//    int type = message->info.type;
//    int key = message->info.key;
//
//    if (verbose)
//        log(LOC, "Handling RMID Of Key %d of Type %s", key, ipc_type[type]);
//    dl = find_in_list(&gotten_first[type], key);
//    while (dl) {
//        if (SAME_MACHINE(&(dl->address), message->address))
//            break;
//        dl = get_next_key(dl);
//    }
//    if (dl == NULL) {
//        log(LOC, "ERROR: There Is No Key To RMID");
//        /* return FAIL; don't return fail, so that the stoped task can resatart */
//    }
//    else {
//        response = *message;
//        response.address = REFEREE_ADDRESS;
//        response.request = REQ_DOWORK;
//        /* send all the keys except the first one */
//        dl2 = find_in_list(&gotten_first[type], key);
//        while (dl2) {
//            if (dl2 != dl) /* not the original one! */ {
//                if (verbose)
//                    log(LOC, "telling key %d of type %s at %s to RMID", dl2->key,
//                    ipc_type[type], inet_ntoa(dl2->address.sin_addr));
//
//                if (debug_protocol) {
//                    log(LOC, "Referee: rmid %s func %d key %d pid %d %d",
//                        inet_ntoa(dl2->address.sin_addr), response.info.func,
//                        response.info.key, response.info.pid, response.pid);
//                    log(LOC, "Referee: args %d %d %d %d owner %d",
//                        response.info.arg1, response.info.arg2, response.info.arg3,
//                        response.info.arg4, response.info.owner);
//                }
//                if (send_in_message(&response, dl2->address) == FAIL) {
//                    log(LOC, "Can't Tell key %d of type %s at %s to RMID", dl2->key,
//                        ipc_type[type], inet_ntoa(dl2->address.sin_addr));
//                    /* return FAIL; don't return fail, so that the stoped task can resatart */
//                }
//            }
//            dl2 = get_next_key(dl2);
//        }
//        /* now see if there is only one machine with the key. This happens 
//          when all the tasks are on the same machine.
//          If this is the case and the key type is SHM, send a message 
//          to that machine, so that the key_manager can detach itself */
//        dl2 = find_in_list(&gotten_first[type], key);
//        if (type == SHM && dl2 && get_next_key(dl2) == NULL) {
//            if (verbose)
//                log(LOC, "Only Machine %s Has This Shared Memory Key",
//                inet_ntoa(dl->address.sin_addr));
//            response = *message;
//            response.address = REFEREE_ADDRESS;
//            response.request = REQ_SHMMAN;
//            response.info.func = DETACH;
//
//            if (debug_protocol) {
//                log(LOC, "Referee: delmgr %s func %d key %d pid %d %d",
//                    inet_ntoa(dl2->address.sin_addr), response.info.func,
//                    response.info.key, response.info.pid, response.pid);
//                log(LOC, "Referee: args %d %d %d %d owner %d",
//                    response.info.arg1, response.info.arg2, response.info.arg3,
//                    response.info.arg4, response.info.owner);
//            }
//
//            if (send_in_message(&response, dl2->address) == FAIL) {
//                log(LOC, "ERROR: Can't Inform Shared Memory Owner To Delete");
//                /* return FAIL; don't return fail, so that the stoped task can resatart */
//            }
//        }
//    } /* if dl */
//    /* log(LOC,"acknowledging the RMID"); */
//    response = *message;
//    response.address = REFEREE_ADDRESS;
//    response.request = RES_REFEREE;
//
//    if (debug_protocol) {
//        log(LOC, "Referee: ack rmid %s func %d key %d pid %d %d",
//            inet_ntoa(message->address.sin_addr), response.info.func,
//            response.info.key, response.info.pid, response.pid);
//        log(LOC, "Referee: args %d %d %d %d owner %d",
//            response.info.arg1, response.info.arg2, response.info.arg3,
//            response.info.arg4, response.info.owner);
//    }
//    if (send_in_message(&response, message->address) == FAIL)
//        return FAIL;
//    if (verbose)
//        log(LOC, "Key %d RMID Acknowledge Done", key);
//    return SUCCESS;
//}
//
//static void sig_handler(int signo) {
//    log(LOC, "Quiting For Signal No. %d", signo);
//    exit(5);
//}
//
///* checks for stale requests and if timed out, fakes a failed commit.
//   This is called at regular intervals */
//void check_answers(void) {
//    int type;
//    struct message message;
//    time_t curr_time;
//    struct dipc_list *dl;
//
//    if (referee_timeout <= 0 || global_timeout == 0) return;
//
//    message.request = RES_REFEREE;
//    message.info.arg2 = 0; /* FAIL */
//    curr_time = time(NULL);
//    for (type = 0; type < 3; type++) {
//        dl = wanted_first[type].next;
//        while (dl) {
//            if (dl->status == REF_ANSWERED) {
//                if ((curr_time - dl->answer_time) > referee_timeout) {
//                    if (verbose || debug_protocol)
//                        log(LOC, "Handling Stale request at %s",
//                        inet_ntoa(dl->address.sin_addr));
//                    /* in case the request is on the local lost, the pid is missing here ... */
//                    message.address = dl->address;
//                    message.info = dl->info;
//                    message.pid = dl->pid;
//                    message.info.func = COMMIT;
//                    referee_dipc_commit(&message, 0);
//                }
//            }
//            dl = dl->next;
//        }
//    }
//}
//
///* the back_door control center. It receives request from the UNIX domain 
//   socket and executes them. This is for 'out of band' control of the referee,
//   without the need to redesign it. The work here should be done quickly, 
//   because while the referee is handling back_door requests, it won't be 
//   able to handle normal, more important requests. */
//static void bd_work_on(int ux_sockfd, struct back_door_message *bd_message) {
//    struct dipc_list *dl;
//    struct back_door_message bd_response;
//    int type, want_trans, act_trans;
//
//    switch (bd_message->cmd) {
//        case BACK_DOOR_INFO:
//            if (verbose)
//                log(LOC, "back door: request for information\n");
//            /* display the information for shared memories, message queues, and
//               semaphore sets */
//            for (type = 0; type < 3; type++) {
//                dl = gotten_first[type].next;
//                while (dl) {
//                    bd_response.arg1 = type;
//                    bd_response.arg2 = dl->key;
//                    bd_response.arg3 = dl->size;
//                    bd_response.arg4 = dl->distrib;
//                    bd_response.arg5 = dl->address.sin_addr.s_addr;
//                    want_trans = sizeof (bd_response);
//                    act_trans = writen(ux_sockfd, (char *) &bd_response, want_trans, NULL);
//                    if (act_trans < want_trans) {
//                        log(LOC, "ERROR: sent %d From %d Bytes  BECAUSE %s",
//                            act_trans, want_trans, strerror(errno));
//                        break;
//                    }
//                    dl = dl->next;
//                }
//                if (dl != NULL) break;
//            }
//            if (type == 3) {
//                /* this signals the end of information */
//                bd_response.arg1 = -1;
//                want_trans = sizeof (bd_response);
//                act_trans = writen(ux_sockfd, (char *) &bd_response, want_trans, NULL);
//                if (act_trans < want_trans)
//                    log(LOC, "ERROR: sent %d From %d Bytes  BECAUSE %s",
//                    act_trans, want_trans, strerror(errno));
//            }
//            break;
//
//        case BACK_DOOR_REMOVE:
//            if (verbose)
//                log(LOC, "back door: request for removal\n");
//            dl = gotten_first[bd_message->arg1].next;
//            while (dl) {
//                if (dl->key == bd_message->arg2 &&
//                    dl->address.sin_addr.s_addr == bd_message->arg5)
//                    break;
//                dl = dl->next;
//            }
//            if (dl) {
//                remove_from_list(&gotten_last[bd_message->arg1], dl);
//                if (verbose)
//                    log(LOC, "back door: removed the entry\n");
//                /* tell dipcreg of the success */
//                bd_message->arg1 = 0;
//            } else {
//                if (verbose)
//                    log(LOC, "back door: did not find the entry\n");
//                bd_message->arg1 = -1;
//            }
//            want_trans = sizeof (*bd_message);
//            act_trans = writen(ux_sockfd, (char *) bd_message, want_trans, NULL);
//            if (act_trans < want_trans)
//                log(LOC, "ERROR: Sent %d From %d Bytes  BECAUSE %s",
//                act_trans, want_trans, strerror(errno));
//            break;
//
//        default:
//            log(LOC, "ERROR: Unknown back door request: %d\n", bd_message->cmd);
//    }
//}
//
//void referee(void) {
//    fd_set rSelFD; /* for select */
//    struct timeval TimeVal;
//    int count;
//    int want_trans, act_trans;
//    struct message message;
//    int in_client_len, in_len;
//    struct sockaddr_in in_client_addr, in_addr;
//    int in_sockfd, in_newsockfd = -1;
//    struct sockaddr_un back_ux_addr, back_ux_client_addr;
//    int back_ux_len, back_ux_client_len;
//    int back_ux_sockfd, back_ux_newsockfd;
//    struct sigaction sact;
//    struct back_door_message bd_message;
//
//    log(LOC, "referee Started");
//
//    sact.sa_handler = sig_handler;
//    sigemptyset(&sact.sa_mask);
//    sact.sa_flags = 0;
//
//    if (sigaction(SIGINT, &sact, NULL) < 0)
//        log(LOC, "ERROR: Can't Catch Signal  BECAUSE %s", strerror(errno));
//    if (sigaction(SIGTERM, &sact, NULL) < 0)
//        log(LOC, "ERROR: Can't Catch Signal  BECAUSE %s", strerror(errno));
//    if (sigaction(SIGSEGV, &sact, NULL) < 0)
//        log(LOC, "ERROR: Can't Catch Signal  BECAUSE %s", strerror(errno));
//
//    /* this forces the referee to read the etc/dipc.allow file again */
//    sact.sa_handler = sighup_handler;
//    if (sigaction(SIGHUP, &sact, NULL) < 0)
//        log(LOC, "ERROR: Can't Catch Signal  BECAUSE %s", strerror(errno));
//    if (on_exit(exit_handler, NULL) < 0)
//        log(LOC, "ERROR: Can't install exit handler BECAUSE %s", strerror(errno));
//
//    for (count = 0; count < 3; count++) {
//        gotten_first[count].next = gotten_first[count].prev = NULL;
//        gotten_last[count] = &gotten_first[count];
//
//        wanted_first[count].next = wanted_first[count].prev = NULL;
//        wanted_last[count] = &wanted_first[count];
//    }
//    /* prepare the "back door" socket */
//    sprintf(back_ux_name, "%s/%s", dipcd_home, REFEREE_BACKDOOR);
//    bzero((char *) &back_ux_addr, sizeof (back_ux_addr));
//    back_ux_addr.sun_family = AF_UNIX;
//    strcpy(back_ux_addr.sun_path, back_ux_name);
//    back_ux_len = strlen(back_ux_addr.sun_path) + sizeof (back_ux_addr.sun_family);
//
//    unlink(back_ux_name); /* just in case... */
//    back_ux_sockfd = plug(AF_UNIX, (struct sockaddr *) &back_ux_addr, back_ux_len);
//    if (back_ux_sockfd < 0) {
//        log(LOC, "ERROR: Can't Create Socket %s", back_ux_name);
//        exit(20);
//    }
//
//    bzero((char *) &in_addr, sizeof (in_addr));
//    in_addr.sin_family = AF_INET;
//    in_addr.sin_addr.s_addr = htonl(INADDR_ANY);
//    in_addr.sin_port = htons(DIPC_REFEREE_PORT);
//    in_len = sizeof (in_addr);
//
//    in_sockfd = plug(AF_INET, (struct sockaddr *) &in_addr, in_len);
//    if (in_sockfd < 0) {
//        log(LOC, "ERROR: Can't Create Socket");
//        exit(20);
//    }
//
//    while (1) {
//        TimeVal.tv_sec = referee_timeout;
//        TimeVal.tv_usec = 0;
//        FD_ZERO(&rSelFD);
//        FD_SET(in_sockfd, &rSelFD);
//        FD_SET(back_ux_sockfd, &rSelFD);
//        while (1) {
//            if (select(FD_SETSIZE, &rSelFD, NULL, NULL, &TimeVal) < 0) {
//                if (errno != EINTR) {
//                    log(LOC, "ERROR: select() failed  BECAUSE %s", strerror(errno));
//                    close(in_sockfd);
//                    exit(20);
//                }
//            } else break;
//        }
//        if (FD_ISSET(in_sockfd, &rSelFD)) {
//            if (use_udp)
//                in_newsockfd = in_sockfd;
//            else {
//                in_client_len = sizeof (in_client_addr);
//                while (1) {
//                    if ((in_newsockfd = accept(in_sockfd, (struct sockaddr *) &in_client_addr,
//                        &in_client_len)) < 0) {
//                        if (errno != EINTR) {
//                            log(LOC, "ERROR: accept() Failed  BECAUSE %s", strerror(errno));
//                            close(in_sockfd);
//                            exit(20);
//                        }
//                    } else break;
//                }
//            }
//            want_trans = sizeof (message);
//            act_trans = get_in_message(in_newsockfd, &message);
//            if (act_trans < want_trans)
//                log(LOC, "ERROR: Received %d From %d Bytes  BECAUSE %s",
//                act_trans, want_trans, strerror(errno));
//            else {
//                if (verbose)
//                    log(LOC, "Remote Call From %s", inet_ntoa(message.address.sin_addr));
//
//                /* check to see if the computer is allowed to access the referee */
//                if (check_allow(message.address.sin_addr.s_addr) == FAIL) {
//                    log(LOC, "Warning: Address %s is not Allowed", inet_ntoa(message.address.sin_addr));
//                    if (!use_udp)
//                        close(in_newsockfd);
//                    continue; /* just ignore it */
//                }
//                if (debug_protocol) {
//                    log(LOC, "Referee: call from %s func %d key %d pid %d %d",
//                        inet_ntoa(message.address.sin_addr),
//                        message.info.func, message.info.key,
//                        message.info.pid, message.pid);
//                    log(LOC, "Referee: args %d %d %d %d owner %d",
//                        message.info.arg1, message.info.arg2, message.info.arg3,
//                        message.info.arg4, message.info.owner);
//                }
//                switch (message.info.func) {
//                    case SEARCH:
//                        referee_dipc_search(&message);
//                        break;
//
//                    case COMMIT:
//                        referee_dipc_commit(&message, 1);
//                        break;
//
//                    case DELKEY:
//                        referee_dipc_delkey(&message);
//                        break;
//
//                    case RMID:
//                        referee_dipc_rmid(&message);
//                        break;
//
//                    default:
//                        log(LOC, "ERROR: Unknown Function %s", work_string(message.info.func));
//                        break;
//                }
//            } /* message received */
//            if (!use_udp)
//                close(in_newsockfd);
//        } else if (FD_ISSET(back_ux_sockfd, &rSelFD)) {
//            if (verbose)
//                log(LOC, "back door call");
//            back_ux_client_len = sizeof (back_ux_client_addr);
//            while (1) {
//                if ((back_ux_newsockfd = accept(back_ux_sockfd,
//                    (struct sockaddr *) &back_ux_client_addr, &back_ux_client_len)) < 0) {
//                    if (errno != EINTR) {
//                        log(LOC, "ERROR: accept() Failed  BECAUSE %s", strerror(errno));
//                        close(back_ux_newsockfd);
//                    } else continue;
//                }
//                else {
//                    want_trans = sizeof (bd_message);
//                    act_trans = readn(back_ux_newsockfd, (char *) &bd_message, want_trans);
//                    if (act_trans < want_trans)
//                        log(LOC, "ERROR: Received %d From %d Bytes  BECAUSE %s",
//                        act_trans, want_trans, strerror(errno));
//                    else
//                        bd_work_on(back_ux_newsockfd, &bd_message);
//                    close(back_ux_newsockfd);
//                    break; /* from the while loop */
//                }
//            } /* while(1) */
//        } /* if back door */
//        /* either a timeout or a request was serviced, check stale requests */
//        check_answers();
//    } /* while(1) */
//}
//


