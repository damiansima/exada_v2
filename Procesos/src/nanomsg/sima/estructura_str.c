#include <stdio.h>

char buf[]={"\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0"};

int main()
{
    int i;
    int *val;
    
    for (i=0;i<10;i++){
        printf("%3i:'%d'\n",i, buf[i]);
    }
    
    printf("end\n");
    
    val=(int *)buf;
    *val=0x123456;
    
    for (i=0;i<10;i++){
        printf("%3i:'%x'\n",i, buf[i]);
    }
    
    printf("end\n");
    
    
    
    return 0;
}
