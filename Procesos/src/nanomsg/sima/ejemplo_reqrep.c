
#include <assert.h>

#include <string.h>
#include <time.h>
#include <unistd.h>
#include <stdio.h>

#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>

#include <fcntl.h>
#include <sys/mman.h>
#include <pthread.h>
#include <sys/types.h>

#include <nanomsg/nn.h>
#include <nanomsg/reqrep.h>


#define NODE0 "node0"
#define NODE1 "node1"
#define DATE "DATE"
#define NUM_THREADS 11

char *date() {
    time_t raw = time(&raw);
    struct tm *info = localtime(&raw);
    char *text = asctime(info);
    text[strlen(text) - 1] = '\0'; // remove '\n'
    return text;
}

void *node0(void *arg) {
    char *url = (char *) arg;
    int sz_date = strlen(DATE) + 1; // '\0' too
    int sock = nn_socket(AF_SP, NN_REP);
    assert(sock >= 0);
    
    printf("SERVER: URL %s\n", url);
    assert(nn_bind(sock, url) >= 0);
    while (1) {
        char *buf = NULL;
        int bytes = nn_recv(sock, &buf, NN_MSG, 0);
        assert(bytes >= 0);
        if (strncmp(DATE, buf, sz_date) == 0) 
        {
            printf("SERVER: RECEIVED <%s>\n",buf);
            char *d = date();
            int sz_d = strlen(d) + 1; // '\0' too
            printf("SERVER: SENDING  DATE %s\n", d);
            sleep(1);
            bytes = nn_send(sock, d, sz_d, 0);
            assert(bytes == sz_d);
        }
        nn_freemsg(buf);
    }
    nn_shutdown(sock, 0);
}

void *node1(void *arg) {
    char *url = (char *) arg;
    int sz_date = strlen(DATE) + 1; // '\0' too
    char *buf = NULL;
    int bytes = -1;
    
    pthread_t th = pthread_self();
    
    sleep(1);
    int sock = nn_socket(AF_SP, NN_REQ);
    assert(sock >= 0);
    printf("NODE%02d: URL %s\n", th, url);
    assert(nn_connect(sock, url) >= 0);
    
    char *d = date();
    int sz_d = strlen(d) + 1; // '\0' too
            
    printf("NODE%02d: SENDING  DATE REQUEST %s\n",th, d);
    
    bytes = nn_send(sock, d, sz_d, 0);
    assert(bytes == sz_d);        
//    bytes = nn_send(sock, d, sz_d, 0);
//    assert(bytes == sz_d); 
    
    printf("NODE%02d: RECEIVED DATE %s\n",th, buf);
    
    // fixme: agrego el timeout
    int to = 3000;
    assert (nn_setsockopt (sock, NN_SOL_SOCKET, NN_RCVTIMEO, &to, sizeof (to)) >= 0);
    bytes = nn_recv(sock, &buf, NN_MSG, 0);
    assert(bytes >= 0);
    printf("NODE%02d: RECEIVED DATE %s\n", th, buf);
    nn_freemsg(buf);

    nn_shutdown(sock, 0);
    return (void *) - 1;
}

int main(const int argc, const char **argv) {

    pthread_t threads[NUM_THREADS];
    int rc;
    long t;
    char url[40];
    
    strcpy(url,argv[1]);


    if (argc != 2) {
        fprintf(stderr, "Usage: pubsub <URL> \n");
        return 1;
    }

    
//    rc = pthread_create(&threads[0], NULL, node0, (void *)url);
//    if (rc) {
//        printf("ERROR; return code from pthread_create() is %d\n", rc);
//        exit(-1);
//    }
    
    
    for (t = 1; t < NUM_THREADS; t++) {
        printf("In main: creating thread %ld\n", t);
        rc = pthread_create(&threads[t], NULL, node1, (void *)url);
        if (rc) {
            printf("ERROR; return code from pthread_create() is %d\n", rc);
            exit(-1);
        }
    }

    for (t = 1; t < NUM_THREADS; t++) {
        pthread_join( threads[t], NULL ); 
    }
    pthread_cancel( threads[0]); 
    puts("done");

    return 0;

}
