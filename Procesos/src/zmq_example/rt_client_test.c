/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   rt_client_test.c
 * Author: sima
 *
 * Created on 26 de abril de 2016, 20:14
 */



#include <stdio.h>
#include <stdlib.h>
#include <zmq.h>

#include <assert.h>
#include <signal.h>
#include <stdarg.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>


#include <unistd.h>

#include "rt_structs.h"
#include <pthread.h>


int main(void)
{
    void *context = zmq_ctx_new();

    //  Socket to talk to clients
    void *cli_cmd = zmq_socket(context, ZMQ_REQ);
    //    zmq_connect (cli_cmd, "tcp://*:9001");
    zmq_connect(cli_cmd, "ipc:///tmp/rt_server.ipc");

    printf("[main] start client...\n");
    char msg[120];
    
    sprintf(msg,"Pasame los datos despues de %d", timems());
    
    int l = z_send_all(cli_cmd, "RT-RX", msg, strlen(msg)); 
 
    printf("[main] send <%s>:%d\n", msg, l);  


    rt_sleep(1);

    z_msg_t *msgreply = z_recv_all(cli_cmd); 
    if(msgreply)
    {
        printf("Received REPLY length:%i %s\n", (int)msgreply->length, msgreply->data);
        free(msgreply);
    }
    else
        printf("Received REPLY NULL \n"); 

    puts("bye..");

    zmq_close(cli_cmd);
    zmq_ctx_destroy(context);
    return 0;
}
