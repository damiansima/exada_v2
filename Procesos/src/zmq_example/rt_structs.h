/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   rt_structs.h
 * Author: sima
 *
 * Created on 27 de abril de 2016, 22:22
 */

#ifndef RT_STRUCTS_H
#define RT_STRUCTS_H

typedef double timems_t;

typedef enum
{
    type_int16 = 1, type_int32, type_int64, type_float, type_double, type_string
} tag_type;

typedef int (*funcion_t) (void *);

typedef struct
{
    int32_t id_scan;
    char data[];
} msg_h_sampler_t;

#define Z_MSG_TOPIC_LEN 10


typedef struct
{
    char topic[Z_MSG_TOPIC_LEN];
    size_t length;
    char data[];
} z_msg_t;

typedef struct
{
    struct {
        int32_t id;
        timems_t updated;
    }header;
    char value[1024];
} msg_tag_t;

#ifndef msleep
    #define msleep(x) usleep(1000*(x))
#endif




/*
 * 
 * 
 * 
 *              MOVER A UTILIDADES
 * 
 * 
 * 
 */
// __FUNCTION__
#ifndef rt_assert
#define rt_assert(validation, msg, ret) {if(!(validation)){fprintf(stderr,"%s:%u: %s\n",__FILE__, __LINE__, msg);return ret;}}
#endif

z_msg_t *z_recv_all(void *sock)
{
    char topic[Z_MSG_TOPIC_LEN];
    size_t length;
    int64_t more;
    size_t more_size = sizeof (more);
    int rc;

    memset(topic,0,Z_MSG_TOPIC_LEN);
    
    rc = zmq_recv(sock, topic, Z_MSG_TOPIC_LEN, 0);
    rt_assert(rc !=-1 , "Error zmq_recv topic", NULL );
    
    rc -= zmq_recv(sock, &length, sizeof (length), 0);
    rt_assert(rc !=-1 , "Error zmq_recv length", NULL );
    
    z_msg_t *msg = calloc(length + sizeof (z_msg_t) , 1);
    rt_assert( msg!=NULL , "Error zmq_recv length", NULL ); 
    
    memcpy(msg->topic,topic,Z_MSG_TOPIC_LEN);
    msg->length = length;
    int l = 0;
    zmq_getsockopt(sock, ZMQ_RCVMORE, &more, &more_size);
    if(!more)
    {
        free(msg);
        return NULL;
    }
    
    while (more && l < msg->length)
    {
        l = zmq_recv(sock, &msg->data[l], msg->length, 0);
        if (l == -1)
        {
            free(msg);
            return NULL;
        }
        zmq_getsockopt(sock, ZMQ_RCVMORE, &more, &more_size);
    }
    return msg;
}

int z_send_all(void *sock, char *topic, void *data, size_t length)
{
    int rc;
    char real_topic[Z_MSG_TOPIC_LEN];
    
    memset(real_topic,0,Z_MSG_TOPIC_LEN);
    if(topic) strncpy(real_topic, topic, Z_MSG_TOPIC_LEN);
    real_topic[Z_MSG_TOPIC_LEN-1]=0;
    
    rc = zmq_send(sock, real_topic, Z_MSG_TOPIC_LEN, ZMQ_SNDMORE); 
    rt_assert(rc != -1, "Error zmq_send topic", -1);
    rc = zmq_send(sock, &length, sizeof (length), ZMQ_SNDMORE); 
    rt_assert(rc != -1, "Error zmq_send length ", -1);
    rc = zmq_send(sock, data, length, 0);
    rt_assert(rc != -1, "Error zmq_send data", -1);

    return 0;
}

// Sleep in mili seconds
#ifndef rt_sleep
    #define rt_sleep(ms) usleep(1000*(ms))
#endif



timems_t timems() {
    struct timeval te; 
    gettimeofday(&te, NULL);
    timems_t milliseconds = te.tv_sec;
    milliseconds += (te.tv_usec/1.0e6);
    return milliseconds;
}






#endif /* RT_STRUCTS_H */

