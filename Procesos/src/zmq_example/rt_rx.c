/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   rt_server.c
 * Author: sima
 *
 * Created on 26 de abril de 2016, 20:14
 */



#include <stdio.h>
#include <stdlib.h>
#include <zmq.h>

#include <assert.h>
#include <signal.h>
#include <stdarg.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>


#include <unistd.h>


int main2(void)
{
    void *context = zmq_ctx_new ();

    //  Socket to talk to clients
    void *cli_cmd = zmq_socket (context, ZMQ_REQ); 
//    zmq_connect (cli_cmd, "tcp://*:9001");
    zmq_connect(cli_cmd, "ipc:///tmp/rt_server.ipc");  

    printf("[main] start client...\n"); 
    char *msg="B Hola mundo loco B";
    int l=zmq_send (cli_cmd, msg, strlen (msg), 0);
    
    printf("[main] send <%s>:%d\n",msg,l); 
    
    char buffer[255];
    l=zmq_recv (cli_cmd, buffer, 255, 0);
    buffer[l]=0;
    printf ("recv: <%s>\n", buffer);
        
    sleep(1);
    
    zmq_close (cli_cmd);
    zmq_ctx_destroy (context);
    return 0;
}


int set_value(void)
{
    void *context = zmq_ctx_new ();

    //  Socket to talk to clients
    void *cli_cmd = zmq_socket (context, ZMQ_REQ); 
//    zmq_connect (cli_cmd, "tcp://*:9001");
    zmq_connect(cli_cmd, "ipc:///tmp/rt_server.ipc");  

    printf("[main] start client...\n"); 
    char *msg="B Hola mundo loco B";
    int l=zmq_send (cli_cmd, msg, strlen (msg), 0);
    
    printf("[main] send <%s>:%d\n",msg,l); 
    
    char buffer[255];
    l=zmq_recv (cli_cmd, buffer, 255, 0);
    buffer[l]=0;
    printf ("recv: <%s>\n", buffer);
        
    sleep(1);
    
    zmq_close (cli_cmd);
    zmq_ctx_destroy (context);
    return 0;
}
 