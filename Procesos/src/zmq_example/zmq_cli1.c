/* 
 * File:   zmq_cli1.c
 * Author: nb
 *
 * Created on 23 de agosto de 2015, 13:13
 */

#include <stdio.h>
#include <stdlib.h>
#include <zmq.h>
#include <unistd.h>
#include <string.h>
#include <assert.h>  


/*
 * 
 */
int main (void)
{
    // Socket to talk to clients
    printf("Inicia el server");
    
    void *context = zmq_ctx_new ();
    void *responder = zmq_socket (context, ZMQ_REQ);

//    zmq_connect (responder, responder"inproc://clients");
//    zmq_bind (responder, "ipc:///tmp/filename");
    zmq_connect(responder, "ipc:///tmp/filename");
    
    
    zmq_send (responder, "World", 5, 0);
    char buffer [10];
    zmq_recv (responder, buffer, 4, 0);
    buffer[9]=0;
    printf ("Received: %s\n",buffer);
    
    
    zmq_close(responder);
    zmq_ctx_destroy(context);
    return 0;
}

