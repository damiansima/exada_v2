/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   sub_1.c
 * Author: sima
 *
 * Created on 25 de abril de 2016, 17:50
 */

#include <stdio.h>
#include <stdlib.h>
#include <zmq.h>

#include <assert.h>
#include <signal.h>
#include <stdarg.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>


//#include "zhelpers.h"
#include <unistd.h>


static void ms_sleep (int msecs)
{
#if (defined (WIN32))
    Sleep (msecs);
#else
    struct timespec t;
    t.tv_sec  =  msecs / 1000;
    t.tv_nsec = (msecs % 1000) * 1000000;
    nanosleep (&t, NULL);
#endif
}


int main (void)
{
    //  Prepare our context and publisher
    void *context = zmq_ctx_new ();
    void *publisher = zmq_socket (context, ZMQ_PUB);
    zmq_bind (publisher, "tcp://*:5563");
    zmq_bind (publisher, "tcp://*:5564");

    char *msg;
    while (1) {

//        msg="A";
//        zmq_send (publisher, msg, strlen (msg), ZMQ_SNDMORE);
//        msg="Hola mundo loco A";
//        zmq_send (publisher, msg, strlen (msg), 0);

//        msg="B";
//        zmq_send (publisher, msg, strlen (msg), ZMQ_SNDMORE);
        msg="B Hola mundo loco B";
        zmq_send (publisher, msg, strlen (msg), 0);
//        s_sendmore (publisher, "B");
//        s_send (publisher, "We  like to see this");
        sleep (1);
        puts("more...");
    }
    //  We never get here, but clean up anyhow
    zmq_close (publisher);
    zmq_ctx_destroy (context);
    return 0;
}