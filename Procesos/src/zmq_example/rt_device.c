/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   rt_server.c
 * Author: sima
 *
 * Created on 26 de abril de 2016, 20:14
 */



#include <stdio.h>
#include <stdlib.h>
#include <zmq.h>

#include <assert.h>
#include <signal.h>
#include <stdarg.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>


//#include "zhelpers.h"
#include <unistd.h>

#include <sqlite3.h>

//#include "zhelpers.h"
#include "rt_structs.h"
#include "logger.h"
#include "lista_continua.h"
#include <pthread.h>


void *context = NULL;
int device_id = 1;
pthread_mutex_t mylock = PTHREAD_MUTEX_INITIALIZER;

#define SCAN_DATA_TYPE int16_t

typedef struct
{
    int new_val_offset;
    msg_tag_t old_val;
} _tags_t;

typedef struct
{
    int id;
    int reg_start;
    int reg_end;
    int reg_length;
    int data_length;
    SCAN_DATA_TYPE *data_scan;
    msg_tag_t *tags;
    int *tags_addres;
    int *tags_type;
    int *tags_scale;
} scans_tags_t;

typedef struct
{
    int id;
    int reg_start;
    int reg_end;
    int reg_length;
    int data_length;
    int data_type_length;
} scans_block_t;

typedef struct
{
    int scan_id;
    int id;
    int ref_address;
    int scale;
    char type[4];
    int length;
    timems_t update;
    char value[1024];
} tag_block_t;



#define valPointer(type, point, index)  ((type)(*(((type *)point) + index )))
#define Pointer(pointer, index)  (((void *)pointer) + index )

//_sampler_tags_t sampler;

char *get_endiand_value(SCAN_DATA_TYPE *src, int index, int len_value)
{
    char *value = calloc(1, len_value / sizeof (char));

    // TODO: completar esto con los tipos de datos y con validaciones
    memcpy(value, &src[index], len_value);
    return value;
}

static void *fn_sampler_data(void *args)
{
    //    scans_tags_t *sampler = args;
    LIST_t *l_sampler = args;
    uint64_t thr_id = pthread_self();
    char endpoint[256];
    int sampler_id = 0;

    LIST_t *l_pub_rt_srv = list_alloc(1024);

    LIST_BLOCK *lb_tag = list_first_block(l_sampler);
    if (lb_tag == NULL)
    {
        log_error("[%llu] No puede continuar porque no hay tags para samplear",
                  thr_id);
        pthread_detach(thr_id);
        return NULL;
    }
    tag_block_t *tag = (tag_block_t *) lb_tag->data;
    sampler_id = tag->scan_id;

//    pthread_mutex_lock( &mylock ); 
    sprintf(endpoint, "inproc://fn_sampler_data_%d", sampler_id);

    void *sock = zmq_socket(context, ZMQ_PULL);
    zmq_connect(sock, endpoint);

    log_info("[%llu] fn_sampler_data sampler_id:%d starting...", thr_id, sampler_id);
    log_info("[%llu] Pull in:%s", thr_id, endpoint);

//    pthread_mutex_unlock( &mylock );
    while (sampler_id==9)
    {
        z_msg_t *msg = z_recv_all(sock);
        if (!msg)
        {
            log_warning("[%llu] Received request NULL ", thr_id);
            continue;
        }

        if (strcmp(msg->topic, "SCAN"))
        {
            log_warning("[%llu] Received bad topic ", thr_id);
            continue;
        }

        log_debug(1,"[%llu] Received request topic:%s length:%i %s ", thr_id,
               msg->topic, (int) msg->length, msg->data);

        timems_t update = timems();
        SCAN_DATA_TYPE *data_scan = msg->data;

        list_init( l_pub_rt_srv, list_memlen(l_pub_rt_srv) );
            
        lb_tag = list_first_block(l_sampler);
        while (lb_tag != NULL)
        {
            tag = (tag_block_t *) lb_tag->data; 

            char *new_value = get_endiand_value(data_scan, tag->ref_address, tag->length);

            if (memcmp(new_value, tag->value, tag->length) || tag->update == 0)
            {
                memcpy(tag->value, new_value, tag->length);
                tag->update = update;
                log_debug(1,"[%llu] add TAG: id:%d time:%.2f value:%d \n",
                       thr_id, tag->id, tag->update, valPointer(int16_t,tag->value,0) );
                // TODO: agregar a la lista de tags para enviar.
            }
            lb_tag = list_next_block(l_sampler, lb_tag);
        }
        free(msg);

        // TODO: enviar el dato a rt_server
        // z_send_all(sock_rt_server, "SAMPLER", l_rt_server, length_memory(l_rt_server));
        list_add_raw_alloc(&l_pub_rt_srv, tag->id, tag, sizeof(tag) ); 

    }

    log_info("[%llu] fn_sampler_data sampler_id:%d end.", thr_id, sampler_id );
    free(l_sampler);
    pthread_detach(thr_id);
    return NULL;
}


//static void *fn_srv_worker (void *context) {
//    //  Socket to talk to dispatcher
//    pthread_t id= pthread_self();
//    void *receiver = zmq_socket (context, ZMQ_REP);
//    zmq_connect (receiver, "inproc://fn_srv_command");
//
//    printf("[%ul] fn_srv_worker starting...\n", id);
//    
//    while (1) {
//        char *string = s_recv (receiver);
//        printf ("[%ul] Received request: [%s]\n", id, string);
//        free (string);
//        //  Do some 'work'
//        sleep (1);
//        //  Send reply back to client
//        s_send (receiver, "World");
//    }
//    zmq_close (receiver);
//    printf("[%ul] fn_srv_worker end...\n", id);
//    return NULL;
//}

int main(void)
{

    context = zmq_ctx_new();
    //  Socket to talk to clients
    //    void *srv_cmd = zmq_socket(context, ZMQ_ROUTER);
    //    //    zmq_bind (srv_cmd, "tcp://*:9001");
    //    zmq_bind(srv_cmd, "ipc:///tmp/rt_server.ipc");


    log_set_file("/tmp/esampler.log");
    log_debug_level(255);
    log_logger_level(255); 



    sqlite3 *db;
    int ret;
    sqlite3_stmt *stmt;
    sqlite3_stmt *stmt2;
    char sql[2048];

    memset(sql, 0, 100);

    sql[0] = 25;
    sql[1] = 24;
    sql[2] = 26;
    sql[3] = 12;
    sql[4] = 10;
    sql[8] = 21;

    int i = valPointer(long long, sql, 1);

//
//    log_info("prueba valPointer: %lld -- %d  sizeof(%d)", valPointer(long long, sql, 0), i,
//             sizeof (valPointer(long long, sql, 1)));
//
//    msg_tag_t T;
//    T.value.i64 = 0x1122334455667788;
//    puts("no se si esto funciona directo por el tema del endinad.");
//    printf("T.value.i64:            0x%016llX\n", T.value.i64);
//    printf("T.valPointer int32_t 0: 0x%016X\n", valPointer(int32_t, &T.value, 0));
//    printf("T.valPointer int32_t 1: 0x%08X\n", valPointer(int32_t, &T.value, 1));
//    printf("T.valPointer int64_t 0: 0x%016llX\n", valPointer(int64_t, &T.value, 0));
//    printf("T.valPointer int16_t 3: 0x%04X\n", valPointer(int16_t, &T.value, 3));
//    printf("T.value.i16:            0x%04X\n", T.value.i16);
//    printf("T.value.i32:            0x%08X\n", T.value.i32);
//    printf("T.value.i64:            0x%016X\n", T.value.i64);


    ret = sqlite3_open("/home/sima/Proyectos/exada_v2/cfg/DBConfig.sqlite", &db);

    LIST_BLOCK *lb;
    scans_block_t *scan;
    LIST_t *l_scans = list_alloc(256);

    sprintf(sql, "select id, block_start,block_end, register_length "
            "from scans where device_id=%d order by id asc", device_id);

    //select s.id as scan_id,s.block_start,s.block_end,s.register_length, t.id as tag_id,t.name, t.address,t.device_scale,t.value_type
    //from tags t
    //join scans s on s.device_id=t.device_id and t.address>=s.block_start and t.address<s.block_end
    //where s.device_id=1  and s.id=1
    //;    

    ret = sqlite3_prepare(db, sql, strlen(sql) + 1, &stmt, NULL);
    ret = sqlite3_step(stmt);
    while (ret == SQLITE_ROW)
    {
        lb = list_add_alloc(&l_scans, 0, sizeof (scans_block_t));
        scan = (scans_block_t *) lb->data;

        scan->id = sqlite3_column_int(stmt, 0);
        scan->reg_start = sqlite3_column_int(stmt, 1);
        scan->reg_end = sqlite3_column_int(stmt, 2);
        scan->reg_length = sqlite3_column_int(stmt, 3);
        scan->data_length = scan->reg_end - scan->reg_start;
        scan->data_type_length = sizeof (SCAN_DATA_TYPE);

        /*
         * 
         * busca los tags asociados al bloque de lectura en cuestion
         * 
         */
        sprintf(sql, "select t.id ,t.name, t.address,t.device_scale, 16 as value_length, t.value_type "
                "from tags t "
                "join scans s on s.device_id=t.device_id and t.address>=s.block_start and t.address<s.block_end "
                "where s.device_id=%d and s.id=%d"
                , device_id, scan->id);
        ret = sqlite3_prepare(db, sql, strlen(sql) + 1, &stmt2, NULL);
        ret = sqlite3_step(stmt2);
        if (ret == SQLITE_ROW)
        {
            LIST_t *l_sampler = list_alloc(256);

            while (ret == SQLITE_ROW)
            {
                lb = list_add_alloc(&l_sampler, 0, sizeof (tag_block_t));
                tag_block_t *tag = (tag_block_t *) lb->data;

                tag->scan_id = scan->id;
                tag->id = sqlite3_column_int(stmt2, 0);
                char *tName = sqlite3_column_text(stmt2, 1);
                int tAddress = sqlite3_column_int(stmt2, 2);
                tag->ref_address = tAddress - scan->reg_start;
                tag->scale = sqlite3_column_int(stmt2, 3);
                tag->length = sqlite3_column_int(stmt2, 4);
                strncpy(tag->type, (char *)sqlite3_column_text(stmt2, 5), sizeof (tag->type));

                printf("- %4d:%s \n       Ubicado en scan:%d address:%d ref_address:%d "
                       "scale:%d\n",
                       tag->id, tName, tag->scan_id, tAddress, tag->ref_address,
                       tag->scale);

                ret = sqlite3_step(stmt2);
            }
            sqlite3_finalize(stmt2);

            pthread_t worker;
            pthread_create(&worker, NULL, fn_sampler_data, (void *) l_sampler);
        }


        ret = sqlite3_step(stmt);
    }
    sqlite3_finalize(stmt);
    sqlite3_close(db);


    msleep(10000);
    pthread_exit(0);
    puts("bye..");

    //    //  Socket to talk to workers
    //    void *srv_workers = zmq_socket(context, ZMQ_DEALER);
    //    zmq_bind(srv_workers, "inproc://fn_srv_command");
    //
    //    //  Launch pool of worker threads
    //    int thread_nbr;
    //    for (thread_nbr = 0; thread_nbr < 5; thread_nbr++)
    //    {
    //        pthread_t worker;
    //        pthread_create(&worker, NULL, fn_sampler_data, context);
    //    }
    //    //  Connect work threads to client threads via a queue proxy
    //    printf("[main] start proxy...\n");
    //    zmq_proxy(srv_cmd, srv_workers, NULL);
    //
    //    //  We never get here, but clean up anyhow
    //    zmq_close(srv_cmd);
    //    zmq_close(srv_workers);
    zmq_ctx_destroy(context);
    return 0;
}


//
//int main2(void)
//{
//    //  Prepare our context and publisher
//    void *context = zmq_ctx_new ();
//    void *publisher = zmq_socket (context, ZMQ_PUB);
//    zmq_bind (publisher, "tcp://*:5563");
//    zmq_bind (publisher, "tcp://*:5564");
//
//    char *msg;
//    while (1) {
//
////        msg="A";
////        zmq_send (publisher, msg, strlen (msg), ZMQ_SNDMORE);
////        msg="Hola mundo loco A";
////        zmq_send (publisher, msg, strlen (msg), 0);
//
////        msg="B";
////        zmq_send (publisher, msg, strlen (msg), ZMQ_SNDMORE);
//        msg="B Hola mundo loco B";
//        zmq_send (publisher, msg, strlen (msg), 0);
////        s_sendmore (publisher, "B");
////        s_send (publisher, "We  like to see this");
//        sleep (1);
//        puts("more...");
//    }
//    //  We never get here, but clean up anyhow
//    zmq_close (publisher);
//    zmq_ctx_destroy (context);
//    return 0;
//}