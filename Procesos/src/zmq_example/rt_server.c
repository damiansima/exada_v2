/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   rt_server.c
 * Author: sima
 *
 * Created on 26 de abril de 2016, 20:14
 */



#include <stdio.h>
#include <stdlib.h>
#include <zmq.h>

#include <assert.h>
#include <signal.h>
#include <stdarg.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>


//#include "zhelpers.h"
#include <unistd.h>

//#include "zhelpers.h"
#include "rt_structs.h"
#include <pthread.h>
 

static void *fn_srv_command (void *context) {
    //  Socket to talk to dispatcher
    uint64_t thr_id= pthread_self();
    
    void *sock = zmq_socket (context, ZMQ_REP);
    zmq_connect (sock, "inproc://fn_srv_command");

    printf("[%llu] fn_srv_command starting...\n", thr_id); 
    
    char buffer[255];
    while(1) {
//        zmq_recv(sock,buffer,255,0);
//        if(strlen(&buffer[sizeof(size_t)]))
//            printf ("[%llu] Received request data:%s\n", thr_id, &buffer[sizeof(size_t)]);
        z_msg_t *msg = z_recv_all(sock);  
        if(msg){
            printf ("[%llu] Received request topic:%s length:%i %s\n", thr_id, msg->topic, (int)msg->length, msg->data);
            free (msg);
        }
        else
            printf ("[%llu] Received request NULL \n", thr_id);
        
        // reply
        z_send_all(sock, NULL, "OK", 2 );   
    }
    
    pthread_detach(thr_id);
    return NULL;
}


//static void *fn_srv_worker (void *context) {
//    //  Socket to talk to dispatcher
//    pthread_t id= pthread_self();
//    void *receiver = zmq_socket (context, ZMQ_REP);
//    zmq_connect (receiver, "inproc://fn_srv_command");
//
//    printf("[%ul] fn_srv_worker starting...\n", id);
//    
//    while (1) {
//        char *string = s_recv (receiver);
//        printf ("[%ul] Received request: [%s]\n", id, string);
//        free (string);
//        //  Do some 'work'
//        sleep (1);
//        //  Send reply back to client
//        s_send (receiver, "World");
//    }
//    zmq_close (receiver);
//    printf("[%ul] fn_srv_worker end...\n", id);
//    return NULL;
//}

int main (void)
{
    void *context = zmq_ctx_new ();

    
    
    while (1){
        printf("timems:%f \n", timems()) ;
        msleep(100);
    }
    //  Socket to talk to clients
    void *srv_cmd = zmq_socket (context, ZMQ_ROUTER);
//    zmq_bind (srv_cmd, "tcp://*:9001");
    zmq_bind (srv_cmd, "ipc:///tmp/rt_server.ipc");

    //  Socket to talk to workers
    void *srv_workers = zmq_socket (context, ZMQ_DEALER);
    zmq_bind (srv_workers, "inproc://fn_srv_command");

    //  Launch pool of worker threads
    int thread_nbr;
    for (thread_nbr = 0; thread_nbr < 5; thread_nbr++) {
        pthread_t worker;
        pthread_create (&worker, NULL, fn_srv_command, context);
    }
    //  Connect work threads to client threads via a queue proxy
    printf("[main] start proxy...\n"); 
    zmq_proxy (srv_cmd, srv_workers, NULL);

    //  We never get here, but clean up anyhow
    zmq_close (srv_cmd);
    zmq_close (srv_workers);
    zmq_ctx_destroy (context);
    return 0;
}
 

//
//int main2(void)
//{
//    //  Prepare our context and publisher
//    void *context = zmq_ctx_new ();
//    void *publisher = zmq_socket (context, ZMQ_PUB);
//    zmq_bind (publisher, "tcp://*:5563");
//    zmq_bind (publisher, "tcp://*:5564");
//
//    char *msg;
//    while (1) {
//
////        msg="A";
////        zmq_send (publisher, msg, strlen (msg), ZMQ_SNDMORE);
////        msg="Hola mundo loco A";
////        zmq_send (publisher, msg, strlen (msg), 0);
//
////        msg="B";
////        zmq_send (publisher, msg, strlen (msg), ZMQ_SNDMORE);
//        msg="B Hola mundo loco B";
//        zmq_send (publisher, msg, strlen (msg), 0);
////        s_sendmore (publisher, "B");
////        s_send (publisher, "We  like to see this");
//        sleep (1);
//        puts("more...");
//    }
//    //  We never get here, but clean up anyhow
//    zmq_close (publisher);
//    zmq_ctx_destroy (context);
//    return 0;
//}