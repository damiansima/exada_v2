/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   sub_1.c
 * Author: sima
 *
 * Created on 25 de abril de 2016, 17:50
 */

#include <stdio.h>
#include <stdlib.h>
#include <zmq.h>

#include <assert.h>
#include <signal.h>
#include <stdarg.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

//  Pubsub envelope subscriber

#include "zhelpers.h"

int main (void)
{
    //  Prepare our context and subscriber
    void *context = zmq_ctx_new ();
    void *subscriber = zmq_socket (context, ZMQ_SUB);
    zmq_connect (subscriber, "tcp://localhost:5563");
    zmq_connect (subscriber, "tcp://localhost:5564");
    zmq_setsockopt (subscriber, ZMQ_SUBSCRIBE, "B", 1);

    char buffer[255];
    int l;
    
    while (1) {
        //  Read envelope with address 
//        char *address = s_recv (subscriber);
        l=zmq_recv (subscriber, buffer, 255, 0);
        buffer[l]=0;
        //  Read message contents
//        char *contents = s_recv (subscriber);
        printf ("[%s] XXX\n", buffer);
//        free (address);
//        free (contents);
    }
    //  We never get here, but clean up anyhow
    zmq_close (subscriber);
    zmq_ctx_destroy (context);
    return 0;
}