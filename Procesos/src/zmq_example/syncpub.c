/* 
 * File:   syncpub.c
 * Author: nb
 *
 * Created on 23 de agosto de 2015, 16:54
 */

#include <stdio.h>
#include <stdlib.h>
#include "zhelpers.h"
#define SUBSCRIBERS_EXPECTED  1000  ////  We wait for 10 subscribers //

int main2(void) {
    void *ctx = zmq_ctx_new();
    assert(ctx);
    /* Create ZMQ_STREAM socket */
    void *socket = zmq_socket(ctx, ZMQ_STREAM);
    assert(socket);
    int rc = zmq_bind(socket, "tcp://*:8888");
//    int rc = zmq_bind(socket, "ipc:///tmp/filename");
    assert(rc == 0);
    /* Data structure to hold the ZMQ_STREAM ID */
    uint8_t id [256];
    size_t id_size = 256;
    while (1) {
        /* Get HTTP request; ID frame and then request */
        id_size = zmq_recv(socket, id, 256, 0);
        assert(id_size > 0);
        /* Prepares the response */
        char http_response [] =
                "HTTP/1.0 200 OK\r\n"
                "Content-Type: text/plain\r\n"
                "\r\n"
                "Hello, World!";
        /* Sends the ID frame followed by the response */
        zmq_send(socket, id, id_size, ZMQ_SNDMORE);
        zmq_send(socket, http_response, strlen(http_response), ZMQ_SNDMORE);
        /* Closes the connection by sending the ID frame followed by a zero response */
        zmq_send(socket, id, id_size, ZMQ_SNDMORE);
        zmq_send(socket, 0, 0, ZMQ_SNDMORE);
        /* NOTE: If we don't use ZMQ_SNDMORE, then we won't be able to send more */
        /* message to any client */
    }
    zmq_close(socket);
    zmq_ctx_destroy(ctx);
}
//  Synchronized publisher

int main(void) {
    void *context = zmq_ctx_new();

    //  Socket to talk to clients
    void *publisher = zmq_socket(context, ZMQ_PUB);

    int sndhwm = 1100000;
    zmq_setsockopt(publisher, ZMQ_SNDHWM, &sndhwm, sizeof (int));

    zmq_bind(publisher, "tcp://*:5561");

    //  Socket to receive signals
    void *syncservice = zmq_socket(context, ZMQ_REP);
    zmq_bind(syncservice, "tcp://*:5562");

    //  Get synchronization from subscribers
    printf("Waiting for subscribers\n");
    int subscribers = 0;
    while (subscribers < SUBSCRIBERS_EXPECTED) {
        //  - wait for synchronization request
        char *string = s_recv(syncservice);
        free(string);
        //  - send synchronization reply
        s_send(syncservice, "");
        subscribers++;
    }
    //  Now broadcast exactly 1M updates followed by END
    printf("Broadcasting messages\n");
    int update_nbr;
    for (update_nbr = 0; update_nbr < 1000000; update_nbr++)
        s_send(publisher, "Rhubarb");

    s_send(publisher, "END");

    zmq_close(publisher);
    zmq_close(syncservice);
    zmq_ctx_destroy(context);
    return 0;
}
