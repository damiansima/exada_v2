/* 
 * File:   syncsub.c
 * Author: nb
 *
 * Created on 23 de agosto de 2015, 16:56
 */

#include <stdio.h>
#include <stdlib.h>
#include "zhelpers.h"



int main(void) {
    void *ctx = zmq_ctx_new();
    assert(ctx);
    /* Create ZMQ_STREAM socket */
    void *socket = zmq_socket(ctx, ZMQ_STREAM);
    assert(socket);
    int rc = zmq_connect(socket, "tcp://127.0.0.1:8888");
//    int rc = zmq_connect(socket, "ipc:///tmp/filename");
    if (rc != 0) 
        fprintf(stderr,"%s\n", zmq_strerror(zmq_errno()));
    fflush(stderr);
    assert(rc == 0);
    
    
    uint8_t id [256];
    size_t id_size = 256;
    rc = zmq_getsockopt (socket, ZMQ_IDENTITY, id, &id_size);
    assert (rc == 0);
    puts(id);
    
    char query[]= "Hello, World!\n";
    

    /* Sends the ID frame followed by the response */
    zmq_send(socket, id, id_size, ZMQ_SNDMORE);
    zmq_send(socket, query, strlen(query), ZMQ_SNDMORE);
    /* Closes the connection by sending the ID frame followed by a zero response */
    zmq_send(socket, id, id_size, ZMQ_SNDMORE);
    zmq_send(socket, 0, 0, ZMQ_SNDMORE);

    
    id_size = zmq_recv(socket, id, 256, 0);
    
    zmq_close(socket);
    zmq_ctx_destroy(ctx);
}


//  Synchronized subscriber
int main2 (void)
{
    void *context = zmq_ctx_new ();

    //  First, connect our subscriber socket
    void *subscriber = zmq_socket (context, ZMQ_SUB);
    zmq_connect (subscriber, "tcp://*:5561");
    zmq_setsockopt (subscriber, ZMQ_SUBSCRIBE, "", 0);

    //  0MQ is so fast, we need to wait a while…
    sleep (1);

    //  Second, synchronize with publisher
    void *syncclient = zmq_socket (context, ZMQ_REQ);
    zmq_connect (syncclient, "tcp://*:5562");

    //  - send a synchronization request
    s_send (syncclient, "");

    //  - wait for synchronization reply
    char *string = s_recv (syncclient);
    free (string);

    //  Third, get our updates and report how many we got
    int update_nbr = 0;
    while (1) {
        char *string = s_recv (subscriber);
        if (strcmp (string, "END") == 0) {
            free (string);
            break;
        }
        free (string);
        update_nbr++;
    }
    printf ("Received %d updates\n", update_nbr);

    zmq_close (subscriber);
    zmq_close (syncclient);
    zmq_ctx_destroy (context);
    return 0;
}
