/* 
 * File:   scan_send_test.c
 * Author: nb
 *
 * Created on 10 de septiembre de 2015, 21:56
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <logger.h>
#include <string.h>
#include <jmorecfg.h>
#include <stdint.h>
#include "utilidades.h"

typedef struct {
    int id;
    int registro;
    short length;
    char type;
} scan_tag;

/* Estructura para dispositivo
 */
typedef struct {
    int id; // ID de configuracion
    char *driver; // Driver usado: ModBusRTU ModBusTCP, etc.
    char *direccion; // Direccion del sispositivo: addres (ModBusRTU) direccion ip (ModBusTCP)
    char *puerto; // Puerto /dev/ser1 (ModBusRTU) puerto TCP (ModBusTCP)
    int fd; // puntero del dispositivo
    int timeout; // timeout de lectura
} ex_scan_device_t;

/* Configuracion del scan
 * 
 * para consultar al campo
 *  - registro inicial
 *  - longitud del bloque a escanear
 * 
 * para guardar el dato
 *  - memoria malloc() donde guardar los datos
 *  - timems_t de la ultima actualizacion
 * 
 */
typedef struct {
    int reg_start;
    int block_length;
    ex_scan_device_t *device;
    char *data;
    timems_t time;
} ex_scan_memory_t;



/* Caso especial para modbus RTU
 */
int mb_rtu_init(char *device);
int mb_rtu_scan(int fd, int direccion, int reg_start, int reg_len, int16_t *data_dest, int timeout);
int mb_rtu_write(int fd, int direccion, int reg_start, int reg_count, int16_t *data_orig, int timeout);
int mb_rtu_close(int fd);

int mb_rtu_scan_simulador(int fd, int direccion, int reg_start, int reg_len, int16_t *data_dest, int timeout) {
    int i;
    
    printf("aca 1 %d\n",reg_len);
    for (i = 0; i < reg_len; i++) {
        data_dest[i] = (int16_t) random() % 1000;
    }

    if (reg_len > 30){
        puts("aca");
        strcpy((char *) &data_dest[4], "hola mundo desde scan");
    }


    msleep(10);
    return TRUE;
}

int init_device(ex_scan_device_t *device, int id, char *direccion, char *driver,
        char *puerto, int timeout) {

    device->id = id;
    device->direccion = strdup(direccion);
    device->driver = strdup(driver);
    device->puerto = strdup(puerto);
    device->timeout = timeout;

    return TRUE;
}

int init_scan_block(ex_scan_memory_t *mem, ex_scan_device_t *device,
        int reg_start, int block_length) {

    mem->reg_start = reg_start;
    mem->device = device;
    mem->block_length = block_length;
    mem->data = malloc((sizeof(int16_t)/sizeof(char))*block_length + 1);

    return TRUE;
}

int do_scan_block(ex_scan_memory_t *mem) {
    int ret;
    ret = mb_rtu_scan_simulador(mem->device->fd, atoi(mem->device->direccion),
            mem->reg_start, mem->block_length, (int16_t *) mem->data, 
            mem->device->timeout);

    if (ret == TRUE) {
        mem->time = timems();
    }
    return ret;
}

int main(int argc, char *argv[]) {

    
    ex_scan_memory_t mem_simulacion;
    ex_scan_device_t dev;
    
    
    log_debug_level(LOG_ALERT | LOG_ERROR | LOG_MSG | LOG_INFO | LOG_WARNING);
    log_logger_level(0);


    log_info("[SCAN_SIMULADOR] init_scan_block ");
    
    init_scan_block( &mem_simulacion, &dev, 34343, 31);

    while(TRUE){
        log_info("[SCAN_SIMULADOR] do_scan_block ");
        do_scan_block( &mem_simulacion );

        log_info("[SCAN_SIMULADOR] print ");
        for (int i = 0; i < 200*2; i++) {
                printf("%c", ((char *)mem_simulacion.data)[i] ); 
        }
        puts("");
        sleep(1);
    }
    log_info("[SCAN_SIMULADOR] end");
    return 0;
}

