/* 
 * File:   mmap_generate.c
 * Author: nb
 *
 * Created on 9 de septiembre de 2015, 21:44
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <logger.h>
#include <string.h>
#include "utilidades.h"




#define FILEPATH "/tmp/mmapped.bin"
#define NUMINTS  (100)
#define FILESIZE (NUMINTS * sizeof(char))


int main(int argc, char *argv[]) {
    int i;
    char *map; /* mmapped array of int's */

    log_debug_level(LOG_ALERT | LOG_ERROR | LOG_MSG | LOG_INFO | LOG_WARNING);
    log_logger_level(0);


    log_info("[MMAP_READ] init ");


    int len = MAX( fsize(FILEPATH), FILESIZE ); 
    map = (char *)mmap_file(FILEPATH, "rw", len);

    
    // escribe datos
    log_info("[MMAP_READ] imprime datos ");
    for (i = 0; i < NUMINTS; i++) {
        printf("%c",map[i]);
    }
    log_info("[MMAP_READ] end ");

    if (munmap(map, len) == -1) {
        perror("Error un-mmapping the file"); 
    }

    return 0;
}

