/* 
 * File:   utilidades.h
 * Author: nb
 *
 * Created on 9 de septiembre de 2015, 23:15
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <string.h>



/*
 * FIXME: Meter en librearia
 */
#define MAX(a,b) (((int)(a)>(int)(b))?(a):(b))
#define MIN(a,b) (((int)(a)<(int)(b))?(a):(b))



int fsize(char *filename) {
    int size;
    FILE* fh;

    fh = fopen(filename, "rb"); //binary mode
    if (fh != NULL) {
        if(fseek(fh, 0, SEEK_END)) {
            fclose(fh);
            return -1;
        }
        size = ftell(fh);
        fclose(fh);
        return size;
    }
    return -1; //error
}



void *mmap_file(char *filename, char *mode, int length) {
    int fd=-1;
    void *map=NULL;
    
    
    if( strstr(mode,"w") ) {
        fd = open(filename, O_RDWR | O_CREAT , (mode_t) 0600);
        if (fd == -1) {
            perror("Error opening file writer");
            return NULL;
        }
        if (ftruncate(fd, length)) {
            perror("Resizing the file failed");
            close(fd);
            return NULL;
        }
    }
    else if( strstr(mode,"r") ){
        fd = open(filename, O_RDONLY, (mode_t) 0600);
        if (fd == -1) {
            perror("Error opening file");
            return NULL;
        }
    }

    // mapear el archivo en memoria
    if( strstr(mode,"w") ){
        map = mmap(0, length, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    }
    else if( strstr(mode,"r") ){
        map = mmap(0, length, PROT_READ, MAP_SHARED, fd, 0);
    }
    
    close(fd);

    if (map == MAP_FAILED) {
        perror("Error mmapping the file");
        return NULL;
    }

    return map;
}

void unmap_file(void *map, int len) {
    if (munmap(map, len) == -1) {
        perror("Error un-mmapping the file");
    }
    return;
}



typedef long long timems_t;

timems_t timems() {
    struct timeval te; 
    timems_t milliseconds; 
    gettimeofday(&te, NULL);
    milliseconds = te.tv_sec*1000LL;
    milliseconds += te.tv_usec/1000LL;
    return milliseconds;
}

#ifndef msleep
    #define msleep(x) usleep(1000*(x))
#endif




typedef int (*hand_thr) (void *);

typedef struct {
    pthread_t thr_function;
    pthread_mutex_t mutex;
    pthread_cond_t cond;
    int timer;
    hand_thr function;
    void *param;
    int retur;
} ipc_thr;

void *thr_handler(void *param) {
    ipc_thr *thr = (ipc_thr *) param;
    // printf("--- thr_handler()\n");
    thr->retur = thr->function(thr->param);
    pthread_cond_signal(&thr->cond);
    pthread_mutex_unlock(&thr->mutex);
    // printf("--- thr_handler() exit !\n");
    pthread_detach(pthread_self());
    return NULL;
}

int thread_create_timeout(hand_thr function, int timer, void *param) {
    int rc;
    struct timespec ts;
    struct timeval tp;
    ipc_thr thr;
    memset(&thr, 0, sizeof (ipc_thr));

    thr.function = function;
    thr.timer = timer;
    thr.param = param;

    // printf("- thread_create_timeout()\n");
    pthread_create(&thr.thr_function, NULL, thr_handler, &thr);

    rc = gettimeofday(&tp, NULL);
    tp.tv_sec = tp.tv_sec + thr.timer / 1000;
    tp.tv_usec = tp.tv_usec + 1000 * (thr.timer % 1000);
    if (tp.tv_usec >= 1000000) {
        ++tp.tv_sec;
        tp.tv_usec -= 1000000;
    }
    ts.tv_sec = tp.tv_sec;
    ts.tv_nsec = tp.tv_usec * 1000;

    // printf("- Thread blocked\n");
    rc = pthread_cond_timedwait(&thr.cond, &thr.mutex, &ts);
    if (rc == ETIMEDOUT) {
        printf("- Timed Out!\n");
        pthread_cancel(thr.thr_function);
        return -1;
    } else
        return thr.retur;
}
