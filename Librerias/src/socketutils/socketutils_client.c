#include <stdio.h>
#include <stdlib.h>

#include "logger.h"
#include "socketutils.h"


int main( void )
{
    log_debug_level(LOG_ALERT|LOG_ERROR|LOG_MSG|LOG_INFO|LOG_WARNING);
    log_logger_level(0); 
    
    
    log_info("[CLI] Prueba socketutils %s","[yea string]"); 
    
    char ip[15];
    log_info("[CLI] get_ip(sima-nb): %s ",get_ip(ip,"sima-nb"));
    
    
    
    int port=6556, sock;
    char *msg="hola mundo desde [CLI]\n";
    
    
    sock = connect_tcp(ip, port);
    log_info("[CLI] connect_tcp port: %d sock:%d",port,sock);
    
    sleep(1);
    sendall(sock,msg,strlen(msg));
    sleep(1);
    sendall(sock,msg,strlen(msg));
    sleep(3);
    
    log_info("[CLI] done...",port,sock);
    
    return EXIT_SUCCESS;
}
  