#include <stdio.h>
#include <sys/types.h>
#include <errno.h>

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>

#include <signal.h>

#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/time.h>                          
#include <sys/select.h>
#include <poll.h>
#include <assert.h>



#include <string.h>
#include "socketutils.h"

int sendall(int sock, char *buf, int len) {
    int sent, tmpres;
    sent = 0;
    while (len > 0) {
        tmpres = send(sock, buf + sent, len, 0);
        if (tmpres == -1) {
            fprintf(stderr, "Error: Can't send query errno:%d - %s \n", errno, strerror(errno));
            return -1;
        }
        sent += tmpres;
        len -= tmpres;
    }
    return len;
}

int http_get(int sock, char *host, char *url) {
#define _USERAGENT "HTMLGET 1.1"

    char *query;
    int resp;
    static char *tpl = "GET %s HTTP/1.1\r\nHost: %s\r\nUser-Agent: %s\r\n\r\n ";

    query = (char *) malloc(strlen(host) + strlen(url) + strlen(_USERAGENT) + strlen(tpl));
    sprintf(query, tpl, url, host, _USERAGENT);
    resp = sendall(sock, query, strlen(query));
    free(query);
    return resp;
}

int http_post_xml(int sock, char *host, char *url, char *strXml) {
    char *query;
    int resp;
    static char *tpl = "POST %s HTTP/1.1"\
                   "\r\nHost: %s"\
                   "\r\nContent-Type: application/x-www-form-urlencoded"\
                   "\r\nContent-Length: %i"\
                   "\r\n\r\nstrXml=%s\r\n ";

    query = (char *) malloc(strlen(host) + strlen(url) + strlen(strXml) + strlen(tpl) + 10);
    sprintf(query, tpl, url, host, strlen(strXml), strXml);
    resp = sendall(sock, query, strlen(query));
    free(query);
    return resp;
}

int readhttp(int sock, char **buf) {
    int length, tmpres;

    *buf = malloc(1000);
    // primero lee la cabecera hasta que encuentra el \r\n    
    length = 0;
    tmpres = readline(sock, *buf, 1000, 5);
    while (tmpres) {
        // printf ("--%s",*buf);
        if (strncmp(*buf, "Content-Length: ", strlen("Content-Length: ")) == 0) {
            length = atoi(*buf + strlen("Content-Length: "));
        }
        // busca la linea sola para fin de cabezera
        if (strcmp(*buf, "\r\n") == 0)
            break;
        tmpres = readline(sock, *buf, 1000, 5);
    }
    if (length == 0)
        return -1; // no se lee la longitud

    // printf ("\nlength: %i ",length);
    *buf = realloc(*buf, length + 1);
    if (buf == 0)
        return -2; // error en malloc

    memset(*buf, 0, length);
    tmpres = readall(sock, *buf, length, 5);
    return tmpres;
}

/**
 * int readline( int sock, char *buf, int maxlen, int timeseg  )
 * 
 * lee una linea delimitada por \n o hasta igual a maxlen;  
 * 
 * @param sock: sock TCP
 * @param buf: bufer donde guardar los datos leidos
 * @param maxlen: longitud maxima a leer
 * @param timesg: tiempo maximo en segundos para empezar a leer  
 * @return: longitud del string leido hasta un \n o timedout
 *          -1 si occure un error
 */
int readline(int sock, char *bufptr, int len, int timemseg) {
    char *bufx = bufptr;
    char c;
    fd_set refd;
    int n, cnt;
    struct timeval tv, tv2;

    tv2.tv_sec = timemseg / 1000;
    tv2.tv_usec = 1000 * (timemseg % 1000);

    while (--len > 0) {
        FD_ZERO(&refd);
        FD_SET(sock, &refd);

        tv.tv_sec = tv2.tv_sec;
        tv.tv_usec = tv2.tv_usec;

        n = select(1 + sock, &refd, NULL, NULL, &tv);
        switch (n) {
            case -1: // error   
                fprintf(stderr, "Error select() errno:%d - %s \n", errno, strerror(errno));
                return -1;
            case 0: // timeout, entoces ya no hay mas nada para leer
                return bufptr - bufx;
            default:
                if (FD_ISSET(sock, &refd)) {
                    cnt = recv(sock, &c, 1, 0);
                    if (cnt < 0) {
                        if (errno == EINTR) {
                            len++; /* the while will decrement */
                            continue;
                        }
                        fprintf(stderr, "Error read() errno:%d - %s \n", errno, strerror(errno));
                        return -1;
                    }
                    if (cnt == 0) {
                        if (bufptr - bufx)
                            return bufptr - bufx;
                        return -1;
                    }
                }
                if (c == '\n') {
                    *bufptr = '\0';
                    return bufptr - bufx;
                }
                *bufptr++ = c;
        }
    }
    return bufptr - bufx;
}

/**
 * int readall( int sock, char *buf, int len,int timeseg )
 * 
 * Lee hasta la longitud len, solo termina cuando lee todo o si se cumple el 
 * tiempo de timedout  
 *  
 * @param sock: socket TCP
 * @param buf: buffer donde guardar los datos leidos
 * @param len: longitud a leer
 * @param timeseg: timedout en segundos para empezar a leer     
 * @return: retorna len si pudo leer completamente 
 *          0 si sale por timedout 
 *          -1 si hay algun error   
 */
int readall(int sock, char *buf, int len, int timeseg) {
    fd_set refd;
    int n, tmpres, ibuf;
    struct timeval tv, tv2;
    memset(buf, 0, len);
    ibuf = 0;

    tv2.tv_sec = timeseg / 1000;
    tv2.tv_usec = 1000 * (timeseg % 1000);

    while (len > 0) {
        FD_ZERO(&refd);
        FD_SET(sock, &refd);
        tv.tv_sec = tv2.tv_sec;
        tv.tv_usec = tv2.tv_usec;

        n = select(1 + sock, &refd, NULL, NULL, &tv);
        switch (n) {
            case -1: // error    
                fprintf(stderr, "Error select() errno:%d - %s \n", errno, strerror(errno));
                return -1;
            case 0: // timeout, entoces ya no hay mas nada para leer
                errno = ETIMEDOUT;
                return ibuf;
            default:
                if (FD_ISSET(sock, &refd)) {
                    //                    tmpres=recv(sock,buf+ibuf,len,0); 
                    tmpres = read(sock, buf + ibuf, len);
                    if (tmpres < 0) {

                        if (errno == EINTR)
                            continue;
                        fprintf(stderr, "Error read() errno:%d - %s \n", errno, strerror(errno));
                        return -1;
                    }
                    if (tmpres == 0) {
                        // end of file ?
                        if (ibuf)
                            return ibuf;
                        return -1;
                    }
                    ibuf += tmpres;
                    len -= tmpres;
                }
                break;
        }
    }
    return ibuf;
}

int create_tcp_socket_host(char *host, int port) {
    int sock;
    char ip[15];
    struct sockaddr_in serv_addr;

    alarm(3);
    if ((sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0) {
        perror("Can't create TCP socket");
        exit(1);
    }
    alarm(0);

    get_ip(ip,host);
    // fprintf(stderr, "IP is %s\n", ip); 

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = inet_addr(ip);
    ;
    serv_addr.sin_port = htons(port);

    //     fcntl(sock,F_GETFL);
    //     fcntl(sock, F_SETFL, O_NONBLOCK);    

    if (connect(sock, (struct sockaddr *) &serv_addr, sizeof (struct sockaddr)) < 0) {
        perror("Could not connect");
        exit(1);
    }
    return sock;
}

char *get_ip(char *ip, const char *host) {
    struct hostent *hent;
    memset(ip, 0, 10 );

    // setear una ip manual para ver la consulta realizada
    // strcpy(ip,"172.17.19.219");
    // return ip;  

    if ((hent = gethostbyname(host)) == NULL) {
        herror("Can't get IP");
        return ip;
    }
    strcpy(ip, inet_ntoa(*((struct in_addr *) hent->h_addr)));
    
    return ip;
}

/**
 * int cust_readline( int sock, char *buf, int maxlen, int timeseg ,char* cendline)
 * 
 * lee una linea delimitada por \n o hasta igual a maxlen;  
 * 
 * @param sock: sock TCP
 * @param buf: bufer donde guardar los datos leidos
 * @param maxlen: longitud maxima a leer
 * @param timesg: tiempo maximo en segundos para empezar a leer
 * @param cendline: fin de linea personalizado   
 * @return: longitud del string leido hasta encontrar caracter de fin o timedout
 *          -1 si ocurre un error
 */
int cust_readline(int sock, char *bufptr, int len, int timeseg, char* eof) {
    char *bufx = bufptr;
    char c;
    fd_set refd;
    int n, cnt;
    int le;
    struct timeval tv;

    le = strlen(eof);

    while (--len > 0) {
        tv.tv_sec = timeseg;
        tv.tv_usec = 0;

        FD_ZERO(&refd);
        FD_SET(sock, &refd);
        n = select(1 + sock, &refd, NULL, NULL, &tv);
        switch (n) {
            case -1: // error   
                fprintf(stderr, "Error select() errno:%d - %s \n", errno, strerror(errno));
                return -1;
            case 0: // timeout, entoces ya no hay mas nada para leer
                return bufptr - bufx;
            default:
                if (FD_ISSET(sock, &refd)) {
                    // WARNING: ver que si se usa un socket o un file descriptor no se comporta
                    // igual usando read que rcev
                    //cnt = read(sock,&c,1);
                    cnt = recv(sock, &c, 1, 0);
                    if (cnt < 0) {
                        if (errno == EINTR) {
                            len++; /* the while will decrement */
                            continue;
                        }
                        fprintf(stderr, "Error read() errno:%d - %s \n", errno, strerror(errno));
                        return -1;
                    }
                    if (cnt == 0) {
                        if (bufptr - bufx)
                            return bufptr - bufx;
                        return -1;
                    }
                }
                *bufptr++ = c;
                if (!strcmp(bufptr - le, eof)) {
                    *(bufptr-le) = '\0';
                    return bufptr - bufx;
                }
        }
    }
    return bufptr - bufx;
}

int bind_tcp(char *ip, int port, int nlist) {
    int sock;
    struct sockaddr_in serv_addr;
    int optval = 1;

    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        perror("Can't create TCP socket");
        return (-1);
    }
    memset(&serv_addr, 0, sizeof (struct sockaddr_in));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = inet_addr(ip);
    serv_addr.sin_port = htons(port);

    setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof (optval));
    // setsockopt(sock,SOL_SOCKET, SO_REUSEPORT,&optval, sizeof(optval));

    if (bind(sock, (struct sockaddr *) &serv_addr, sizeof (struct sockaddr)) == -1) {
        perror("Can't binding TCP socket");
        close(sock);
        return (-1);
    }
    if (listen(sock, nlist) == -1) {
        perror("Can't listen TCP socket");
        close(sock);
        return (-1);
    }
    return sock;
}

int connect_tcp(char *ip, int port) {
    int res;
    long arg;
    fd_set myset;
    struct timeval tv;
    int valopt;
    unsigned int lon;

    int sock;
    struct sockaddr_in serv_addr;

    if ((sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0) {
        perror("Can't create TCP socket");
        return (-1);
    }
    // fprintf(stderr, "tcp ip :%s::%i\n", ip, port);
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = inet_addr(ip);
    serv_addr.sin_port = htons(port);

    // Set non-blocking 
    if ((arg = fcntl(sock, F_GETFL, NULL)) < 0) {
        fprintf(stderr, "Error fcntl(..., F_GETFL) (%s)\n", strerror(errno));
        return -1;
    }
    arg |= O_NONBLOCK;
    if (fcntl(sock, F_SETFL, arg) < 0) {
        fprintf(stderr, "Error fcntl(..., F_SETFL) (%s)\n", strerror(errno));
        return -1;
    }
    // Trying to connect with timeout 
    res = connect(sock, (struct sockaddr *) &serv_addr, sizeof (serv_addr));
    if (res < 0) {
        if (errno == EINPROGRESS) {
            // fprintf(stderr, "EINPROGRESS in connect() - selecting\n");
            do {
                tv.tv_sec = 0;
                tv.tv_usec = 100000;
                FD_ZERO(&myset);
                FD_SET(sock, &myset);
                res = select(sock + 1, NULL, &myset, NULL, &tv);
                if (res < 0 && errno != EINTR) {
                    fprintf(stderr, "Error connecting %d - %s\n", errno, strerror(errno));
                    return -1;
                } else if (res > 0) {
                    // Socket selected for write 
                    lon = sizeof (int);
                    if (getsockopt(sock, SOL_SOCKET, SO_ERROR, (void*) (&valopt), &lon) < 0) {
                        fprintf(stderr, "Error in getsockopt() %d - %s\n", errno, strerror(errno));
                        return -1;
                    }
                    // Check the value returned... 
                    if (valopt) {
                        fprintf(stderr, "Error in delayed connection() %d - %s\n", valopt, strerror(valopt));
                        return -1;
                    }
                    break;
                } else {
                    fprintf(stderr, "Timeout in select() - Cancelling!\n");
                    return -1;
                }
            } while (1);
        } else {
            fprintf(stderr, "Error connecting %d - %s\n", errno, strerror(errno));
            return -1;
        }
    }
    // Set to blocking mode again... 
    if ((arg = fcntl(sock, F_GETFL, NULL)) < 0) {
        fprintf(stderr, "Error fcntl(..., F_GETFL) (%s)\n", strerror(errno));
        return -1;
    }
    arg &= (~O_NONBLOCK);
    if (fcntl(sock, F_SETFL, arg) < 0) {
        fprintf(stderr, "Error fcntl(..., F_SETFL) (%s)\n", strerror(errno));
        return -1;
    }
    return sock;
}

int nn_device_twoway(struct nn_device_recipe *device,
        int s1, nn_fd s1rcv, nn_fd s1snd,
        int s2, nn_fd s2rcv, nn_fd s2snd) {
    int rc;
    struct pollfd pfd[4];

    /*  Initialise the pollset. */
    pfd[0].fd = s1rcv;
    pfd[0].events = POLLIN;
    pfd[1].fd = s1snd;
    pfd[1].events = POLLIN;
    pfd[2].fd = s2rcv;
    pfd[2].events = POLLIN;
    pfd[3].fd = s2snd;
    pfd[3].events = POLLIN;

    while (1) {

        /*  Wait for network events. */
        rc = poll(pfd, 4, -1);
        assert(rc >= 0);
//        if (nn_slow(rc < 0 && errno == EINTR))
//            return -1;
        assert(rc != 0);

        /*  Process the events. When the event is received, we cease polling
            for it. */
        if (pfd[0].revents & POLLIN)
            pfd[0].events = 0;
        if (pfd[1].revents & POLLIN)
            pfd[1].events = 0;
        if (pfd[2].revents & POLLIN)
            pfd[2].events = 0;
        if (pfd[3].revents & POLLIN)
            pfd[3].events = 0;

        /*  If possible, pass the message from s1 to s2. */
        if (pfd[0].events == 0 && pfd[3].events == 0) {
            rc = nn_device_mvmsg(device, s1, s2, NN_DONTWAIT);
            if (nn_slow(rc < 0))
                return -1;
            pfd[0].events = POLLIN;
            pfd[3].events = POLLIN;
        }

        /*  If possible, pass the message from s2 to s1. */
        if (pfd[2].events == 0 && pfd [1].events == 0) {
            rc = nn_device_mvmsg(device, s2, s1, NN_DONTWAIT);
            if (nn_slow(rc < 0))
                return -1;
            pfd[2].events = POLLIN;
            pfd[1].events = POLLIN;
        }
    }
}

