#include <stdio.h>
#include <stdlib.h>

#include "logger.h"
#include "socketutils.h"


int main( void )
{
    log_debug_level(LOG_ALERT|LOG_ERROR|LOG_MSG|LOG_INFO|LOG_WARNING);
    log_logger_level(0); 
    
    
    log_info("[SRV] Prueba socketutils %s","[yea string]"); 
    
    char ip[15];
    log_info("[SRV] get_ip(sima-nb): %s ",get_ip(ip,"sima-nb"));
    
    
    
    int port=6556, sock, csock;
    struct sockaddr_in client;
    socklen_t clen = sizeof(struct sockaddr_in);
    char buf[1024];
    
    sock = bind_tcp(ip, port, 10);
    log_info("[SRV] bind port: %d sock:%d",port,sock);
    
    
    while(1)
    {
        log_info("[SRV] esperando conexxion entrante...");
        csock = accept(sock, (struct sockaddr *)&client, &clen);
        log_info("[SRV] Conexion desde: %s:%d",inet_ntoa(client.sin_addr),ntohs(client.sin_port));
        {
            log_msg("[SRV] accpt CLI:%i !",csock);
            while(readline(csock,buf,1024,5000) != -1 ){
                log_msg("[SRV] read <<%s>> !",buf);
            }
            log_msg("[SRV] CLI:%i END !",csock);
        } 
    } 
    
    return EXIT_SUCCESS;
}
  