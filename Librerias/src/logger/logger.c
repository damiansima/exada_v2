
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>   
#include <sys/time.h>
#include <fcntl.h>
#include <string.h>
#include <time.h>
#include <strings.h>
#include <ncurses.h>
#include <pthread.h>

#include "logger.h"

// variables privadas globales
static char _FILE_LOG[200] = "/tmp/logger.log";
static pthread_mutex_t fprintf_mutex = PTHREAD_MUTEX_INITIALIZER;

static int _file_date = 0;
static char _fmt_file_date[128];
static int _file_size = 50000000;

int _LOG_DEBUG_PRINT = LOG_ALERT | LOG_ERROR;
int _LOG_DEBUG_LEVEL = 1;
int _LOGGER_SAVE = 255;

char _space[24] = "";
int _level = 0;

void log_set_size(int file_size_kb)
{
    if (file_size_kb < 10 || file_size_kb > 100000)
        return;
    _file_size = file_size_kb * 1024;
}

void logger_file(char *file_path)
{
    char *dot;
    char command[256];
    strcpy(_FILE_LOG, file_path);
    if (!_file_date)
    {
        dot = strrchr(_FILE_LOG, '.');
        if (!(dot && !strcasecmp(dot, ".log")))
            strcat(_FILE_LOG, ".log");
    }
    sprintf(command, "mkdir $(dirname %s) 2> /dev/null ", _FILE_LOG);
    system(command);
    pthread_mutex_unlock(&fprintf_mutex);
}

void logger_file_date(char *file_path, char *fmt_date)
{
    // fmt es algo como "%Y%m%d"  o "%d_%b_%y" 
    // tambien se puede usar la hora, etc..
    _file_date = strlen(fmt_date) > 2 ? 1 : 0;
    strcpy(_fmt_file_date, fmt_date);
    logger_file(file_path);
}

void log_set_fmt(const char *fmt_date)
{
    // fmt es algo como "%Y%m%d"  o "%d_%b_%y"  
    // tambien se puede usar la hora, etc..
    _file_date = strlen(fmt_date) > 2 ? 1 : 0;
    strcpy(_fmt_file_date, fmt_date);
}

void log_set_file(char *log)
{
    logger_file(log);
    //     fprintf(stderr,_LOG_INFO"- INFO: "_LOG_RESET);
    //     fprintf(stderr,"FILE_LOG: %s\n",_FILE_LOG);     
}

void log_logger_level(int lev)
{
    _LOGGER_SAVE = lev;
}

void log_debug_level(int lev)
{
    _LOG_DEBUG_PRINT = lev;
}

/*
log_logger() generico     [OK]
log_error() error         [OK]
log_alert() alerta        [OK]
log_info()  informacion   [OK]
log_debug() depuraci�n    [NO]
log_user()  usuario       [NO]
 */

void log_alert(char *fmt, ...)
{
    va_list arglist;

    if (_LOGGER_SAVE & LOG_ALERT)
    {
        va_start(arglist, fmt);
        _logger("ALERT:", fmt, arglist);
        va_end(arglist);
    }
    if (_LOG_DEBUG_PRINT & LOG_ALERT)
    {
        va_start(arglist, fmt);
        _debug(LOG_ALERT, fmt, arglist);
        va_end(arglist);
    }
    //    log_trace_level(0);
}

void log_info(char *fmt, ...)
{
    va_list arglist;

    if (_LOGGER_SAVE & LOG_INFO)
    {
        va_start(arglist, fmt);
        _logger(" INFO:", fmt, arglist);
        va_end(arglist);
    }
    if (_LOG_DEBUG_PRINT & LOG_INFO)
    {
        va_start(arglist, fmt);
        _debug(LOG_INFO, fmt, arglist);
        va_end(arglist);
    }
    //    log_trace_level(0);
}

void log_error(char *fmt, ...)
{
    va_list arglist;

    if (_LOGGER_SAVE & LOG_ERROR)
    {
        va_start(arglist, fmt);
        _logger("ERROR:", fmt, arglist);
        va_end(arglist);
    }
    if (_LOG_DEBUG_PRINT & LOG_ERROR)
    {
        va_start(arglist, fmt);
        _debug(LOG_ERROR, fmt, arglist);
        va_end(arglist);
    }
    //    log_trace_level(0);
}

void log_msg(char *fmt, ...)
{
    va_list arglist;

    if (_LOGGER_SAVE & LOG_MSG)
    {
        va_start(arglist, fmt);
        _logger("  MSG:", fmt, arglist);
        va_end(arglist);
    }
    if (_LOG_DEBUG_PRINT & LOG_MSG)
    {
        va_start(arglist, fmt);
        _debug(LOG_MSG, fmt, arglist);
        va_end(arglist);
    }
    //    log_trace_level(0);
}

void log_warning(char *fmt, ...)
{
    va_list arglist;

    if (_LOGGER_SAVE & LOG_WARNING)
    {
        va_start(arglist, fmt);
        _logger(" WARN:", fmt, arglist);
        va_end(arglist);
    }
    if (_LOG_DEBUG_PRINT & LOG_WARNING)
    {
        va_start(arglist, fmt);
        _debug(LOG_WARNING, fmt, arglist);
        va_end(arglist);
    }
    //    log_trace_level(0);
}

void logger(char *fmt, ...)
{
    va_list arglist;

    va_start(arglist, fmt);
    _logger("", fmt, arglist);
    va_end(arglist);

    if (_LOG_DEBUG_PRINT & (LOG_MSG | LOG_INFO))
    {
        va_start(arglist, fmt);
        _debug(0, fmt, arglist);
        va_end(arglist);
    }
    //    log_trace_level(0);
}

void log_set_debug_verbose(int level)
{
    _LOG_DEBUG_LEVEL = level;
    log_debug(0, "logger set verbose: %d", level);
}

void log_debug(int level, char *fmt, ...)
{
    va_list arglist;


    if (level <= _LOG_DEBUG_LEVEL)
    {
        va_start(arglist, fmt);
        _debug(LOG_DEBUG, fmt, arglist);
        va_end(arglist);

        if (_LOGGER_SAVE & LOG_WARNING)
        {
            va_start(arglist, fmt);
            _logger("DEBUG:", fmt, arglist);
            va_end(arglist);
        }
    }
    //    log_trace_level(0);
}

void log_cancel_trace(void)
{
    strcpy(_space, "");
    _level = 0;
}

char *log_trace_level(int l)
{

    char *s = _space;
    char *mod = "├"; // ┌ ┘ ─ ┴ ├

    if (l > 0 && _level < 20)
    {
        mod = "└──┬";
        l = _level;
        _level += 3;
    }
    else if (l < 0 && _level >= 3)
    {
        mod = "┌──┴";
        _level -= 3;
        l = _level;
    }
    else
    {
        l = _level;
    }
    while (l--) *s++ = ' ';
    while (*mod) *s++ = *mod++;
    *s = 0;

    return _space;
}

void _debug(int mode, const char *fmt, va_list arglist)
{
    pthread_mutex_lock(&fprintf_mutex);
    switch (mode)
    {
    case LOG_ALERT: fprintf(stderr, _LOG_ALERT "- ALERT: "_LOG_RESET);
        break;
    case LOG_ERROR: fprintf(stderr, _LOG_ERROR "- ERROR: "_LOG_RESET);
        break;
    case LOG_INFO: fprintf(stderr, _LOG_INFO "-  INFO: "_LOG_RESET);
        break;
    case LOG_WARNING: fprintf(stderr, _LOG_WARNING"-  WARN: "_LOG_RESET);
        break;
    case LOG_MSG: fprintf(stderr, _LOG_MSG "-   MSG: "_LOG_RESET);
        break;
    case LOG_DEBUG: fprintf(stderr, _LOG_DEBUG "- DEBUG:"_LOG_RESET" ");
        break;
    }
    fprintf(stderr, _space);
    vfprintf(stderr, fmt, arglist);
    fprintf(stderr, "\n");
    fflush(stderr);
    pthread_mutex_unlock(&fprintf_mutex); 
}


void _logger(char *mode, const char *format, va_list arglist)
{

    FILE *Fd = NULL;
    time_t Time;
    int sizefile = 0;
    char bufti[86];
    char file_log[128];

    Time = time(NULL);

    // si creea un solo archivo de log => chequea el tamanio  
    if (!_file_date)
    {
        Fd = fopen(_FILE_LOG, "r");
        if (Fd != NULL)
        {
            sizefile = ftell(Fd);
            fclose(Fd);
            if (sizefile > _file_size)
            {
                sprintf(bufti, "%s.old", _FILE_LOG);
                rename(_FILE_LOG, bufti);
                //                sprintf(bufti,"mv %s %s.old",_FILE_LOG,_FILE_LOG);
                //                system(bufti);
                fprintf(stderr, "archivo %s muy grande, se renombra", _FILE_LOG);
                fprintf(stderr, "%s", bufti);
            }
        }
        strcpy(file_log, _FILE_LOG);
    }
        // si usa un formato para cada dia u hora no testea el tamanio
    else
    {
        strftime(bufti, 80, _fmt_file_date, localtime(&Time));
        sprintf(file_log, "%s.%s.log", _FILE_LOG, bufti);
        // fprintf(stderr, "_FILE_LOG:%s\n", file_log ); 
    }

    Fd = fopen(file_log, "a+");
    if (Fd == NULL)
    {
        perror(file_log);
        return;
    }
    // TODO: hacer que esto funcione con threads y que sea multi hilo
    flockfile(Fd);
    // funciona con pthread_mutex_lock pero no desde la libreria.
    // hay que generar un estrucura para logear
    /*
     * 
     * struct st_logger{
     *  char *file_log;
     *  char *format;
     *  pthread_mutex_t *file_mutex; 
     *  pthread_mutex_t *out_mutex; 
     *  int debug_level;
     *  int logger_level;
     *  int size_limit;
     *  int rotation;
     * }
     * 
     */

    strftime(bufti, 80, "%d/%m/%Y %H:%M:%S", localtime(&Time));
    fprintf(Fd, "%s ", bufti);
    fprintf(Fd, "%s %s", mode, _space);
    vfprintf(Fd, format, arglist);
    fprintf(Fd, "\n");
    fflush(Fd);
    funlockfile(Fd);

    fclose(Fd);
}




