#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include "lista_continua.h"


#define LEN_SHM 1024

#define TESTS(a) printf("%-30s: %s\n",#a,(char *)a)
#define TEST(a) printf("%-30s: %i\n",#a,(int)a)
#define TESTN(a) {printf("--- %-30s\n",#a);a;}



int main () 
{
    LIST_BLOCK *block;
    char *shm = (char *)malloc(LEN_SHM);
    LIST_t *list = (LIST_t *)shm;
    
    list_init(list,LEN_SHM);
        
    TESTN(block = list_add(list, 100, 8));
    TESTN(block = list_add(list, 100, 2));
    TESTN(block = list_add(list, 100, 3));
    TESTN(block = list_add(list, 100, 4));
    TESTN(block = list_add(list, 100, 5));
    TESTN(block = list_add(list, 100, 6));
    TESTN(block = list_add(list, 100, 7));
//    TESTN(block=list_add(list, 98, 9));
    TEST(list->ctr.count_list);
    
    TESTN(block = list_first_block(list));
    while(block)
    {
        strcpy(block->data,"nada");
        block = list_next_block(list,block);
    }
    block=list_get_block(list,7);
    memcpy(block->data,"--- hola LOCO     ",13);
    
    TESTN(block = list_first_block(list));
    while(block)
    {
        TEST(block);
        TEST(block->key);
        TEST(block->data);
        TEST(block->nbytes);
        TEST(block->next);
        TEST(block->prev);
        TESTS(block->data);
        TESTN(block = list_next_block(list,block));
    }
    free(shm);
            
    return 0;
}