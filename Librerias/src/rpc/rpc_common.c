#include "../include/rpc_common.h"


char *rpc_strerror(e_rpc_t error)
{
    switch(error)
    {
        case RPC_FAIL: return "Retornada por lafuncion";
        case RPC_FAIL_CONNECTION: return "Conneccion con el servidor";
        case RPC_FAIL_NO_FUNCTION: return "Funcion no registrada";
        case RPC_NO_CONNECTED: return "No esta conectado"; 
        case RPC_OK: return "Exito";
    }
    return "Error desconocido";
}

int list_add_raw( LIST_t *list, int id, void *value, int size) 
{
    LIST_BLOCK *block;
    block = list_add(list,size,id);
    if( block == NULL )
        return -1;
    memcpy(block->data,value,size);    
    return 0; 
}


int list_add_float(LIST_t *list, int id, float value) 
{
    return list_add_raw( list, id, &value, sizeof(value)+1);
}
int list_add_int(LIST_t *list, int id, long int value)
{
    return list_add_raw( list, id, &value, sizeof(value)+1);
} 




int create_tcp_socket(char *ip, int port) {
    int res;
    long arg;
    fd_set myset;
    struct timeval tv;
    int valopt;
    unsigned int lon;

    int sock;
    struct sockaddr_in serv_addr;
    
    if ((sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0) {
        perror("Can't create TCP socket");
        return (-1);
    }
    fprintf( stderr, "tcp ip :%s::%i\n",ip,port);   
    serv_addr.sin_family = AF_INET; 
    serv_addr.sin_addr.s_addr = inet_addr(ip);
    serv_addr.sin_port = htons(port);

    // Set non-blocking 
    if ((arg = fcntl(sock, F_GETFL, NULL)) < 0) {
        fprintf(stderr, "Error fcntl(..., F_GETFL) (%s)\n", strerror(errno));
        return -1;
    }
    arg |= O_NONBLOCK;
    if (fcntl(sock, F_SETFL, arg) < 0) {
        fprintf(stderr, "Error fcntl(..., F_SETFL) (%s)\n", strerror(errno));
        return -1;
    }
    // Trying to connect with timeout 
    res = connect(sock, (struct sockaddr *) &serv_addr, sizeof (serv_addr));
    if (res < 0) {
        if (errno == EINPROGRESS) {
            fprintf(stderr, "EINPROGRESS in connect() - selecting\n");
            do {
                tv.tv_sec = 0;
                tv.tv_usec = 100000;
                FD_ZERO(&myset);
                FD_SET(sock, &myset);
                res = select(sock + 1, NULL, &myset, NULL, &tv);
                if (res < 0 && errno != EINTR) {
                    fprintf(stderr, "Error connecting %d - %s\n", errno, strerror(errno));
                    return -1;
                }
                else if (res > 0) {
                    // Socket selected for write 
                    lon = sizeof (int);
                    if (getsockopt(sock, SOL_SOCKET, SO_ERROR, (void*) (&valopt), &lon) < 0) {
                        fprintf(stderr, "Error in getsockopt() %d - %s\n", errno, strerror(errno));
                        return -1;
                    }
                    // Check the value returned... 
                    if (valopt) {
                        fprintf(stderr, "Error in delayed connection() %d - %s\n", valopt, strerror(valopt));
                        return -1;
                    }
                    break;
                }
                else {
                    fprintf(stderr, "Timeout in select() - Cancelling!\n");
                    return -1;
                }
            } while (1);
        }
        else {
            fprintf(stderr, "Error connecting %d - %s\n", errno, strerror(errno));
            return -1;
        }
    }
    // Set to blocking mode again... 
    if ((arg = fcntl(sock, F_GETFL, NULL)) < 0) {
        fprintf(stderr, "Error fcntl(..., F_GETFL) (%s)\n", strerror(errno));
        return -1;
    }
    arg &= (~O_NONBLOCK);
    if (fcntl(sock, F_SETFL, arg) < 0) {
        fprintf(stderr, "Error fcntl(..., F_SETFL) (%s)\n", strerror(errno));
        return -1;
    }
    return sock;
}



int sendall(int sock, char *buf, int len) {
    int sent, tmpres;
    sent = 0;
    while (len > 0) {
        tmpres = write(sock, buf+sent, len );
        if (tmpres == -1) {
            if( errno == EINTR )
                continue;    
            perror("Can't send query");
            return -1;
        }
        sent += tmpres;
        len -= tmpres;
    }
    return sent;
}


int readall(int sock, char *buf, int len, int timeseg) {
    fd_set refd;
    int n, tmpres, ibuf;
    struct timeval tv, tv2;
    memset(buf, 0, len);
    ibuf = 0;

    if(timeseg)
    {
        tv2.tv_sec = timeseg / 1000;
        tv2.tv_usec = 1000 * (timeseg % 1000);
    }

    while (len > 0) {
        if(timeseg)
        {
            FD_ZERO(&refd);
            FD_SET(sock, &refd);
            tv.tv_sec = tv2.tv_sec;
            tv.tv_usec = tv2.tv_usec;
            n = select(1 + sock, &refd, NULL, NULL, &tv);
            switch (n) {
                case -1: // error    
                    return -1;
                case 0: // timeout, entoces ya no hay mas nada para leer
                    errno = ETIMEDOUT;
                    return ibuf;
                default:
                    if (FD_ISSET(sock, &refd)) {
                        //                    tmpres=recv(sock,buf+ibuf,len,0); 
                        tmpres = read(sock, buf + ibuf, len);
                        if (tmpres < 0) {
                            if (errno == EINTR)
                                continue;
                            return -1;
                        }
                        if (tmpres == 0) {
                            // end of file ?
                            if(ibuf)
                              return ibuf;  
                            return -1;
                        }
                        ibuf += tmpres;
                        len -= tmpres;
                    }
                    break;
            }
        } else {
            tmpres = read(sock, buf + ibuf, len);
            if (tmpres < 0) {
                if (errno == EINTR)
                    continue;
                return -1;
            }
            if (tmpres == 0) {
                // end of file ?
                return ibuf;
            }
            ibuf += tmpres;
            len -= tmpres;
        }
    }
    return ibuf;
}
 

int bind_tcp(char *ip, int port,int nlist)
{
    int sock;
    struct sockaddr_in serv_addr;
    int optval=1;
    
    if((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        perror("Can't create TCP socket");
        return (-1);
    }            
    memset(&serv_addr,0,sizeof(struct sockaddr_in));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = inet_addr(ip);   
    serv_addr.sin_port = htons(port);
    
    setsockopt(sock,SOL_SOCKET, SO_REUSEADDR,&optval, sizeof(optval));  
    // setsockopt(sock,SOL_SOCKET, SO_REUSEPORT,&optval, sizeof(optval));
    
    if (bind(sock,&serv_addr, sizeof(struct sockaddr)) == -1) 
    {
        perror( "Can't binding TCP socket");
        close(sock);
        return (-1);
    }
    if (listen(sock, nlist) == -1) 
    {
        perror("Can't listen TCP socket");
        close(sock);                     
        return (-1);
    } 
    return sock; 
}