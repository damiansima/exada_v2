#include "../include/rpc_test.h"


void funcion_test(RPC_t *rpc) {
    LIST_t *lr_rply = NULL;
    LIST_t *lr_send = list_alloc(512);
    e_rpc_t ret;
    list_add_str(lr_send, 1, "hola mundo desde cliente");

    ret = rpc_send_msg(rpc, "funcion_test", lr_send, &lr_rply);
    if (ret != RPC_OK) {
        printf("%s\n",rpc_strerror(ret));
        free(lr_rply);
        return;
    }

    if (list_has_id(lr_rply, 4)) {
        printf("data %s\n", list_get_str(lr_rply, 4));
    }

    printf("data %li\n", list_get_int(lr_rply, 1));
    printf("data %f\n", list_get_float(lr_rply, 2));

    free(lr_rply);
}

int w_fread(RPC_t *rpc, char *data, int size, FILE *fp) {
    LIST_t *lr_rply = NULL;
    LIST_t *lr_send = list_alloc(1024);
    int nr;

    list_add_int(lr_send, 1, size);
    list_add_ptr(lr_send, 2, fp);

    if( rpc_send_msg(rpc, "w_fread", lr_send, &lr_rply) != RPC_OK )
    {   
        free(lr_rply); 
        return 0;
    }
    
    nr = list_get_int(lr_rply, 1);
    memcpy(data, list_get_str(lr_rply, 2), nr);
    free(lr_rply);
    return nr;
}

FILE *w_fopen(RPC_t *rpc, char *filename, char *mode) {
    LIST_t *lr_rply = NULL;
    LIST_t *lr_send = list_alloc(1024);
    FILE *fp = NULL;
    e_rpc_t ret;
    list_add_str(lr_send, 1, filename);
    list_add_str(lr_send, 2, mode);

    ret = rpc_send_msg(rpc, "w_fopen", lr_send, &lr_rply);
    if (ret != RPC_OK) {
        printf("%s\n",rpc_strerror(ret));
        free(lr_rply); 
        return NULL;
    }

    if (list_has_id(lr_rply, 1)) {
        fp = list_get_ptr(lr_rply, 1); 
        printf("fp %i\n", (int)fp);
    }
    free(lr_rply);  
    return fp;
}