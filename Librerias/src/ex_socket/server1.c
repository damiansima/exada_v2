#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>

#include "logger.h"
#include "ex_socket.h"

#include <netinet/tcp.h>
#include <netinet/in.h>
#include <netinet/udp.h>
#include <sys/un.h>


void pepe(int a){
      printf( " pepe sig:%d %s\n ", a, strerror(errno));
}

#define test_length(type) log_msg("%30s: %d","sizeof("#type")",sizeof(type))

int main( void )
{
    log_debug_level(LOG_ALERT|LOG_ERROR|LOG_MSG|LOG_INFO|LOG_WARNING);
    log_logger_level(0); 
    
    
    log_info("[SRV] Prueba socketutils %s","[yea string]"); 
    
    test_length(long long int);
    test_length(struct sockaddr);
    test_length(struct sockaddr_in);
    test_length(struct sockaddr_un);
    test_length(struct sockaddr_storage);
    
    esock_t *p;
//    char *url;
//    char *url2;
    int r;
    
//    url="tcp://sima-ub:3431";
//    url2="tcp://sima-ub:3433";
    p = ex_sock_alloc();
    r=ex_listen( p, "tcp://sima-ub:3432", 10 );
    log_info("sock:%d",r);
    r=ex_listen( p, "tcp://sima-ub:3433", 10 );
    log_info("sock:%d",r);
    r=ex_listen( p, "tcp://sima-ub:3434", 10 );
    log_info("sock:%d",r);
    emsg_t *buf=NULL;
    while(1){
        log_info("ex_rcev");
        buf= ex_rcev(p,0);        
        log_msg("Longitud del paquete:%d <%s> ",buf->length, buf->data);
        log_msg("nservers:%d",p->nservers);
        
        if(buf){
            char *msg;
            int len=asprintf( &msg,"reply desde %s:%d", p->client->parent->path, p->client->parent->port);
            len = ex_reply(p, msg, len);
            log_msg("reply ret:%d",len);
            free(buf);
        }
        else
            log_error("[SRV] TEST %s",ex_strerror());
            
    }
    
//    sleep(2);
    
    
//    if(ex_sock_validator(p,url,"SERVER")==ERROR){
//        log_error("[SRV] TEST %s",ex_strerror());
//        return EXIT_FAILURE;
//    }
        
//    log_msg("[SRV] TEST %30s : %p ", url , p ); 
//    log_msg("[SRV] TEST %30s : port %d", url , p->servers.port ); 
//    log_msg("[SRV] TEST %30s : path %s", url , p->servers.path ); 
    
//    log_msg("[SRV] ex_listen %d ",  );
    
    

    

    
    
    log_msg("[SRV] ex_rcev() end ");
    
    log_msg("[SRV] done");
    
    return EXIT_SUCCESS;
    
    
    
    int s = bind_tcp("0.0.0.0",9898,10);
    log_msg("[SRV] TEST %30s : %p ", "bind" , s ); 
    
    struct sockaddr cli;
    socklen_t size = sizeof(cli);
    
//    sleep(5);
    
    signal(SIGPIPE,pepe);
    
    int con = accept(s, (struct sockaddr *)&cli, &size);         
    
    log_msg("[SRV] TEST %30s : %p ", "acept" , con );     
    
    while(1){
        puts("sendall()");
        sendall(con,"hola",5);
        perror("que?");
        sleep(1);
    }
    
    puts("[SRV] done");
     
    return EXIT_SUCCESS;
}
  