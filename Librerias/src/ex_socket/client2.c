#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>

#include "logger.h"
#include "ex_socket.h"

int main(void)
{
    esock_t *client;
    int ret;
    emsg_t *buf = NULL;

    log_debug_level(LOG_ALERT | LOG_ERROR | LOG_MSG | LOG_INFO | LOG_WARNING);
    log_logger_level(0);

    log_info("[CLI] Prueba socketutils ");

    client = ex_sock_alloc();
    ret = ex_connect(client, "tcp://sima-ub:9001");
    if (ret == ERROR)
    {
        log_error("[CLI] TEST %s", ex_strerror());
        //        return EXIT_FAILURE;
    }

    ex_send(client, 0, "Init()", 6);

    while (1)
    {
//        log_msg("...");
        buf = NULL;
//        buf = 1;
        while (buf == NULL)
        {
            int l = ex_check_connect(client);
            if (l == client->nclient)
            {
                buf = ex_rcev(client, 0);
            }
            else
            {
                puts(".");
                buf = ex_rcev(client, 1000);
            }
        };
        
//        puts(".");
//        ex_check_connect(client);
//        buf = ex_rcev(client, 1000);  
        if (buf)
        {
            ex_reply(client,"Ok, de cliente",14); 
            log_msg("recivo:%d <<%s>> ", buf->length, buf->data);
            free(buf);
        }
    }

    while (0)
    {
        log_msg("...");
        buf = ex_rcev(client, 10);
        log_info("BUF: %s", (buf ? "OK BUF" : "NULL"));

        if (buf)
        {
            log_msg("recivo:%d <<%s>> ", buf->length, buf->data);
            free(buf);
        }
        else
        {
            log_msg("ok.. reintentar");
            ex_check_connect(client);
            sleep(1);
        }
    }

    log_msg("done");

    return EXIT_SUCCESS;
}
