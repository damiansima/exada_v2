#include <stdio.h>
#include <sys/types.h>
#include <errno.h>

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>

#include <signal.h>

#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/time.h>                          
#include <sys/select.h>
#include <poll.h>
#include <assert.h>



#include <string.h>
#include <netinet/tcp.h>
#include "ex_socket.h"

static char ERRSTR[1024];

#define assert_errstr( valid , msg ) ((valid)? 0 : sprintf(ERRSTR,"%s: %s (%s) fail!",__FUNCTION__, msg, #valid ))
#define eprintf(format, ...) fprintf (stderr, format, __VA_ARGS__)

#define efree(mem) if(mem){free(mem);mem=NULL;}

char *ex_strerror(void)
{
    return ERRSTR;
}

char *ex_get_ip(const char *host)
{
    struct hostent *hent;
    char *ip = calloc(15, 1);

    if ((hent = gethostbyname(host)) == NULL)
    {
        free(ip);
        return NULL;
    }
    strcpy(ip, inet_ntoa(*((struct in_addr *) hent->h_addr)));
    return ip;
}

char *skipstr(char *src, char *skip)
{
    char *p;
    p = strstr(src, skip);
    return ( p ? (p + strlen(skip)) : NULL);
}

int ex_conn_validator(econn_t *conn, char *addr)
{
    char *path = NULL;
    char *port = NULL;

    if ((path = skipstr(addr, "udp://")) != NULL)
    {
        path = strdup(path);
        port = strstr(path, ":");
        conn->flags.udp = 1;
    }
    else if ((path = skipstr(addr, "tcp://")) != NULL)
    {
        path = strdup(path);
        port = strstr(path, ":");
        conn->flags.tcp = 1;
    }
    else
    {
        assert_errstr(0, "addr, protocolo desconocido");
        return ERROR;
    }

    if (port)
    {
        *port = '\0';
        port += 1;
    }

    conn->path = ex_get_ip(path);
    conn->port = atoi(port);
    efree(path);

    if (assert_errstr(conn->path != NULL, "addr, no es una direccion ip valida"))
    {
        return ERROR;
    }
    if (port && assert_errstr(conn->port > 0, "addr, puerto incorrecto"))
    {
        efree(conn->path);
        return ERROR;
    }

    return 0;
}

int ex_listen(esock_t *s, char *addr, int nlist)
{
    int sock;
    struct sockaddr_in serv_addr;

    if (assert_errstr(s->nservers < EX_MAX_SERVER, "Supera el maximo configurado"))
        return ERROR;

    econn_t *conn = calloc(sizeof (econn_t), 1);
    if (ex_conn_validator(conn, addr) == ERROR)
    {
        efree(conn);
        return ERROR;
    }

    conn->fd = 0;
    conn->flags.server = 1;

    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        assert_errstr(0, "No se puede crear el socket TCP");
        efree(conn->path);
        efree(conn);
        return ERROR;
    }
    memset(&serv_addr, 0, sizeof (struct sockaddr_in));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = inet_addr(conn->path);
    serv_addr.sin_port = htons(conn->port);

    int optval = 1;
    setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof (optval));
    setsockopt(sock, SOL_SOCKET, SO_REUSEPORT, &optval, sizeof (optval));

    if (bind(sock, (struct sockaddr *) &serv_addr, sizeof (struct sockaddr)) == -1)
    {
        assert_errstr(0, "No se puede enlazar el socket");
        efree(conn->path);
        efree(conn);
        close(sock);
        return ERROR;
    }
    if (listen(sock, nlist) == -1)
    {
        assert_errstr(0, "No se puede poner en escucha");
        efree(conn->path);
        efree(conn);
        close(sock);
        return ERROR;
    }
    conn->fd = sock;
    s->servers[s->nservers] = conn;
    s->nservers++;

    return sock;
}

<<<<<<< HEAD
int ex_connect_tcp(econn_t *conn)
{
    ex_disconnect(conn);

    conn->flags.client = 1;
    conn->fd = connect_tcp(conn->path, conn->port);
    if (conn->fd < 0)
    {
        assert_errstr(0, "No se puede conectar con el servidor");
        return ERROR;
    }

    struct timeval tv;
    tv.tv_sec = 0;
=======
int ex_connect_tcp(econn_t *conn){
    struct sockaddr_in serv_addr;
    
    memset(&serv_addr, 0, sizeof (struct sockaddr_in));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = inet_addr(conn->path);
    serv_addr.sin_port = htons(conn->port);
    
    conn->flags.connected=0;
    
    long arg,argset;
    if ((arg = fcntl(conn->fd, F_GETFL, NULL)) < 0) {
        assert_errstr(0,"Error fcntl(F_GETFL)");
        return ERROR;
    }
    // NO Bloqueante
    argset = arg | O_NONBLOCK;
    fcntl(conn->fd, F_SETFL, argset);
    
    
    if(connect(conn->fd, (struct sockaddr *) &serv_addr, sizeof(struct sockaddr_in)) < 0) {
        if(errno!=EISCONN){
            assert_errstr(0,"No se puede conectar con el servidor");
            return ERROR;
        }
    }
    
    // Bloqueante
    argset = arg & (~O_NONBLOCK);
    fcntl(conn->fd, F_SETFL, argset);
    
    struct timeval tv;  
    tv.tv_sec  = 0; 
>>>>>>> b4c7c0cd31e29312b9b89d55e22bfb5a33a79a2f
    tv.tv_usec = 1000;
    setsockopt(conn->fd, SOL_SOCKET, SO_RCVTIMEO, (char *) &tv, sizeof (struct timeval));
    setsockopt(conn->fd, SOL_SOCKET, SO_SNDTIMEO, (char *) &tv, sizeof (struct timeval));

    int flag = 1;
    setsockopt(conn->fd, IPPROTO_TCP, TCP_NODELAY, (char *) &flag, sizeof (int));

    conn->flags.connected = 1;
    return 0;
}

int ex_connect(esock_t *s, char *addr)
{
    int ret;

    if (assert_errstr(s->nclient < EX_MAX_CLIENT, "Supera el maximo configurado"))
        return ERROR;

    econn_t *conn = calloc(sizeof (econn_t), 1);
    if (ex_conn_validator(conn, addr) == ERROR)
    {
        efree(conn);
        return ERROR;
    }

    conn->fd = 0;
    conn->flags.client = 1;
    conn->flags.server = 0;

    //    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
    //        assert_errstr(0,"No se puede crear el socket TCP");
    //        efree(conn->path);
    //        efree(conn);
    //        return ERROR;
    //    }


    //    conn->fd=sock;
    conn->flags.connected = 0;
    s->clients[s->nclient] = conn;
    s->nclient++;

    ret = ex_connect_tcp(conn);

    return ret;
}

//    if (setsockopt(sockfd, SOL_SOCKET, SO_RCVTIMEO, (char *)&timeout, sizeof(timeout)) < 0 ) 
//    perror("setsockopt failed \n");
//
//    if (setsockopt(sockfd, SOL_SOCKET, SO_SNDTIMEO, (char *)&timeout, sizeof(timeout)) < 0 ) 
//    perror("setsockopt failed \n");

//    struct timeval tv;
//    /* Set another IP socket options for timeout so we do not block waiting */
//    tv.tv_sec  = 10;  /* Wait for max 10 sec on recvmsg */
//    tv.tv_usec = 0;
//    int rv = setsockopt(p->sock.fd, SOL_SOCKET, SO_RCVTIMEO, (char *) &tv,
//             sizeof(struct timeval));
//    if (rv < 0) 
//        log_error("Set SO_RCVTIMEO");
//    
//    
//    struct msghdr m;
//    
//    while(1){ 
//        recvmsg(p->sock.fd, &m ,0 );
//        sleep(1);
//    }
//    

void ex_disconnect(econn_t *conn)
{
    if (conn->fd > 0)
    {
        shutdown(conn->fd, SHUT_RDWR);
        close(conn->fd);
        conn->fd = -1;
    }
    conn->flags.connected = 0;
}

int ex_reply(esock_t *sock, char *msg, int length)
{
    int l = 0;
    l = send(sock->client->fd, (char *) &length, sizeof (length), MSG_MORE | MSG_NOSIGNAL);
    if (l > 0)
        l = sendall(sock->client->fd, msg, length);
    return l;
}

void ex_skipread(int s)
{
    int ret;
    char buf[1024];
    do
    {
        ret = recv(s, (char *) &buf, 1024, MSG_NOSIGNAL | MSG_DONTWAIT );
    }
    while (ret > 0);
}

emsg_t *ex_rcev(esock_t *sock, int timemseg)
{
    int ret, len_packet;
    if (sock->nservers)
        ret = ex_rcev_gen(sock, sock->nservers, EX_MAX_CLIENT, 1, 0, timemseg);
    else
        ret = ex_rcev_gen(sock, sock->nservers, EX_MAX_CLIENT, 1, 0, timemseg);
    if (ret <= 0)
        return NULL;

    //    struct timeval tv;  
    //    tv.tv_sec  = 0; 
    //    tv.tv_usec = 1000;
    //    setsockopt( sock->client->fd, SOL_SOCKET, SO_RCVTIMEO, (char *) &tv,sizeof(struct timeval));

    ret = recv(sock->client->fd, (char *) &len_packet, sizeof (len_packet), MSG_WAITALL | MSG_NOSIGNAL);

    if (assert_errstr((ret == 4 && len_packet > 0 && len_packet < EX_MAX_PACKET), "Mensaje de longitud incorrecta"))
    {
        ex_skipread(sock->client->fd);
        return NULL;
    }

    emsg_t *alloc_buf = calloc(sizeof (char), len_packet + sizeof (emsg_t) + 1);
    if (assert_errstr(alloc_buf != NULL, "calloc "))
        return NULL;

    int l = readall(sock->client->fd, alloc_buf->data, len_packet, timemseg);
    if (assert_errstr(l == len_packet, "No se pudo leer todo el paquete"))
    {
        efree(alloc_buf);
        return NULL;
    }
    alloc_buf->data[len_packet] = 0;
    alloc_buf->length = len_packet;
    return alloc_buf;
}


#define ex_free_sock(conn)  { shutdown(conn->fd, SHUT_RDWR);close(conn->fd);efree(conn->path);efree(conn);}

int ex_skipallread(econn_t **CLI, int maxcli)
{
    int i;
    int ret, nfds, con;
    fd_set rd;
    struct timeval tv;

    while (TRUE)
    {

        nfds = 0;
        FD_ZERO(&rd);

        for (i = 0; i < maxcli; i++)
        { // ES_MAX_NCSOCK
            if (CLI[i])
            {
                FD_SET(CLI[i]->fd, &rd);
                nfds = MAX(nfds, CLI[i]->fd);
            }
        }


        tv.tv_sec = 0;
        tv.tv_usec = 10000;

        ret = select(nfds + 1, &rd, NULL, NULL, &tv);

        if (ret == -1 && errno == EINTR)
            continue;
        if (ret < 1)
        {
            assert_errstr(0, "Error en select");
            return ERROR;
        }
        for (i = 0; i < maxcli; i++)
        {
            if (CLI[i])
            {
                con = CLI[i]->fd;
                if (FD_ISSET(con, &rd))
                {
                    ex_skipread(con);
                }
            }
        }
        return TRUE;
    }
    return ERROR;
}

int ex_check_connect(esock_t *sock)
{
    int i, l = 0, r;

    for (i = 0; i < EX_MAX_CLIENT; i++)
    {
        if (sock->clients[i] && sock->clients[i]->flags.client &&
            sock->clients[i]->flags.connected == 0)
        {
            r = ex_connect_tcp(sock->clients[i]);

            if(r == 0)
            {
                printf("Conectado con server: %s:%d\n", sock->clients[i]->path,
                       sock->clients[i]->port);
            }
        }
        if (sock->clients[i] && sock->clients[i]->flags.client &&
            sock->clients[i]->flags.connected)
        {
            l++;
        }
    }
    return l;
}

int ex_send(esock_t *sock, int id_client, char *msg, int length)
{
    int i;
    int l = 0, ret = 0;


    // elimina las conecciones pendientes y borra los mensajes pendientes de lecura
    ret = ex_rcev_gen(sock, sock->nservers, EX_MAX_CLIENT, 1, 0, 1);
    while (ret == 1)
    {
        ex_skipallread(sock->clients, sock->nclient);
        ret = ex_rcev_gen(sock, sock->nservers, EX_MAX_CLIENT, 1, 0, 1);
    }

    ex_check_connect(sock);

    ret = 0;
    for (i = (id_client > 0 ? id_client - 1 : 0); i < EX_MAX_CLIENT; i++)
    {
        if (sock->clients[i])
        {
            l = 0;
            if (sock->clients[i]->flags.connected)
            {
                l = send(sock->clients[i]->fd, (char *) &length, sizeof (length),
                         MSG_MORE | MSG_NOSIGNAL);
                if (l > 0)
                    l = sendall(sock->clients[i]->fd, msg, length);
            }
            if (l > 0)
            {
                ret++;
            }
        }
        if (id_client > 0)
            break;
    }
    return ret;
}

int ex_rcev_gen(esock_t *s, int nsrv, int ncli, int r, int wlen, int timemseg)
{
    int i, j;
    int ret, nfds, h, con;
    fd_set rd, wd;
    struct timeval tv, tv2;

    tv2.tv_sec = timemseg / 1000;
    tv2.tv_usec = 1000 * (timemseg % 1000);

    while (TRUE)
    {

        tv.tv_sec = tv2.tv_sec;
        tv.tv_usec = tv2.tv_usec;


        nfds = 0;
        if (r) FD_ZERO(&rd);
        if (wlen) FD_ZERO(&wd);

        for (i = 0; i < nsrv; i++)
        { // EX_MAX_SERVER
            if (r) FD_SET(s->servers[i]->fd, &rd);
            nfds = MAX(nfds, s->servers[i]->fd);
        }

        for (i = 0; i < EX_MAX_CLIENT; i++)
        { // EX_MAX_CLIENT
            if (s->clients[i])
            {
                if (r) FD_SET(s->clients[i]->fd, &rd);
                if (wlen) FD_SET(s->clients[i]->fd, &wd);
                nfds = MAX(nfds, s->clients[i]->fd);
            }
        }

        ret = select(nfds + 1, &rd, NULL, NULL, (timemseg > 0 ? &tv : NULL));
        
        if (ret == -1 && errno == EINTR)
        {
            perror("ret == -1");
            continue;
        }
        if (ret < 0)
        {
            assert_errstr(0, "Error en select");
            return -1;
        }
        if (ret == 0)
        {
            assert_errstr(0, "Tiempo de lectura agotado (timedout)");
            return 0;
        }

        for (j = 0; j < nsrv; j++)
        {
            h = s->servers[j]->fd;
            if (FD_ISSET(h, &rd))
            {
                unsigned int l;
                struct sockaddr_in client_address;
                memset(&client_address, 0, l = sizeof (client_address));
                con = accept(h, (struct sockaddr *) &client_address, &l);
                if (con < 0)
                {
                    assert_errstr(0, "socket - accept");
                    return -1;
                }
                else
                {
                    printf("Conexion Aceptada: %s:%d\n", inet_ntoa(client_address.sin_addr),
                           client_address.sin_port);
                    for (i = 0; i < EX_MAX_CLIENT; i++)
                    {
                        if (!s->clients[i])
                        {
                            s->clients[i] = calloc(sizeof (econn_t), 1);
                            s->clients[i]->fd = con;
                            s->clients[i]->parent = s->servers[j];
                            s->clients[i]->path = strdup(inet_ntoa(client_address.sin_addr));
                            s->clients[i]->port = client_address.sin_port;
                            s->nclient = MAX(s->nclient, i + 1);
                            s->clients[i]->flags.connected = 1;
                            s->clients[i]->flags.server = 1;
                            break;
                        }
                    }
                }
                continue;
            }
        }

        for (i = 0; i < ncli; i++)
        {
            if (s->clients[i])
            {
                con = s->clients[i]->fd;
                if (FD_ISSET(con, &rd))
                {
                    uint8_t len = 0;
                    ret = recv(con, (char *) &len, sizeof (len), MSG_PEEK | MSG_NOSIGNAL);
                    if (ret < 0 && errno == EINTR)
                        continue;
                    if (ret < 1)
                    {
                        if (s->clients[i]->flags.server)
                        {
                            fprintf(stderr, "Elimina conexion: %s:%d\n", s->clients[i]->path,
                                    s->clients[i]->port);
                            ex_free_sock(s->clients[i]);
                            break;
                        }
                        else
                        {
                            fprintf(stderr, "Desconectada conexion con: %s:%d\n",
                                    s->clients[i]->path, s->clients[i]->port);
                            s->clients[i]->flags.connected = 0;
                            return -2;
                        }
                        break;
                    }
                    s->client = s->clients[i];
                    return 1;
                }
                // break;
            }
        }
    }

    return -1;
}

esock_t *ex_sock(char *addr, char *mode)
{
    esock_t *s = ex_sock_alloc();
    if (s == NULL) return NULL;

    if (ex_sock_validator(s, addr, mode) == ERROR)
    {
        efree(s);
        return NULL;
    }


    return s;
}

esock_t *ex_sock_alloc(void)
{
    esock_t *s = (esock_t *) calloc(sizeof (esock_t), 1);
    if (assert_errstr(s != NULL, "malloc"))
        return NULL;
    return s;
}

int ex_sock_validator(esock_t *sock, char *addr, char *mode)
{
    //    char *path=NULL;
    //    char *port=NULL;
    //    
    //    if( (path=skipstr(addr,"udp://"))!=NULL ){
    //        path=strdup(path);
    //        port=strstr(path,":");
    //        sock->flags.udp=1;
    //    }
    //    else if( (path=skipstr(addr,"tcp://"))!=NULL ){
    //        path=strdup(path);
    //        port=strstr(path,":");
    //        sock->flags.tcp=1;
    //    }
    //    else{
    //        assert_errstr( 0, "addr, protocolo desconocido" ) ;
    //        return ERROR;
    //    }
    //    
    //    if( isString(mode,"SERVER"))
    //        sock->flags.server=1;
    //    else if( isString(mode,"CLIENT"))
    //        sock->flags.client=1;
    //    else{
    //        assert_errstr( 0, "mode, modo de operacion incorrecto" ) ;
    //        return NULL;
    //    }
    //    
    //    if(port){
    //        *port='\0';
    //        port+=1;
    //    }
    //    
    //    sock->sock.path=ex_get_ip(path);
    //    sock->sock.port=atoi(port);
    //    efree(path);
    //    
    //    if( assert_errstr( sock->sock.path!=NULL, "addr, no es una direccion ip valida" ) ){
    //        return ERROR;
    //    }
    //    if( port && assert_errstr( sock->sock.port>0 , "addr, puerto incorrecto" ) ){
    //        efree(sock->sock.path);
    //        return ERROR;
    //    }

    return 0;
}

int send2(int sock, char *buf, int len)
{
    fd_set sefd;
    int n, tmpres, ibuf;
    int sent;
    sent = 0;

    ibuf = 0;


    while (len > 0)
    {
        puts("aca -1");

        FD_ZERO(&sefd);
        FD_SET(sock, &sefd);

        n = select(1 + sock, NULL, &sefd, NULL, NULL);
        puts("aca -2");
        switch (n)
        {
        case -1: // error    
            fprintf(stderr, "Error select() errno:%d - %s \n", errno, strerror(errno));
            return -1;
        case 0: // timeout, entoces ya no hay mas nada para leer
            errno = ETIMEDOUT;
            return ibuf;
        default:
            if (FD_ISSET(sock, &sefd))
            {
                puts("aca 0");
                tmpres = send(sock, buf + sent, len, MSG_NOSIGNAL);
                if (tmpres == -1)
                {
                    fprintf(stderr, "Error: Can't send query errno:%d - %s \n", errno, strerror(errno));
                    return -1;
                }
                if (tmpres < 0)
                {
                    if (errno == EAGAIN)
                        continue;
                    fprintf(stderr, "Error read() errno:%d - %s \n", errno, strerror(errno));
                    return -1;
                }
                puts("aca 2");
                sent += tmpres;
                len -= tmpres;

            }
            break;
        }
    }
    return ibuf;
}

int sendall(int sock, char *buf, int len)
{
    int sent, tmpres;
    sent = 0;
    while (len > 0)
    {
        tmpres = send(sock, buf + sent, len, MSG_NOSIGNAL);
        if (tmpres < 0)
        {
            // fprintf(stderr, "Error: Can't send query errno:%d - %s \n", errno, strerror(errno));
            return ERROR;
        }
        sent += tmpres;
        len -= tmpres;
    }
    return sent;
}

int http_get(int sock, char *host, char *url)
{
#define _USERAGENT "HTMLGET 1.1"

    char *query;
    int resp;
    static char *tpl = "GET %s HTTP/1.1\r\nHost: %s\r\nUser-Agent: %s\r\n\r\n ";

    query = (char *) malloc(strlen(host) + strlen(url) + strlen(_USERAGENT) + strlen(tpl));
    sprintf(query, tpl, url, host, _USERAGENT);
    resp = sendall(sock, query, strlen(query));
    free(query);
    return resp;
}

int http_post_xml(int sock, char *host, char *url, char *strXml)
{
    char *query;
    int resp;
    static char *tpl = "POST %s HTTP/1.1"\
                   "\r\nHost: %s"\
                   "\r\nContent-Type: application/x-www-form-urlencoded"\
                   "\r\nContent-Length: %i"\
                   "\r\n\r\nstrXml=%s\r\n ";

    query = (char *) malloc(strlen(host) + strlen(url) + strlen(strXml) + strlen(tpl) + 10);
    sprintf(query, tpl, url, host, strlen(strXml), strXml);
    resp = sendall(sock, query, strlen(query));
    free(query);
    return resp;
}

int readhttp(int sock, char **buf)
{
    int length, tmpres;

    *buf = malloc(1000);
    // primero lee la cabecera hasta que encuentra el \r\n    
    length = 0;
    tmpres = readline(sock, *buf, 1000, 5);
    while (tmpres)
    {
        // printf ("--%s",*buf);
        if (strncmp(*buf, "Content-Length: ", strlen("Content-Length: ")) == 0)
        {
            length = atoi(*buf + strlen("Content-Length: "));
        }
        // busca la linea sola para fin de cabezera
        if (strcmp(*buf, "\r\n") == 0)
            break;
        tmpres = readline(sock, *buf, 1000, 5);
    }
    if (length == 0)
        return -1; // no se lee la longitud

    // printf ("\nlength: %i ",length);
    *buf = realloc(*buf, length + 1);
    if (buf == 0)
        return -2; // error en malloc

    memset(*buf, 0, length);
    tmpres = readall(sock, *buf, length, 5);
    return tmpres;
}

/**
 * int readline( int sock, char *buf, int maxlen, int timeseg  )
 * 
 * lee una linea delimitada por \n o hasta igual a maxlen;  
 * 
 * @param sock: sock TCP
 * @param buf: bufer donde guardar los datos leidos
 * @param maxlen: longitud maxima a leer
 * @param timesg: tiempo maximo en segundos para empezar a leer  
 * @return: longitud del string leido hasta un \n o timedout
 *          -1 si occure un error
 */
int readline(int sock, char *bufptr, int len, int timemseg)
{
    char *bufx = bufptr;
    char c;
    fd_set refd;
    int n, cnt;
    struct timeval tv, tv2;

    tv2.tv_sec = timemseg / 1000;
    tv2.tv_usec = 1000 * (timemseg % 1000);

    while (--len > 0)
    {
        FD_ZERO(&refd);
        FD_SET(sock, &refd);

        tv.tv_sec = tv2.tv_sec;
        tv.tv_usec = tv2.tv_usec;

        n = select(1 + sock, &refd, NULL, NULL, &tv);
        switch (n)
        {
        case -1: // error   
            fprintf(stderr, "Error select() errno:%d - %s \n", errno, strerror(errno));
            return -1;
        case 0: // timeout, entoces ya no hay mas nada para leer
            return bufptr - bufx;
        default:
            if (FD_ISSET(sock, &refd))
            {
                cnt = recv(sock, &c, 1, 0);
                if (cnt < 0)
                {
                    if (errno == EINTR)
                    {
                        len++; /* the while will decrement */
                        continue;
                    }
                    fprintf(stderr, "Error read() errno:%d - %s \n", errno, strerror(errno));
                    return -1;
                }
                if (cnt == 0)
                {
                    if (bufptr - bufx)
                        return bufptr - bufx;
                    return -1;
                }
            }
            if (c == '\n')
            {
                *bufptr = '\0';
                return bufptr - bufx;
            }
            *bufptr++ = c;
        }
    }
    return bufptr - bufx;
}

/**
 * int readall( int sock, char *buf, int len,int timeseg )
 * 
 * Lee hasta la longitud len, solo termina cuando lee todo o si se cumple el 
 * tiempo de timedout  
 *  
 * @param sock: socket TCP
 * @param buf: buffer donde guardar los datos leidos
 * @param len: longitud a leer
 * @param timeseg: timedout en segundos para empezar a leer     
 * @return: retorna len si pudo leer completamente 
 *          0 si sale por timedout 
 *          -1 si hay algun error   
 */
int readall(int sock, char *buf, int len, int timemseg)
{
    fd_set refd;
    int n, tmpres, ibuf;
    struct timeval tv, tv2;
    //    memset(buf, 0, len);
    ibuf = 0;

    tv2.tv_sec = timemseg / 1000;
    tv2.tv_usec = 1000 * (timemseg % 1000);

    while (len > 0)
    {
        FD_ZERO(&refd);
        FD_SET(sock, &refd);
        tv.tv_sec = tv2.tv_sec;
        tv.tv_usec = tv2.tv_usec;

        n = select(1 + sock, &refd, NULL, NULL, &tv);
        switch (n)
        {
        case -1: // error    
            fprintf(stderr, "Error select() errno:%d - %s \n", errno, strerror(errno));
            return -1;
        case 0: // timeout, entoces ya no hay mas nada para leer
            errno = ETIMEDOUT;
            return ibuf;
        default:
            if (FD_ISSET(sock, &refd))
            {
                tmpres = recv(sock, buf + ibuf, len, MSG_NOSIGNAL);
                //                    tmpres = read(sock, buf+ibuf, len);
                if (tmpres < 0)
                {

                    if (errno == EINTR)
                        continue;
                    fprintf(stderr, "Error read() errno:%d - %s \n", errno, strerror(errno));
                    return -1;
                }
                if (tmpres == 0)
                {
                    // end of file ?
                    if (ibuf)
                        return ibuf;
                    return -1;
                }
                ibuf += tmpres;
                len -= tmpres;
            }
            break;
        }
    }
    return ibuf;
}

int create_tcp_socket_host(char *host, int port)
{
    int sock;
    struct sockaddr_in serv_addr;

    alarm(3);
    if ((sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
    {
        perror("Can't create TCP socket");
        exit(1);
    }
    alarm(0);

    //    ex_get_ip(ip,host);
    // fprintf(stderr, "IP is %s\n", ip); 

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = inet_addr(host);
    serv_addr.sin_port = htons(port);

    //     fcntl(sock,F_GETFL);
    //     fcntl(sock, F_SETFL, O_NONBLOCK);    

    if (connect(sock, (struct sockaddr *) &serv_addr, sizeof (struct sockaddr)) < 0)
    {
        perror("Could not connect");
        close(sock);
        sock = -1;
    }
    return sock;
}

/**
 * int cust_readline( int sock, char *buf, int maxlen, int timeseg ,char* cendline)
 * 
 * lee una linea delimitada por \n o hasta igual a maxlen;  
 * 
 * @param sock: sock TCP
 * @param buf: bufer donde guardar los datos leidos
 * @param maxlen: longitud maxima a leer
 * @param timesg: tiempo maximo en segundos para empezar a leer
 * @param cendline: fin de linea personalizado   
 * @return: longitud del string leido hasta encontrar caracter de fin o timedout
 *          -1 si ocurre un error
 */
int cust_readline(int sock, char *bufptr, int len, int timeseg, char* eof)
{
    char *bufx = bufptr;
    char c;
    fd_set refd;
    int n, cnt;
    int le;
    struct timeval tv;

    le = strlen(eof);

    while (--len > 0)
    {
        tv.tv_sec = timeseg;
        tv.tv_usec = 0;

        FD_ZERO(&refd);
        FD_SET(sock, &refd);
        n = select(1 + sock, &refd, NULL, NULL, &tv);
        switch (n)
        {
        case -1: // error   
            fprintf(stderr, "Error select() errno:%d - %s \n", errno, strerror(errno));
            return -1;
        case 0: // timeout, entoces ya no hay mas nada para leer
            return bufptr - bufx;
        default:
            if (FD_ISSET(sock, &refd))
            {
                // WARNING: ver que si se usa un socket o un file descriptor no se comporta
                // igual usando read que rcev
                //cnt = read(sock,&c,1);
                cnt = recv(sock, &c, 1, MSG_NOSIGNAL);
                if (cnt < 0)
                {
                    if (errno == EINTR)
                    {
                        len++; /* the while will decrement */
                        continue;
                    }
                    fprintf(stderr, "Error read() errno:%d - %s \n", errno, strerror(errno));
                    return -1;
                }
                if (cnt == 0)
                {
                    if (bufptr - bufx)
                        return bufptr - bufx;
                    return -1;
                }
            }
            *bufptr++ = c;
            if (!strcmp(bufptr - le, eof))
            {
                *(bufptr - le) = '\0';
                return bufptr - bufx;
            }
        }
    }
    return bufptr - bufx;
}

int bind_tcp(char *ip, int port, int nlist)
{
    int sock;
    struct sockaddr_in serv_addr;
    int optval = 1;

    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        perror("Can't create TCP socket");
        return (-1);
    }
    memset(&serv_addr, 0, sizeof (struct sockaddr_in));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = inet_addr(ip);
    serv_addr.sin_port = htons(port);

    setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof (optval));
    // setsockopt(sock,SOL_SOCKET, SO_REUSEPORT,&optval, sizeof(optval));

    if (bind(sock, (struct sockaddr *) &serv_addr, sizeof (struct sockaddr)) == -1)
    {
        perror("Can't binding TCP socket");
        close(sock);
        return (-1);
    }
    if (listen(sock, nlist) == -1)
    {
        perror("Can't listen TCP socket");
        close(sock);
        return (-1);
    }
    return sock;
}

int connect_tcp(char *ip, int port)
{
    int res;
    long arg;
    fd_set myset;
    struct timeval tv;
    int valopt;
    unsigned int lon;

    int sock;
    struct sockaddr_in serv_addr;

    if ((sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
    {
        perror("Can't create TCP socket");
        return (-1);
    }
    // fprintf(stderr, "tcp ip :%s::%i\n", ip, port);
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = inet_addr(ip);
    serv_addr.sin_port = htons(port);

    // Set non-blocking 
    if ((arg = fcntl(sock, F_GETFL, NULL)) < 0)
    {
        fprintf(stderr, "Error fcntl(..., F_GETFL) (%s)\n", strerror(errno));
        return -1;
    }
    arg |= O_NONBLOCK;
    if (fcntl(sock, F_SETFL, arg) < 0)
    {
        fprintf(stderr, "Error fcntl(..., F_SETFL) (%s)\n", strerror(errno));
        return -1;
    }
    // Trying to connect with timeout 
    res = connect(sock, (struct sockaddr *) &serv_addr, sizeof (serv_addr));
    if (res < 0)
    {
        if (errno == EINPROGRESS)
        {
            do
            {
                tv.tv_sec = 0;
                tv.tv_usec = 100000;
                FD_ZERO(&myset);
                FD_SET(sock, &myset);
                res = select(sock + 1, NULL, &myset, NULL, &tv);
                if (res < 0 && errno != EINTR)
                {
                    fprintf(stderr, "Error connecting %d - %s\n", errno, strerror(errno));
                    return -1;
                }
                else if (res > 0)
                {
                    // Socket selected for write 
                    lon = sizeof (int);
                    if (getsockopt(sock, SOL_SOCKET, SO_ERROR, (void*) (&valopt), &lon) < 0)
                    {
                        fprintf(stderr, "Error in getsockopt() %d - %s\n", errno, strerror(errno));
                        return -1;
                    }
                    // Check the value returned... 
                    if (valopt)
                    {
                        //                        fprintf(stderr, "Error in delayed connection() %d - %s\n", valopt, strerror(valopt));
                        return -1;
                    }
                    break;
                }
                else
                {
                    fprintf(stderr, "Timeout in select() - Cancelling!\n");
                    return -1;
                }
            }
            while (1);
        }
        else
        {
            fprintf(stderr, "Error connecting %d - %s\n", errno, strerror(errno));
            return -1;
        }
    }
    // Set to blocking mode again... 
    if ((arg = fcntl(sock, F_GETFL, NULL)) < 0)
    {
        fprintf(stderr, "Error fcntl(..., F_GETFL) (%s)\n", strerror(errno));
        return -1;
    }
    arg &= (~O_NONBLOCK);
    if (fcntl(sock, F_SETFL, arg) < 0)
    {
        fprintf(stderr, "Error fcntl(..., F_SETFL) (%s)\n", strerror(errno));
        return -1;
    }
    return sock;
}

//int nn_device_twoway(struct nn_device_recipe *device,
//        int s1, nn_fd s1rcv, nn_fd s1snd,
//        int s2, nn_fd s2rcv, nn_fd s2snd) {
//    int rc;
//    struct pollfd pfd[4];
//
//    /*  Initialise the pollset. */
//    pfd[0].fd = s1rcv;
//    pfd[0].events = POLLIN;
//    pfd[1].fd = s1snd;
//    pfd[1].events = POLLIN;
//    pfd[2].fd = s2rcv;
//    pfd[2].events = POLLIN;
//    pfd[3].fd = s2snd;
//    pfd[3].events = POLLIN;
//
//    while (1) {
//
//        /*  Wait for network events. */
//        rc = poll(pfd, 4, -1);
//        assert(rc >= 0);
////        if (nn_slow(rc < 0 && errno == EINTR))
////            return -1;
//        assert(rc != 0);
//
//        /*  Process the events. When the event is received, we cease polling
//            for it. */
//        if (pfd[0].revents & POLLIN)
//            pfd[0].events = 0;
//        if (pfd[1].revents & POLLIN)
//            pfd[1].events = 0;
//        if (pfd[2].revents & POLLIN)
//            pfd[2].events = 0;
//        if (pfd[3].revents & POLLIN)
//            pfd[3].events = 0;
//
//        /*  If possible, pass the message from s1 to s2. */
//        if (pfd[0].events == 0 && pfd[3].events == 0) {
//            rc = nn_device_mvmsg(device, s1, s2, NN_DONTWAIT);
//            if (nn_slow(rc < 0))
//                return -1;
//            pfd[0].events = POLLIN;
//            pfd[3].events = POLLIN;
//        }
//
//        /*  If possible, pass the message from s2 to s1. */
//        if (pfd[2].events == 0 && pfd [1].events == 0) {
//            rc = nn_device_mvmsg(device, s2, s1, NN_DONTWAIT);
//            if (nn_slow(rc < 0))
//                return -1;
//            pfd[2].events = POLLIN;
//            pfd[1].events = POLLIN;
//        }
//    }
//}
//




static int forward_port;

#undef max
#define max(x,y) ((x) > (y) ? (x) : (y))

static int listen_socket(int listen_port)
{
    struct sockaddr_in a;
    int s;
    int yes;
    if ((s = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        perror("socket");
        return -1;
    }
    yes = 1;
    if (setsockopt
        (s, SOL_SOCKET, SO_REUSEADDR,
         (char *) &yes, sizeof (yes)) < 0)
    {
        perror("setsockopt");
        close(s);
        return -1;
    }
    memset(&a, 0, sizeof (a));
    a.sin_port = htons(listen_port);
    a.sin_family = AF_INET;
    if (bind
        (s, (struct sockaddr *) &a, sizeof (a)) < 0)
    {
        perror("bind");
        close(s);
        return -1;
    }
    printf("aceptando conexiones en el puerto %d\n",
           (int) listen_port);
    listen(s, 10);
    return s;
}

static int connect_socket(int connect_port,
                          char *address)
{
    struct sockaddr_in a;
    int s;
    if ((s = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        perror("socket");
        close(s);
        return -1;
    }

    memset(&a, 0, sizeof (a));
    a.sin_port = htons(connect_port);
    a.sin_family = AF_INET;

    if (!inet_aton
        (address,
         (struct in_addr *) &a.sin_addr.s_addr))
    {
        perror("formato de dirección IP incorrecto");
        close(s);
        return -1;
    }

    if (connect
        (s, (struct sockaddr *) &a,
         sizeof (a)) < 0)
    {
        perror("connect()");
        shutdown(s, SHUT_RDWR);
        close(s);
        return -1;
    }
    return s;
}

#define SHUT_FD1 {                      \
               if (fd1 >= 0) {                 \
                   shutdown (fd1, SHUT_RDWR);  \
                   close (fd1);                \
                   fd1 = -1;                   \
               }                               \
           }

#define SHUT_FD2 {                      \
               if (fd2 >= 0) {                 \
                   shutdown (fd2, SHUT_RDWR);  \
                   close (fd2);                \
                   fd2 = -1;                   \
               }                               \
           }

#define BUF_SIZE 1024

int TEST(int argc, char **argv)
{
    int h;
    int fd1 = -1, fd2 = -1;
    char buf1[BUF_SIZE], buf2[BUF_SIZE];
    int buf1_avail, buf1_written;
    int buf2_avail, buf2_written;

    if (argc != 4)
    {
        fprintf(stderr,
                "Uso\n\tfwd <puerto-escucha> \
       <redirigir-a-puerto> <redirigir-a-dirección-ip>\n");
        exit(1);
    }

    signal(SIGPIPE, SIG_IGN);

    forward_port = atoi(argv[2]);

    h = listen_socket(atoi(argv[1]));
    if (h < 0)
        exit(1);

    for (;;)
    {
        int r, nfds = 0;
        fd_set rd, wr, er;
        FD_ZERO(&rd);
        FD_ZERO(&wr);
        FD_ZERO(&er);
        FD_SET(h, &rd);
        nfds = max(nfds, h);
        if (fd1 > 0 && buf1_avail < BUF_SIZE)
        {
            FD_SET(fd1, &rd);
            nfds = max(nfds, fd1);
        }
        if (fd2 > 0 && buf2_avail < BUF_SIZE)
        {
            FD_SET(fd2, &rd);
            nfds = max(nfds, fd2);
        }
        if (fd1 > 0
            && buf2_avail - buf2_written > 0)
        {
            FD_SET(fd1, &wr);
            nfds = max(nfds, fd1);
        }
        if (fd2 > 0
            && buf1_avail - buf1_written > 0)
        {
            FD_SET(fd2, &wr);
            nfds = max(nfds, fd2);
        }
        if (fd1 > 0)
        {
            FD_SET(fd1, &er);
            nfds = max(nfds, fd1);
        }
        if (fd2 > 0)
        {
            FD_SET(fd2, &er);
            nfds = max(nfds, fd2);
        }

        r = select(nfds + 1, &rd, &wr, &er, NULL);

        if (r == -1 && errno == EINTR)
            continue;
        if (r < 0)
        {
            perror("select()");
            exit(1);
        }
        if (FD_ISSET(h, &rd))
        {
            unsigned int l;
            struct sockaddr_in client_address;
            memset(&client_address, 0, l =
                   sizeof (client_address));
            r = accept(h, (struct sockaddr *)
                       &client_address, &l);
            if (r < 0)
            {
                perror("accept()");
            }
            else
            {
                SHUT_FD1;
                SHUT_FD2;
                buf1_avail = buf1_written = 0;
                buf2_avail = buf2_written = 0;
                fd1 = r;
                fd2 =
                    connect_socket(forward_port,
                                   argv[3]);
                if (fd2 < 0)
                {
                    SHUT_FD1;
                }
                else
                    printf("conexión desde %s\n",
                           inet_ntoa
                           (client_address.sin_addr));
            }
        }
        /* NB: lee datos OOB antes de las lecturas normales */
        if (fd1 > 0)
            if (FD_ISSET(fd1, &er))
            {
                char c;
                errno = 0;
                r = recv(fd1, &c, 1, MSG_OOB);
                if (r < 1)
                {
                    SHUT_FD1;
                }
                else
                    send(fd2, &c, 1, MSG_OOB);
            }
        if (fd2 > 0)
            if (FD_ISSET(fd2, &er))
            {
                char c;
                errno = 0;
                r = recv(fd2, &c, 1, MSG_OOB);
                if (r < 1)
                {
                    SHUT_FD1;
                }
                else
                    send(fd1, &c, 1, MSG_OOB);
            }
        if (fd1 > 0)
            if (FD_ISSET(fd1, &rd))
            {
                r =
                    read(fd1, buf1 + buf1_avail,
                         BUF_SIZE - buf1_avail);
                if (r < 1)
                {
                    SHUT_FD1;
                }
                else
                    buf1_avail += r;
            }
        if (fd2 > 0)
            if (FD_ISSET(fd2, &rd))
            {
                r =
                    read(fd2, buf2 + buf2_avail,
                         BUF_SIZE - buf2_avail);
                if (r < 1)
                {
                    SHUT_FD2;
                }
                else
                    buf2_avail += r;
            }
        if (fd1 > 0)
            if (FD_ISSET(fd1, &wr))
            {
                r = write(fd1,
                          buf2 + buf2_written,
                          buf2_avail -
                          buf2_written);
                if (r < 1)
                {
                    SHUT_FD1;
                }
                else
                    buf2_written += r;
            }
        if (fd2 > 0)
            if (FD_ISSET(fd2, &wr))
            {
                r = write(fd2,
                          buf1 + buf1_written,
                          buf1_avail -
                          buf1_written);
                if (r < 1)
                {
                    SHUT_FD2;
                }
                else
                    buf1_written += r;
            }
        /* comprueba si se han escrito tantos datos como se han leído */
        if (buf1_written == buf1_avail)
            buf1_written = buf1_avail = 0;
        if (buf2_written == buf2_avail)
            buf2_written = buf2_avail = 0;
        /* si un extremo ha cerrado la conexión, continúa escribiendo al otro
           extremo hasta que no queden datos */
        if (fd1 < 0
            && buf1_avail - buf1_written == 0)
        {
            SHUT_FD2;
        }
        if (fd2 < 0
            && buf2_avail - buf2_written == 0)
        {
            SHUT_FD1;
        }
    }
    return 0;
}