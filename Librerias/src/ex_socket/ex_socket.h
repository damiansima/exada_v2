#include <stdio.h>
#include <sys/types.h>
#include <errno.h>

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>

#include <signal.h>

#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/time.h>                          
#include <sys/select.h>
#include <string.h>



#ifndef _SOCKETUTILS_H
#define _SOCKETUTILS_H

#define EX_MAX_PACKET (1*1024*1024) 
#define EX_MAX_NCSOCK 128
#define EX_MAX_CLIENT 128
#define EX_MAX_SERVER 5

typedef enum{
    pubsub_pub=10,
    pubsub_sub,
    reqrep_rep,
    reqrep_req,
    pushpull_push,
    pushpull_pull,
}protocol_mode_t; 

struct _econn_t{
    int fd;
    char *path;
    uint16_t port;
    struct _econn_t *parent;
    union {
        struct {
            unsigned server:1;
            unsigned client:1;
            unsigned connected:1;
            unsigned tcp:1;
            unsigned udp:1;
            unsigned ipc:1;
        };
        uint8_t values;         
    }flags;   
};
typedef struct _econn_t econn_t;

typedef struct {
    econn_t *servers[EX_MAX_SERVER];
    econn_t *clients[EX_MAX_CLIENT];
    econn_t *client;
    uint8_t nservers;
    uint8_t nclient;
    union {
        struct {
            unsigned server:1;
            unsigned client:1;
            unsigned binding:1;
            unsigned connected:1;
            unsigned tcp:1;
            unsigned udp:1;
            unsigned ipc:1;
            unsigned inproc:1;
        };
        uint16_t values;         
    }flags;       
    protocol_mode_t mode;
}esock_t;

typedef struct{ 
    uint32_t length;
    char data[];
}emsg_t;




#ifndef ERROR
    #define ERROR -1
#endif

#ifndef SUCCESS
    #define SUCCESS 0
#endif

#ifndef FALSE
    #define FALSE 0
#endif

#ifndef TRUE
    #define TRUE 1
#endif

#ifndef isString
    #define isString(src,cmp) (!strcasecmp(src,cmp))
#endif


#define MAX(x,y) ((x) > (y) ? (x) : (y))


char *ex_get_ip(const char *host); 

esock_t *ex_sock(char *addr, char *mode);

esock_t *ex_sock_alloc( void );

int ex_listen(esock_t *s, char *addr, int nlist); 


int ex_connect(esock_t *s, char *addr);


int ex_sock_validator( esock_t *sock, char *addr, char *mode);



emsg_t *ex_rcev( esock_t *sock, int timemseg);

int ex_send(esock_t *sock, int id_client, char *msg, int length);

int ex_check_connect(esock_t *sock);

int ex_reply( esock_t *sock, char *msg, int length );

void ex_disconnect(econn_t *conn);


char *ex_strerror(void);



// privadas
int ex_rcev_gen( esock_t *s, int nsrv, int ncli, int r, int wlen, int timemseg);



int send2(int sock, char *buf, int len );

/**
 * int sendall(int sock,char *buf,int len)
 * 
 * Bucle para enviar, solo sale cuando envia todo o algun error.
 * @return: 0 si envia todo 
 */
int sendall(int sock,char *buf,int len);
  
  

/**
 * int http_get(int sock, char *host, char *url)
 * 
 * @param sock
 * @param host: string del host (el mismo que se encontraria configurado en  /etc/hosts)
 * @param url:  direccion url a consultar
 * 
 *  ejemplo: si en el navegador consulto:
 *      http://nse.siderar.ot/webservice/WSModelo.asmx/GetEntradaModelo?strCodModelo=17&strCliente=Multicliente
 *  
 *  donde nse.sirerar.ot esta configurado en /etc/hosts y puedo hacer ping 
 *  
 *  para consultar esa pagina hago:
 *      http_get(sock,"nse.siderar.ot","/webservice/WSModelo.asmx/GetEntradaModelo?strCodModelo=17&strCliente=Multicliente"); 
 *      // y luego leeo la respuesta con
 *      readhttp(sock,&buf);
 * 
*/
int http_get(int sock, char *host, char *url);


int http_post_xml(int sock, char *host, char *url, char *strXml);



/** 
 * int readhttp(int sock,char *buf)
 * 
 * @param sock: sock TCP conectado de donde lee la respuesta
 * @param buf: puntero del puntero  donde se alloca la respuesta con malloc()
 * @return: entero con la longitud leida o errores (-1 -2) 
 * @warning: es necesario realizar free(buf);
 * 
 * Ejemplo:
 *      char *buf;
 *      r=readhttp(sock,&buf)
 *      if (r>0)
 *          // hace algo // 
 *      free (buf);       
 */
int readhttp(int sock,char **buf);


/**
 * int readline( int sock, char *buf, int maxlen, int timeseg  )
 * 
 * lee una linea delimitada por \n o hasta igual a maxlen;  
 * 
 * @param sock: sock TCP
 * @param buf: bufer donde guardar los datos leidos
 * @param maxlen: longitud maxima a leer
 * @param timesg: tiempo maximo en segundos para empezar a leer  
 * @return: longitud del string leido hasta un \n
 *
 */  
int readline( int sock, char *bufptr, int len,int timeseg );
   
/**
 * int readall( int sock, char *buf, int len,int timeseg )
 * 
 * Lee hasta la longitud len, solo termina cuando lee todo o si se cumple el 
 * tiempo de timedout  
 *  
 * @param sock: socket TCP
 * @param buf: buffer donde guardar los datos leidos
 * @param len: longitud a leer
 * @param timeseg: timedout en segundos para empezar a leer     
 * @return: retorna len si pudo leer completamente 
 *          >=0 y < len si sale por timedout (caracteres lidos) 
 *          -1 si hay algun error   
 */   
int readall( int sock, char *buf, int len,int timeseg );

   
int create_tcp_socket_host(char *host,int port);

 
 
/**
 * int cust_readline( int sock, char *buf, int maxlen, int timeseg ,char* cendline)
 * 
 * lee una linea delimitada por \n o hasta igual a maxlen;  
 * 
 * @param sock: sock TCP
 * @param buf: bufer donde guardar los datos leidos
 * @param maxlen: longitud maxima a leer
 * @param timesg: tiempo maximo en segundos para empezar a leer
 * @param cendline: fin de linea personalizado   
 * @return: longitud del string leido hasta encontrar caracter de fin o timedout
 *          -1 si ocurre un error
 */  
int cust_readline( int sock, char *bufptr, int len,int timeseg, char* eof );


int bind_tcp(char *ip, int port,int nlist);

int connect_tcp(char *ip,int port); 

#endif


