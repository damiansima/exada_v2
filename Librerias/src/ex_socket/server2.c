#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>

#include "logger.h"
#include "ex_socket.h"

void pepe(int a)
{
    printf(" pepe sig:%d %s\n ", a, strerror(errno));
}



#ifndef msleep
#define msleep(x) usleep(1000*(x))
#endif

int main(void)
{
    int r;
    int len;
    char *msg;
    esock_t *p;
    emsg_t *buf = NULL;


    log_debug_level(LOG_ALERT | LOG_ERROR | LOG_MSG | LOG_INFO | LOG_WARNING);
    log_logger_level(0);

    log_info("[SRV] Prueba socketutils %s", "[yea string]");

    p = ex_sock_alloc();
    r = ex_listen(p, "tcp://sima-ub:9001", 100);
    log_info("sock:%d", r);


    //    log_info("ex_rcev");
    //    buf= ex_rcev(p,0);
    //    log_msg("Longitud del paquete:%d <<%s>> ",buf->length, buf->data);
    //    log_msg("nservers:%d",p->nservers);   
    //    free(buf); 

    sleep(1);
    int i = 0;
    while (1)
    {
        len = asprintf(&msg, "send desde %s:%d [%d]", p->servers[0]->path,
                       p->servers[0]->port, i++);
        r = ex_send(p, 1, msg, len);
        log_info("Envia: <%s> ret:%d", msg, r);


//        log_info("ex_rcev");
//        buf = ex_rcev(p, 100);
//        if(buf){
//            log_msg("Longitud del paquete:%d <<%s>> ", buf->length, buf->data);
//            free(buf);
//        } 

        free(msg);
        msleep(100);
    }

    log_msg("[SRV] ex_rcev() end ");
    log_msg("[SRV] done");

    return EXIT_SUCCESS;
}
