/* 
 * File:   server_ipc.c
 * Author: nb
 *
 * Created on 22 de septiembre de 2015, 22:06
 */

#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>

#include "logger.h"
#include "ex_socket.h"

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>
#include <stdio.h>



/*
 * 
 * 
 * 
 * http://osr507doc.sco.com/en/netguide/dusockT.datagram_code_samples.html
 * 
 * 
 * 
 */

#define test_length(type) log_msg("%30s: %d","sizeof("#type")",sizeof(type))

int main(void) {
    log_debug_level(LOG_ALERT | LOG_ERROR | LOG_MSG | LOG_INFO | LOG_WARNING);
    log_logger_level(0);


    log_info("[SRV IPC] Prueba socketutils %s", "[yea string]");

    test_length(long long int);
    test_length(struct sockaddr);
    test_length(struct sockaddr_in);
    test_length(struct sockaddr_un);
    test_length(struct sockaddr_storage);

    int server_sockfd;
    int client_sockfd;
    int server_len;
    socklen_t client_len;
    struct sockaddr_un server_address;
    struct sockaddr_un client_address;

    unlink("server_socket");
    server_sockfd = socket(AF_UNIX, SOCK_DGRAM, 0);

    server_address.sun_family = AF_UNIX;
    strcpy(server_address.sun_path, "server_socket");
    server_len = sizeof(server_address);
    bind(server_sockfd, (struct sockaddr*) &server_address, server_len);

    listen(server_sockfd, 5);
    while (TRUE) {
        char buf[1024];
        client_sockfd = accept(server_sockfd, (struct sockaddr*) & client_address, &client_len);
        
        memset(buf,0,1024);
        int r=read(client_sockfd, buf, 1024);
        if(r>0){
            log_msg("read:%d: %s",r, buf);
            log_msg("eco:%s",buf);
            write(client_sockfd, buf, r);
            log_msg("close");
            close(client_sockfd);
        }
    }

    log_info("[SRV IPC] done");

    return EXIT_SUCCESS;
}
