/* 
 * File:   client_ipc.c
 * Author: nb
 *
 * Created on 22 de septiembre de 2015, 22:07
 */

#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>

#include "logger.h"
#include "ex_socket.h"

#include <netinet/tcp.h>
#include <netinet/in.h>
#include <netinet/udp.h>
#include <sys/un.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>
#include <stdio.h>

#define test_length(type) log_msg("%30s: %d","sizeof("#type")",sizeof(type))

int main( void )
{
    log_debug_level(LOG_ALERT|LOG_ERROR|LOG_MSG|LOG_INFO|LOG_WARNING);
    log_logger_level(0); 
    
    
    log_info("[CLI IPC] Prueba socketutils %s","[yea string]"); 


    int         sockfd;
    int         len;
    struct sockaddr_un address;
    int         result;
    char        ch       = 'A';

    sockfd = socket(AF_UNIX, SOCK_DGRAM, 0);

    address.sun_family = AF_UNIX;
    strcpy(address.sun_path, "server_socket");
    len = sizeof(address);

    result = connect(sockfd, (struct sockaddr*)&address, len);

    if(result == -1)
    {
        perror("oops:  client1");
        exit(1);
    }

    char buf[1024];
    int r;
    {
        memset(buf,0,1024);
        r = read(1, buf, 1024);
        
        log_msg("Send: <%s>",buf);
        write(sockfd, buf, r);
        r=read(sockfd, buf, 1024);
        log_msg("Rcev:%d <%s>",r,buf);

    }
    printf("char from server = %c\n", ch);
    close(sockfd);
    exit(0);
}