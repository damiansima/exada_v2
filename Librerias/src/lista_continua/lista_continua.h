/* 
 * File:   lista_continua.h
 * Author: sima
 *
 * Created on 3 de junio de 2014, 22:17
 */

#ifndef LISTA_CONTINUA_H
#define	LISTA_CONTINUA_H


#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <fcntl.h>
#include <stdarg.h>  

typedef struct{
    int key;
    int next;
    int prev;
    char data[];
}LIST_BLOCK; 

typedef int (*__handler_list_gfind) (LIST_BLOCK *);

typedef struct{
    int count_list;
    int block_start;
    int block_end;
    int length;
}LIST_CTR; 

typedef struct{
    LIST_CTR ctr;
    LIST_BLOCK block;
}LIST_t; 



LIST_t *list_init(LIST_t *list,int length);

LIST_t *list_alloc(int length);

LIST_t *list_resize(LIST_t *list, int length);


LIST_BLOCK *list_add(LIST_t *list,  int key, int nbytes);

LIST_BLOCK *list_add_alloc(LIST_t **list,  int key, int nbytes);



LIST_BLOCK *list_first_block(LIST_t *list);

LIST_BLOCK *list_next_block(LIST_t *list, LIST_BLOCK *block);



LIST_BLOCK *list_generic_find(LIST_t *list, __handler_list_gfind func);

LIST_BLOCK *list_get_block(LIST_t *list, int key); 

LIST_BLOCK *list_pop(LIST_t *list);

// olds
#define length_memory(l)    ((l)->ctr.length + sizeof(LIST_CTR))
#define length_list(l)      ((l)->ctr.block_end + sizeof(LIST_CTR))

#define list_empty(l)       (((l)==NULL) ? 1 : (((l)->ctr.block_end==0) ? 1 : 0 ))
#define list_memlen(l)      ((l)->ctr.length + sizeof(LIST_CTR))
#define list_datalen(l)     ((l)->ctr.block_end + sizeof(LIST_CTR))

int list_get_nbytes( LIST_t *list, LIST_BLOCK *block); 

LIST_BLOCK *list_add_raw( LIST_t *list, int key, void *value, int nbytes);
LIST_BLOCK *list_add_raw_alloc( LIST_t **list, int key, void *value, int nbytes); 

#define list_add_int(list,key,value)    list_add_raw(list,key, &(int){value}, sizeof(int))
#define list_add_int64(list,key,value)  list_add_raw(list,key, &(long long int){value}, sizeof(long long int))
#define list_add_float(list,key,value)  list_add_raw(list,key, &(float){value}, sizeof(float))
#define list_add_double(list,key,value) list_add_raw(list,key, &(double){value}, sizeof(double))
#define list_add_str(list,key,value)    list_add_raw(list,key,(char *)value,strlen(value)+1)

char *list_get_raw(LIST_t *list, int key, void *dest);

#define list_get_int(list,key) (*((int *) list_get_raw(list,key,NULL)))
#define list_get_int64(list,key) (*((long long int *) list_get_raw(list,key,NULL)))
#define list_get_float(list,key) (*((float *) list_get_raw(list,key,NULL)))
#define list_get_double(list,key) (*((double *) list_get_raw(list,key,NULL)))
#define list_get_str(list,key) ((char *) list_get_raw(list,key,NULL))   

                                                
#endif	/* LISTA_CONTINUA_H */
