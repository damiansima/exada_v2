/* 
 * File:   lista_continua.h
 * Author: sima
 *
 * Created on 3 de junio de 2014, 22:17
 */


#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <fcntl.h>
#include <stdarg.h>  
#include "lista_continua.h"


#define list_assert(A) _list_assert( (A) ,__FILE__, __LINE__, #A, NULL )
#define list_assert_msg(A, msg) _list_assert( (A) ,__FILE__, __LINE__, #A, msg )

int _list_assert(int val, char *file, int line, char *cmd, char *msg) 
{
    val=!val;
    if(val)
    {
        fprintf(stderr,"%s:%u: '%s' %s FAIL!\n",file, line, cmd, msg);
    }
    return val;
}

LIST_BLOCK *list_pop(LIST_t *list)
{
    LIST_BLOCK *block, *block_prev;
    if(list->ctr.count_list==0)
        return NULL;
    list->ctr.block_end = list->ctr.block_start;
    block = (LIST_BLOCK *)((char *)&list->block + list->ctr.block_end);
    list->ctr.block_start = block->prev;
    block_prev =(LIST_BLOCK *)((char *)&list->block + block->prev);  
    block_prev->next =  -1;
    list->ctr.count_list--;
    return block;
}

LIST_BLOCK *list_add(LIST_t *list,  int key, int nbytes) 
{
    LIST_BLOCK *block, *block_prev;
    if (list_assert(list!=NULL)) 
        return NULL;
    if (list_assert((list->ctr.length - list->ctr.block_end - nbytes - sizeof(LIST_BLOCK)) >0 ))
        return NULL;
    
    if (key && list_assert_msg(list_get_block(list,key)==NULL, "clave en uso"))
        return NULL;
    
    block = (LIST_BLOCK *)((char *)&list->block + list->ctr.block_end);
    block->key = key;
    block->next =-1;
    block->prev = -1;
//    memset(block->data,0,nbytes);
    
    if(list->ctr.count_list > 0)
    {
        block->prev = list->ctr.block_start;
        block_prev =(LIST_BLOCK *)((char *)&list->block + block->prev);  
        block_prev->next =  list->ctr.block_end;
    }
    list->ctr.block_start = list->ctr.block_end;
    list->ctr.block_end = list->ctr.block_end + nbytes + sizeof(LIST_BLOCK);
    list->ctr.count_list++;
    
    return block;
}



LIST_BLOCK *list_first_block(LIST_t *list) 
{
    if (list_assert(list!=NULL))
        return NULL;
    return (LIST_BLOCK *)((char *)&list->block );  
}

LIST_BLOCK *list_next_block(LIST_t *list, LIST_BLOCK *block) 
{
    if (list_assert(list!=NULL))
        return NULL;
    if (list_assert(block!=NULL))
        return NULL;
    
    
    if(block->next == -1 )
    {
        return NULL;  
    }
    else 
    {
       return (LIST_BLOCK *)((char *)&list->block + block->next);  
    }
}


LIST_BLOCK *list_get_block(LIST_t *list, int key) 
{
    LIST_BLOCK *block=NULL;
    
    if (list==NULL)
        return NULL;
    
    if (list->ctr.count_list <= 0)
        return NULL; 
    
    block = list_first_block(list);
    while(block)
    {
        if(block->key==key)
            return block; 
        
        block = list_next_block(list,block);
    }
    return NULL;  
}



LIST_t *list_init(LIST_t *list,int length)
{
    if (list_assert(list!=NULL))
        return NULL;
    list->ctr.block_end=0;
    list->ctr.block_start=-1;
    list->ctr.count_list=0;
    list->ctr.length = length - sizeof(LIST_CTR);
    return list;
}

LIST_t *list_alloc(int length)
{
    LIST_t *list;
    if (list_assert(length>0))
        return NULL;
    list = (LIST_t *)malloc( length );
    
    return list_init(list, length);
}


LIST_t *list_resize(LIST_t *list, int length)
{
    LIST_t *l;
    int res=length%512;
    if (list_assert(list!=NULL))
        return NULL;
    if(list->ctr.length >= length - sizeof(LIST_CTR))
        return list;
    if(res)
        length += (512-res);
    l = (LIST_t *)realloc( list, length );
    if(l!=NULL)
        l->ctr.length = length - sizeof(LIST_CTR);
    return l;
}



LIST_BLOCK *list_add_alloc(LIST_t **list, int key, int nbytes) 
{
    LIST_t *l;
    if (list_assert(*list!=NULL))
        return NULL;
    if (((*list)->ctr.length - (*list)->ctr.block_end - nbytes - sizeof(LIST_BLOCK)) <= 0 )
    {
        l=list_resize( *list, (*list)->ctr.length + sizeof(LIST_CTR) + nbytes + sizeof(LIST_BLOCK) +1 );
        if (list_assert(l!=NULL))
            return NULL;
        *list=l;
    }   
    return list_add( *list, key, nbytes);
}



LIST_BLOCK *list_generic_find(LIST_t *list, __handler_list_gfind func) 
{
    LIST_BLOCK *b=NULL;
    int i;
    
    if (list->ctr.count_list <= 0)
        return NULL; 
    
    b = (LIST_BLOCK *)((char *)&list->block);  
    if(func(b))
        return b;
    for(i=0; i < list->ctr.count_list && b->next>=0; i++)
    {
        b = (LIST_BLOCK *)((char *)&list->block + b->next);  
        if(func(b))
            return b;
    }
    return NULL;  
}


int list_get_nbytes( LIST_t *list, LIST_BLOCK *block) 
{
    int nbytes;
    if (list_assert( list!=NULL ) )
        return -1;
    if (list_assert( block!=NULL ) )
        return -1;
    
    if(block->next > 0)
        nbytes = block->next - ((int)block - (int)&list->block) - sizeof(LIST_BLOCK); 
    else
        nbytes = list->ctr.block_end - list->ctr.block_start-sizeof(LIST_BLOCK);

    return nbytes;
}

LIST_BLOCK *list_add_raw( LIST_t *list, int key, void *value, int nbytes) 
{
    LIST_BLOCK *block;
    block = list_add(list, key, nbytes );
    if( block == NULL )
        return NULL;
    memcpy(block->data, value, nbytes);    
    return block; 
}

LIST_BLOCK *list_add_raw_alloc( LIST_t **list, int key, void *value, int nbytes) 
{
    LIST_BLOCK *block;
    block = list_add_alloc(list, key, nbytes );
    if( block == NULL )
        return NULL;
    memcpy(block->data, value, nbytes);    
    return block; 
}

char *list_get_raw(LIST_t *list, int key, void *dest)
{
    LIST_BLOCK *block;
    block = list_get_block(list,key);
    if (list_assert( block!=NULL ) )
        return NULL;
    
    if(dest)
        memcpy(dest,block->data,list_get_nbytes(list,block));
    else
        dest=block->data;
    return dest;
}

//int list_add_float(LIST_t *list, int key, float value) 
//{
//    return list_add_raw( list, key, &value, sizeof(value)+1);
//}
//int list_add_int(LIST_t *list, int key, long int value)
//{
//    return list_add_raw( list, key, &value, sizeof(value)+1);
//} 

