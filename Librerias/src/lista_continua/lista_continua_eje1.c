#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include "lista_continua.h"


#define LEN_SHM 102

#define TESTS(a) printf("%-30s: %s\n",#a,(char *)a)
#define TEST(a) printf("%-30s: %i\n",#a,(int)a)
#define TESTN(a) {printf("--- %-30s\n",#a);a;}



int main () 
{
    LIST_BLOCK *block;
    LIST_t *list ;
//    char *shm = (char *)malloc(LEN_SHM);
//    list = (LIST_t *)shm;
//    
//    list_init(list,LEN_SHM);
    
    list = list_alloc(1200);
        
    TESTN(block = list_add(list, 8, 100));
    TESTN(block = list_add(list, 2, 100));
    TESTN(block = list_add(list, 3, 23));
    TESTN(block = list_add(list, 4, 100));
    TESTN(block = list_add(list, 5, 22));
    TESTN(block = list_add(list, 6, 23));
    TESTN(block = list_add(list, 1, 234));
    TESTN(block = list_add(list, 1, 221));
    TESTN(block = list_add(list, 7, 111));
    
    TESTN(block = list_add_int(list, 12, 4321));
    TEST(list_get_int(list, 12));
    TEST(list->ctr.count_list);
    
    TESTN(block = list_first_block(list));
    while(block)
    {
        strcpy(block->data,"nada");
        block = list_next_block(list,block);
    }
    block=list_get_block(list,7);
    strcpy(block->data,"--- hola LOCO    7");
    block=list_get_block(list,4);
    strcpy(block->data,"--- hola LOCO    4");
    
    TESTN(block = list_first_block(list));
    while(block)
    {
        TEST(block);
        TEST(block->key);
        TEST(block->data);
//        TEST(block->nbytes);
        TEST(block->next);
        TEST(block->prev);
        TEST(list_get_nbytes(list,block));
        TESTS(block->data);
        TESTN(block = list_next_block(list,block));
    }
    char val[1000];
    list_get_raw(list,4,val);
    printf("Testeo val:%s\n", val);
    free(list);
            
    return 0;
}