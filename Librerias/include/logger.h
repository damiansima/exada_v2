#ifndef _LOGGER_H__
#define _LOGGER_H__

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>   
#include <sys/time.h>

#define _LOG_ERROR       "\033[01;31m"
#define _LOG_ALERT       "\033[01;31m"
#define _LOG_WARNING     "\033[01;33m"
#define _LOG_MSG         "\033[01;34m"
#define _LOG_INFO        "\033[01;32m"
#define _LOG_DEBUG       "\033[01;44m"
#define _LOG_RESET       "\033[00m"


#define LOG_ALERT   1
#define LOG_ERROR   1<<1
#define LOG_WARNING 1<<2
#define LOG_INFO    1<<3
#define LOG_MSG     1<<4
#define LOG_DEBUG   1<<5

// <Public>
////////////////////////////////////////////////////////////

/*   logger_file_date(char *file_path, char *fmt_date);
* 
*       file_path: es la base del archivo
*       fmt_date: es el formato para la fecha    
*  
*    fmt es algo como "%Y%m%d"  o "%d_%b_%y"  
*    tambien se puede usar la hora, etc..
*/
void logger_file_date(char *file_path, char *fmt_date);


/*  set_file_size(file_size_kb) 
*   
*   elige el tama�o del archivo de log 
*        set_file_size(10); //set KB                                     
**/

void log_set_size(int file_size_kb);

    
/*  log_set_file(file_log); 
*   
*   elige el archivo de log, ejemplo: 
*        log_set_file("/tmp/borrar.log");                                     
**/
void log_set_file(char *log);


/*  log_set_fmt(fmt_date);
 * 
 * Se usa antes que log_set_file
 *      fmt es algo como "%Y%m%d"  o "%d_%b_%y"
 *      tambien se puede usar la hora, etc...
 *      No es compatible con set_file_size
 *  
 * ejemplo:
 *      log_set_fmt("%Y%m%d");
 *      log_set_file("/tmp/borrar.log");                                     
**/
void log_set_fmt(const char *fmt_date); 


/* void log_debug_level(int deb)
*  void log_logger_level(int lev); 
*  
*  Estas funciones setena que niveles o medo seran guardados 
*  en archivo de log o imprimir en pantalla.
*  
*  ejemplo:
*       log_logger_level(LOG_ERROR|LOG_ALERT);
*       // solo logea en archivo error y alert
*/                                          
void log_debug_level(int deb);
void log_logger_level(int lev);

void log_set_debug_verbose(int level);


/* void log_<mode>(char *fmt,...) 
*  
*  se usa igual que printf(fmt, ...)
*  
*  donde mode indica el tipo de mensaje:
*       alert
*       info
*       error
*       msg
*       warning
*       
*  Se puede configurar que nivel o modo guardaria en archivo 
*  o imprime en pantalla
*  
*/
void log_alert(char *fmt,...);
void log_info(char *fmt,...);
void log_error(char *fmt,...);
void log_msg(char *fmt,...);
void log_warning(char *fmt,...);
void logger(char *fmt,...);
void log_debug(int level, char *fmt,...); 

char *log_trace_level(int l); 
void log_cancel_trace(void);

// <Private>
///////////////////////////////////////////////////////////////
void _debug(int mode, const char *fmt, va_list arglist);
void _logger(char *mode, const char *format, va_list arglist);

#endif

