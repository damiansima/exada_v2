#include <stdio.h>
#include <sys/types.h>
#include <errno.h>

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>

#include <signal.h>

#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/time.h>                          
#include <sys/select.h>
#include <string.h>



#ifndef _SOCKETUTILS_H
#define _SOCKETUTILS_H

/**
 * int sendall(int sock,char *buf,int len)
 * 
 * Bucle para enviar, solo sale cuando envia todo o algun error.
 * @return: 0 si envia todo 
 */
int sendall(int sock,char *buf,int len);
  
  

/**
 * int http_get(int sock, char *host, char *url)
 * 
 * @param sock
 * @param host: string del host (el mismo que se encontraria configurado en  /etc/hosts)
 * @param url:  direccion url a consultar
 * 
 *  ejemplo: si en el navegador consulto:
 *      http://nse.siderar.ot/webservice/WSModelo.asmx/GetEntradaModelo?strCodModelo=17&strCliente=Multicliente
 *  
 *  donde nse.sirerar.ot esta configurado en /etc/hosts y puedo hacer ping 
 *  
 *  para consultar esa pagina hago:
 *      http_get(sock,"nse.siderar.ot","/webservice/WSModelo.asmx/GetEntradaModelo?strCodModelo=17&strCliente=Multicliente"); 
 *      // y luego leeo la respuesta con
 *      readhttp(sock,&buf);
 * 
*/
int http_get(int sock, char *host, char *url);


int http_post_xml(int sock, char *host, char *url, char *strXml);



/** 
 * int readhttp(int sock,char *buf)
 * 
 * @param sock: sock TCP conectado de donde lee la respuesta
 * @param buf: puntero del puntero  donde se alloca la respuesta con malloc()
 * @return: entero con la longitud leida o errores (-1 -2) 
 * @warning: es necesario realizar free(buf);
 * 
 * Ejemplo:
 *      char *buf;
 *      r=readhttp(sock,&buf)
 *      if (r>0)
 *          // hace algo // 
 *      free (buf);       
 */
int readhttp(int sock,char **buf);


/**
 * int readline( int sock, char *buf, int maxlen, int timeseg  )
 * 
 * lee una linea delimitada por \n o hasta igual a maxlen;  
 * 
 * @param sock: sock TCP
 * @param buf: bufer donde guardar los datos leidos
 * @param maxlen: longitud maxima a leer
 * @param timesg: tiempo maximo en segundos para empezar a leer  
 * @return: longitud del string leido hasta un \n
 *
 */  
int readline( int sock, char *bufptr, int len,int timeseg );
   
/**
 * int readall( int sock, char *buf, int len,int timeseg )
 * 
 * Lee hasta la longitud len, solo termina cuando lee todo o si se cumple el 
 * tiempo de timedout  
 *  
 * @param sock: socket TCP
 * @param buf: buffer donde guardar los datos leidos
 * @param len: longitud a leer
 * @param timeseg: timedout en segundos para empezar a leer     
 * @return: retorna len si pudo leer completamente 
 *          >=0 y < len si sale por timedout (caracteres lidos) 
 *          -1 si hay algun error   
 */   
int readall( int sock, char *buf, int len,int timeseg );

   
int create_tcp_socket_host(char *host,int port);

 
char *get_ip(char *ip, const char *host);
 
 
/**
 * int cust_readline( int sock, char *buf, int maxlen, int timeseg ,char* cendline)
 * 
 * lee una linea delimitada por \n o hasta igual a maxlen;  
 * 
 * @param sock: sock TCP
 * @param buf: bufer donde guardar los datos leidos
 * @param maxlen: longitud maxima a leer
 * @param timesg: tiempo maximo en segundos para empezar a leer
 * @param cendline: fin de linea personalizado   
 * @return: longitud del string leido hasta encontrar caracter de fin o timedout
 *          -1 si ocurre un error
 */  
int cust_readline( int sock, char *bufptr, int len,int timeseg, char* eof );


int bind_tcp(char *ip, int port,int nlist);

int connect_tcp(char *ip,int port); 

#endif


